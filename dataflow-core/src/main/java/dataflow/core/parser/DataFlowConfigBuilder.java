/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfigBuilder.java
 */

package dataflow.core.parser;

import static dataflow.core.parser.DataFlowConfig.DEFAULT_ITEM_VALUE_NAME;

import dataflow.annotation.metadata.DataFlowComponentInputMetadata;
import dataflow.annotation.metadata.DataFlowComponentMetadata;
import dataflow.annotation.metadata.DataFlowConfigurablePropertyMetadata;
import dataflow.core.component.ConstValueProviderMetadata;
import dataflow.core.component.DataFlowInstanceValueProvider;
import dataflow.core.component.PlaceholderStringValueProviderMetadata;
import dataflow.core.component.collection.ListValueProviderMetadata;
import dataflow.core.component.collection.MapEntryValueProviderMetadata;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.GraphUtil;
import dataflow.core.engine.PlaceholderString;
import dataflow.core.engine.ValueReference;
import dataflow.core.exception.DataFlowConfigurationException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@SuppressWarnings({"unchecked", "WeakerAccess"})
public class DataFlowConfigBuilder {

    public DataFlowEngine getEngine() {
        return engine;
    }

    static class IdGenerator {

        private static final String PREFIX = "_";
        private Map<String, Integer> nameCountMap = new HashMap<>();

        public String generate(String prefix) {
            prefix = prefix != null ? PREFIX + prefix : PREFIX;
            int count = nameCountMap.getOrDefault(prefix, 0);
            String name = prefix + count;
            nameCountMap.put(prefix, count + 1);
            return name;
        }
    }

    static class ConfigBuilderContext {

        final Map<String, ComponentConfig> componentConfigs = new HashMap<>();
        final Map<String, DataFlowConfig> childDataFlowConfigs = new HashMap<>();
        final IdGenerator idGenerator = new IdGenerator();
        final DataFlowEngine engine;
        final ExpressionConfigPostProcessor expressionConfigPostProcessor;
        final ValueConverterConfigPostProcessor valueConverterConfigPostProcessor;
        final Set<String> propValueRefDisabledComponentIds = new HashSet<>();

        private ConfigBuilderContext(DataFlowEngine engine) {
            this.engine = engine;
            this.expressionConfigPostProcessor = new ExpressionConfigPostProcessor();
            this.valueConverterConfigPostProcessor = new ValueConverterConfigPostProcessor();
        }
    }

    private static final Pattern VALUE_REFERENCE = Pattern.compile("\\$\\(([^)]+?)\\)");
    private static final String PLACEHOLDER_STRING_ID_PREFIX = "placeholderString";

    private final DataFlowEngine engine;


    public DataFlowConfigBuilder(DataFlowEngine engine) {
        this.engine = engine;
    }

    public DataFlowConfig build(Map<String, Object> parsedConfig) throws DataFlowConfigurationException {
        return build(parsedConfig, new ConfigBuilderContext(engine));
    }

    public static DataFlowConfig build(Map<String, Object> parsedConfig, ConfigBuilderContext ctx)
            throws DataFlowConfigurationException {
        for (Map.Entry<String, Object> entry : parsedConfig.entrySet()) {
            String key = entry.getKey().toLowerCase();
            if (key.equalsIgnoreCase("DataFlow")) {
                Object dataFlowObj = entry.getValue();
                if (!(dataFlowObj instanceof Map)) {
                    throw new DataFlowConfigurationException("Invalid DataFlow configuration: " + dataFlowObj);
                }
                return buildDataFlowConfig((Map<String, Object>) dataFlowObj, ctx);
            } else {
                return buildDataFlowConfig(parsedConfig, ctx);
            }
        }
        throw new DataFlowConfigurationException(
                "Configuration must have DataFlow or ValueProviders as a root element");
    }

    private static DataFlowConfig buildDataFlowConfig(Map<String, Object> dataFlowObj, ConfigBuilderContext ctx)
            throws DataFlowConfigurationException {
        String id = null;
        ComponentConfig output = null;
        boolean eventsEnabled = false;

        if(dataFlowObj.getClass() == RawComponentConfig.class) {
            // DataFlow consists of a single component.
            output = buildComponent(dataFlowObj, ctx);
            dataFlowObj = new HashMap<>();
        }
        for (Map.Entry<String, Object> entry : dataFlowObj.entrySet()) {
            String key = entry.getKey().toLowerCase();
            if (key.equalsIgnoreCase("id")) {
                id = (String) entry.getValue();
            } else if (key.equalsIgnoreCase("Components") ||
                (key.equalsIgnoreCase("ValueProviders") ||
                    key.equalsIgnoreCase("Actions"))) {
                buildComponents(entry.getValue(), ctx);
            } else if (key.equalsIgnoreCase("output")) {
                output = buildComponent(entry.getValue(), ctx);
            } else if (key.equalsIgnoreCase("eventsEnabled")) {
                eventsEnabled = (boolean) entry.getValue();
            } else {
                Map<String, Object> componentConfig = new HashMap<>();
                componentConfig.put(entry.getKey(), entry.getValue());
                buildComponent(componentConfig, ctx);
            }
        }

        ctx.expressionConfigPostProcessor.process(ctx);

        new ArrayList<>(ctx.componentConfigs.values()).forEach(v ->
                buildPropertyValueReferences(v, v.getProperties(), ctx));

        processExtends(ctx);

        if (output == null) {
            List<ComponentConfig> outputs = ctx.componentConfigs.values().stream()
                .filter(componentCfg -> componentCfg.getType() != null)
                .filter(componentCfg -> !componentCfg.isSink())
                .filter(componentCfg -> componentCfg.getOutputComponents() != null &&
                    componentCfg.getOutputComponents().isEmpty())
                .collect(Collectors.toList());
            if (outputs.size() == 1) {
                output = outputs.get(0);
            } else {
                throw new DataFlowConfigurationException(
                    "Output for dataflow " + id + " is not defined");
            }
        }

        ctx.valueConverterConfigPostProcessor.process(ctx);

        return new DataFlowConfig(id, ctx.componentConfigs, output, ctx.childDataFlowConfigs, eventsEnabled);
    }

    static void buildPropertyValueReferences(ComponentConfig componentConfig, Object propertyValue,
            ConfigBuilderContext ctx) {
        if(propertyValue == null ||
            ctx.propValueRefDisabledComponentIds.contains(componentConfig.getId())) {
            return;
        }
        if (propertyValue instanceof Map) {
            Map map = (Map) propertyValue;
            Iterator<Map.Entry> entryIt = map.entrySet().iterator();
            while (entryIt.hasNext()) {
                Map.Entry entry = entryIt.next();
                ComponentConfig propertyValueProvider = getOrCreatePropertyValueProvider(
                        entry.getValue(), ctx);
                if (propertyValueProvider != null) {
                    entry.setValue(new ValueReference(propertyValueProvider.getId()));
                    componentConfig.addPropertyValueProvider(propertyValueProvider);
                }
            }
            map.values().forEach(mapValue ->
                    buildPropertyValueReferences(componentConfig, mapValue, ctx));
        } else if (propertyValue instanceof List) {
            List list = (List) propertyValue;
            for (int idx = 0; idx < list.size(); idx++) {
                Object item = list.get(idx);
                ComponentConfig propertyValueProvider = getOrCreatePropertyValueProvider(item, ctx);
                if (propertyValueProvider != null) {
                    list.set(idx, new ValueReference(propertyValueProvider.getId()));
                    componentConfig.addPropertyValueProvider(propertyValueProvider);
                }
            }
            list.forEach(item -> buildPropertyValueReferences(componentConfig, item, ctx));
        }
    }

    private static ComponentConfig getOrCreatePropertyValueProvider(Object propertyValue, ConfigBuilderContext ctx) {
        if (!(propertyValue instanceof String)) {
            return null;
        }
        String stringValue = (String) propertyValue;
        ComponentConfig componentConfig = null;
        Matcher matcher = VALUE_REFERENCE.matcher(stringValue);
        if (matcher.matches()) {
            componentConfig = buildValueReference(matcher.group(1), ctx);
        } else if (VALUE_REFERENCE.matcher(stringValue).find()) {
            // Placeholder string.
            componentConfig = createPlaceholderStringValueProvider(stringValue, ctx);
        }
        return componentConfig;
    }

    static void processExtends(ConfigBuilderContext ctx) throws DataFlowConfigurationException {
        Map<String, ComponentConfig> componentConfigs = ctx.componentConfigs;
        Set<String> extendedIds = new HashSet<>();
        for (ComponentConfig config : componentConfigs.values()) {
            if (config.getExtendsId() != null) {
                // Build the chain of extended configs.
                List<ComponentConfig> extendedConfigs = GraphUtil.topologicalSort(config, extendedConfig -> {
                    ComponentConfig extendsConfig = componentConfigs.get(extendedConfig.getExtendsId());
                    return extendsConfig != null ? Collections.singletonList(extendsConfig) : Collections.emptyList();
                });

                // Extend the configs in order from highest to lowest level.
                for (ComponentConfig extendedConfig : extendedConfigs) {
                    if (extendedConfig.getExtendsId() != null && !extendedIds.contains(extendedConfig.getId())) {
                        extendConfig(extendedConfig, componentConfigs);
                        extendedIds.add(extendedConfig.getId());
                    }
                }
                extendConfig(config, componentConfigs);
            }
        }
    }

    static void extendConfig(ComponentConfig config,
            Map<String, ComponentConfig> componentConfigs) throws DataFlowConfigurationException {
        ComponentConfig extendedConfig = componentConfigs.get(config.getExtendsId());
        config.setExtendedConfig(extendedConfig);
    }

    static List<ComponentConfig> buildComponents(Object componentsObj, ConfigBuilderContext ctx)
            throws DataFlowConfigurationException {
        if (!(componentsObj instanceof List)) {
            throw new DataFlowConfigurationException("Components must be a list");
        }

        List<ComponentConfig> retVal = new ArrayList<>();
        List componentsList = (List) componentsObj;
        for (Object componentObj : componentsList) {
            if(!(componentObj instanceof RawComponentConfig)) {
                throw new DataFlowConfigurationException(
                    "Invalid component configuration. Did you forget to add a tag before the name? " + componentObj);
            }
            retVal.add(buildComponent(componentObj, ctx));
        }
        return retVal;
    }

    static Map<String, ComponentConfig> buildInput(Object inputObj,
        DataFlowComponentMetadata componentMetadata, ConfigBuilderContext ctx)
        throws DataFlowConfigurationException {
        if (!(inputObj instanceof Map)) {
            throw new DataFlowConfigurationException("Input must be a map");
        }

        Map<String, ComponentConfig> retVal = new HashMap<>();
        Map<String, Object> inputMap = (Map<String, Object>) inputObj;
        for (Map.Entry<String, Object> input : inputMap.entrySet()) {
            String inputName = input.getKey();
            DataFlowComponentInputMetadata inputMetadata = null;
            if(componentMetadata != null) {
                inputMetadata = componentMetadata.getInputMetadata().stream()
                        .filter(metadata -> metadata.getName().equalsIgnoreCase(inputName))
                        .findAny().orElse(null);
            }
            retVal.put(inputName, buildInputValue(input.getValue(), inputMetadata, ctx));
        }
        return retVal;
    }

    static ComponentConfig buildInputValue(Object inputComponent, DataFlowComponentInputMetadata inputMetadata,
            ConfigBuilderContext ctx) {
        ComponentConfig componentConfig;
        if (inputMetadata != null && inputMetadata.getRawType().equalsIgnoreCase(
                DataFlowInstance.class.getName())) {
            // Return a DataFlowInstanceValueProvider.
            String dataFlowId;
            Set<String> providedValueIds = new HashSet<>();
            String itemValueName = DEFAULT_ITEM_VALUE_NAME;
            if (inputComponent instanceof String) {
                // The value is an id reference.
                dataFlowId = (String) inputComponent;
            } else if (inputComponent instanceof Map || inputComponent instanceof List) {
                // The value is an anonymous child DataFlow.
                dataFlowId = ctx.idGenerator.generate("_childFlow");
                Map<String, Object> rawChildConfig = new HashMap<>();
                if(inputComponent instanceof Map) {
                    rawChildConfig = (Map<String, Object>) inputComponent;
                } else {
                    List components = (List) inputComponent;
                    rawChildConfig.put("Components", components);
                }
                DataFlowConfig childConfig = build(rawChildConfig, new ConfigBuilderContext(ctx.engine));
                childConfig.setId(dataFlowId);
                ctx.childDataFlowConfigs.put(childConfig.getId(), childConfig);
                providedValueIds.addAll(childConfig.getProvidedValueIds());
            } else {
                throw new DataFlowConfigurationException("Invalid child data flow definition: " +
                    inputComponent + " for input " + inputMetadata);
            }
            componentConfig = getOrCreateComponentConfig(
                    ctx.idGenerator.generate("_dataFlow" + dataFlowId + "_component"), ctx);
            componentConfig.setType(DataFlowInstanceValueProvider.class.getSimpleName());
            componentConfig.setInternal(true);
            componentConfig.setProperties(Collections.singletonMap("dataFlowId", dataFlowId));
            providedValueIds.stream()
                .filter(id -> !id.equalsIgnoreCase(itemValueName))
                .map(id -> buildValueReference(id, ctx))
                .filter(valRef -> valRef.getType() == null)
                .forEach(valRef -> valRef.setSink(true));
        } else {
            componentConfig = buildComponent(inputComponent, ctx);
        }
        return componentConfig;
    }

    static ComponentConfig buildComponent(Object componentObj, ConfigBuilderContext ctx)
            throws DataFlowConfigurationException {
        if (!(componentObj instanceof RawComponentConfig)) {
            return buildObjectValueProvider(componentObj, ctx);
        }
        RawComponentConfig rawComponentConfig = (RawComponentConfig) componentObj;

        Map<String, Object> parsedComponent = (Map<String, Object>) componentObj;
        String type = rawComponentConfig.getType();
        if ("Component".equalsIgnoreCase(type) || "ProvidedValue".equalsIgnoreCase(type)) {
            type = null;
        }
        DataFlowComponentMetadata componentMetadata = null;
        if (type != null) {
            try {
                componentMetadata = ctx.engine.getComponentMetadata(type);
            } catch (Exception ex) {
                if(!type.endsWith("ValueProvider") && !type.endsWith("Action")) {
                    // Try appending ValueProvider to the type name.
                    try {
                        componentMetadata = ctx.engine.getComponentMetadata(type + "ValueProvider");
                        type = type + "ValueProvider";
                    } catch(Exception ex2) {
                        try {
                            // Try appending Action to the type name.
                            componentMetadata = ctx.engine.getComponentMetadata(type + "Action");
                            type = type + "Action";
                        } catch(Exception ex3) {
                            throw ex;
                        }
                    }
                }
            }
        }

        boolean internal = false;
        String id = (String) parsedComponent.get("id");
        if (id == null) {
            id = ctx.idGenerator.generate(type);
            internal = true;
        }
        ComponentConfig config = getOrCreateComponentConfig(id, ctx);
        if (type != null) {
            config.setType(type);
        }
        config.setInternal(internal);
        Map<String, Object> properties = new HashMap<>();
        config.setRawConfig(parsedComponent);
        for (Map.Entry<String, Object> entry : parsedComponent.entrySet()) {
            String key = entry.getKey();
            if (key.equalsIgnoreCase("id")) {
                continue;
            } else if (key.equalsIgnoreCase("extends")) {
                config.setExtendsId((String) entry.getValue());
            } else if (key.equalsIgnoreCase("input")) {
                config.setInput(buildInput(entry.getValue(), componentMetadata, ctx));
            } else if(key.equalsIgnoreCase("inputTypes")) {
                if (!(entry.getValue() instanceof Map)) {
                    throw new DataFlowConfigurationException(String.format("%s inputTypes must be a map but was %s",
                            id, entry.getValue()));
                }
                config.setInputTypes((Map<String, String>) entry.getValue());
            } else if(key.equalsIgnoreCase("outputType")) {
                if (!(entry.getValue() instanceof String)) {
                    throw new DataFlowConfigurationException(String.format("%s outputType must be a string but was %s",
                            id, entry.getValue()));
                }
                config.setOutputType((String) entry.getValue());
            } else if (key.equalsIgnoreCase("failOnError")) {
                config.setFailOnError((Boolean) entry.getValue());
            } else if (key.equalsIgnoreCase("eventsEnabled")) {
                config.setEventsEnabled((Boolean) entry.getValue());
            } else if(key.equalsIgnoreCase("abstract")) {
                config.setAbstract((Boolean) entry.getValue());
            } else if (key.equalsIgnoreCase("sink")) {
                config.setSink((Boolean) entry.getValue());
            } else if (componentMetadata != null) {
                // Try to determine if this is an input or a property based on the component metadata
                DataFlowConfigurablePropertyMetadata propertyMetadata = componentMetadata.getPropertyMetadataMap().get(key);
                DataFlowComponentInputMetadata inputMetadata = componentMetadata.getInputMetadata().stream()
                        .filter(in -> in.getName().equalsIgnoreCase(key))
                        .findAny().orElse(null);
                if (propertyMetadata != null || inputMetadata == null) {
                    properties.put(key, entry.getValue());
                } else {
                    ComponentConfig inputComponent = buildInputValue(entry.getValue(), inputMetadata, ctx);
                    config.setInput(key, inputComponent);
                }
            } else {
                properties.put(key, entry.getValue());
            }
        }
        config.setProperties(properties);
        return config;
    }

    static ComponentConfig createPlaceholderStringValueProvider(String str,
            ConfigBuilderContext ctx) {
        Optional<ComponentConfig> existingConfig = ctx.componentConfigs.values().stream()
                .filter(v -> PlaceholderStringValueProviderMetadata.TYPE.equals(v.getType()))
                .filter(v -> str.equals(v.getProperties().get("placeholderString")))
                .findAny();
        if (existingConfig.isPresent()) {
            return existingConfig.get();
        }
        ComponentConfig placeholderStringConfig = getOrCreateComponentConfig(
                ctx.idGenerator.generate(PLACEHOLDER_STRING_ID_PREFIX), ctx);
        placeholderStringConfig.setType(PlaceholderStringValueProviderMetadata.TYPE);
        placeholderStringConfig.setInternal(true);
        Map<String, Object> properties = new HashMap<>();
        properties.put("placeholderString", str);
        placeholderStringConfig.setProperties(properties);
        PlaceholderString placeholderString = new PlaceholderString(str);

        Map<String, ComponentConfig> input = placeholderString.getPlaceholderNames().stream()
                .map(id -> getOrCreateComponentConfig(id, ctx))
                .collect(Collectors.toMap(ComponentConfig::getId, v -> v));
        placeholderStringConfig.setInput(input);
        placeholderStringConfig.setRawConfig(properties);
        // Don't try to resolve property value references on the placeholder string property.
        ctx.propValueRefDisabledComponentIds.add(placeholderStringConfig.getId());
        return placeholderStringConfig;
    }

    static ComponentConfig buildObjectValueProvider(final Object valueProviderObj,
            final ConfigBuilderContext ctx) throws DataFlowConfigurationException {
        if (valueProviderObj instanceof String) {
            String str = (String) valueProviderObj;
            Matcher matcher = VALUE_REFERENCE.matcher(str);
            if (matcher.matches()) {
                return buildValueReference(matcher.group(1), ctx);
            } else if(matcher.find(0)) {
                return createPlaceholderStringValueProvider(str, ctx);
            }
        } else if (valueProviderObj instanceof List) {
            List list = (List) valueProviderObj;
            ComponentConfig config = getOrCreateComponentConfig(ctx.idGenerator.generate("list"), ctx);
            config.setType(ListValueProviderMetadata.TYPE);
            config.setInternal(true);
            int idx = 0;
            Map<String, Object> properties = new HashMap<>();
            List<String> itemKeys = new ArrayList<>();
            Map<String, ComponentConfig> inputs = new HashMap<>();
            for (Object listItem : list) {
                String itemKey = String.valueOf(idx++);
                ComponentConfig itemConfig = buildComponent(listItem, ctx);
                inputs.put(itemKey, itemConfig);
                itemKeys.add(itemKey);
            }
            config.setInput(inputs);
            properties.put("itemKeys", itemKeys);
            config.setProperties(properties);
            return config;
        }

        ComponentConfig config = getOrCreateComponentConfig(ctx.idGenerator.generate("const"), ctx);
        config.setType(ConstValueProviderMetadata.TYPE);
        config.setInternal(true);
        config.setProperties(Collections.singletonMap("value", valueProviderObj));
        return config;
    }

    static ComponentConfig buildValueReference(String reference, ConfigBuilderContext ctx)
            throws DataFlowConfigurationException {
        if (reference.contains(".")) {
            return buildMapEntryValueProviderConfig(reference, ctx);
        } else if (reference.contains("[")) {
            // TODO(thorntonv): Handle list element access in value reference.
            throw new RuntimeException("List element access not implemented");
        } else {
            return getOrCreateComponentConfig(reference, ctx);
        }
    }

    static ComponentConfig buildMapEntryValueProviderConfig(String reference, ConfigBuilderContext ctx) {
        ComponentConfig mapEntryValueProvider = getOrCreateComponentConfig(
                ctx.idGenerator.generate("mapEntry"), ctx);
        mapEntryValueProvider.setInternal(true);
        mapEntryValueProvider.setType(MapEntryValueProviderMetadata.TYPE);
        int dotIdx = reference.indexOf('.');
        String valueId = reference.substring(0, dotIdx);
        String key = reference.substring(dotIdx + 1);
        Map<String, Object> properties = new HashMap<>();
        properties.put("key", key);
        mapEntryValueProvider.setProperties(properties);
        Map<String, ComponentConfig> inputs = new HashMap<>();
        inputs.put("map", getOrCreateComponentConfig(valueId, ctx));
        mapEntryValueProvider.setInput(inputs);
        return mapEntryValueProvider;
    }

    static ComponentConfig getOrCreateComponentConfig(String id, ConfigBuilderContext ctx) {
        ComponentConfig config = ctx.componentConfigs.get(id);
        if (config == null) {
            config = new ComponentConfig();
            config.setId(id);
            ctx.componentConfigs.put(id, config);
        }
        return config;
    }
}
