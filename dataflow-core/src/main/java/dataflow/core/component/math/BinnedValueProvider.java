/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  BinnedValueProvider.java
 */

package dataflow.core.component.math;

import java.util.List;

import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.core.value.DoubleValue;
import dataflow.core.value.FloatValue;
import dataflow.core.value.IntegerValue;
import dataflow.core.value.LongValue;

@DataFlowComponent(description = "A ValueProvider that outputs the bin for the given input value.")
public class BinnedValueProvider {

    private List<Double> bins;

    @DataFlowConfigurable
    public BinnedValueProvider(@DataFlowConfigProperty(
            description = "A list of bin boundaries. Values less than the first boundary will be assigned bin 0. " +
                    "Values at the boundary will be assigned to the next higher bin."
    ) List<Double> bins) {
        this.bins = bins;
    }

    @OutputValue(description = "The bin for the specific value")
    public int getValue(@InputValue(
            supportedTypes = {IntegerValue.TYPE, LongValue.TYPE, FloatValue.TYPE, DoubleValue.TYPE},
            description = "The value that will be used to determine the bin to provide")
            double value) {
        for (int idx = 0; idx < bins.size(); idx++) {
            if (bins.get(idx) > value) {
                return idx;
            }
        }
        return bins.size();
    }
}
