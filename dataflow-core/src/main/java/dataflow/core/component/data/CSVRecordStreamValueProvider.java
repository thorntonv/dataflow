/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  CSVRecordStreamValueProvider.java
 */

package dataflow.core.component.data;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.OutputValue;
import dataflow.core.datasource.DataSource;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.ValueRegistry;
import dataflow.core.engine.ValueType;
import dataflow.core.exception.DataFlowConfigurationException;

@DataFlowComponent
public class CSVRecordStreamValueProvider {

    private static final boolean PARALLEL_FALSE = false;
    private CSVFormat csvFormat;
    private final RecordSchema schema;
    private final boolean failOnInvalidRecord;
    private final boolean skipInvalidRecords;
    private final DataSource dataSource;
    private final List<RecordValidator> recordValidators;
    private final DataFlowEngine engine;
    private final boolean dedupeStrings;
    private Map<String, String> dedupedStrings = new HashMap<>();

    @DataFlowConfigurable
    public CSVRecordStreamValueProvider(@DataFlowConfigProperty DataSource source,
            @DataFlowConfigProperty String schema,
            @DataFlowConfigProperty(required = false) List<RecordValidator> recordValidators,
            @DataFlowConfigProperty(required = false) String delimiter,
            @DataFlowConfigProperty(required = false) Boolean withFirstRecordAsHeader,
            @DataFlowConfigProperty(required = false) Boolean failOnInvalidRecord,
            @DataFlowConfigProperty(required = false) Boolean skipInvalidRecords,
            @DataFlowConfigProperty(required = false) Boolean dedupeStrings) {
        this.dataSource = source;
        this.schema = new RecordSchema(schema);
        this.recordValidators = recordValidators != null ? recordValidators : Collections.emptyList();
        this.failOnInvalidRecord = failOnInvalidRecord != null ? failOnInvalidRecord : true;
        this.skipInvalidRecords = skipInvalidRecords != null ? skipInvalidRecords : true;
        this.dedupeStrings = dedupeStrings != null ? dedupeStrings : true;
        this.engine = DataFlowExecutionContext.getCurrentExecutionContext().getEngine();

        csvFormat = CSVFormat.DEFAULT;
        if (delimiter != null) {
            if (delimiter.length() != 1) {
                throw new DataFlowConfigurationException("Invalid delimiter " + delimiter);
            }
            csvFormat = csvFormat.withDelimiter(delimiter.charAt(0));
        }
        if (withFirstRecordAsHeader != null && withFirstRecordAsHeader) {
            csvFormat = csvFormat.withFirstRecordAsHeader();
        }
    }

    @OutputValue
    public Stream<Map<String, Object>> getValue() throws IOException {
        CSVParser csvParser = new CSVParser(new InputStreamReader(dataSource.getInputStream()), csvFormat);
        final int columnCount = schema.getColumnNames().size();
        final List<String> columnNames = schema.getColumnNames();
        return StreamSupport.stream(csvParser.spliterator(), PARALLEL_FALSE).map(csvRecord -> {
            Map<String, Object> record = new HashMap<>();
            if (csvRecord.size() != columnCount) {
                if (failOnInvalidRecord) {
                    throw new RuntimeException("Invalid record: " + csvRecord);
                }
                if (skipInvalidRecords) {
                    return null;
                }
            }

            for (int idx = 0; idx < columnCount; idx++) {
                String columnName = columnNames.get(idx);
                String columnStrValue = csvRecord.get(idx);
                if (columnStrValue != null) {
                    String type = schema.getColumnValueType(columnName);
                    Object columnValue = engine.convert(columnStrValue, ValueRegistry.STRING_VALUE_TYPE,
                            ValueType.fromString(type));

                    if (dedupeStrings && columnValue != null && columnValue.getClass() == String.class) {
                        // Dedupe the string.
                        String dedupedString = dedupedStrings.get(columnValue);
                        if (dedupedString == null) {
                            dedupedString = (String) columnValue;
                            dedupedStrings.put(dedupedString, dedupedString);
                        }
                        columnValue = dedupedString;
                    }

                    record.put(columnName, columnValue);
                }
            }
            if (!recordValidators.stream().allMatch(validator -> validator.isValid(record, schema))) {
                if (failOnInvalidRecord) {
                    throw new RuntimeException("Record validation failed: " + csvRecord);
                }
                if (skipInvalidRecords) {
                    return null;
                }
            }
            return record;
        }).filter(Objects::nonNull);
    }
}
