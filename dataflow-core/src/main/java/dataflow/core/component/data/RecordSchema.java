/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  RecordSchema.java
 */

package dataflow.core.component.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dataflow.core.exception.DataFlowConfigurationException;

public class RecordSchema {

    private final List<String> columnNames;
    private final Map<String, String> columnValueTypes;

    public RecordSchema(String schema) {
        this.columnNames = new ArrayList<>();
        this.columnValueTypes = new HashMap<>();
        String[] columnSchemas = schema.split(",");
        for (String columnSchema : columnSchemas) {
            String[] parts = columnSchema.split(":");
            if (parts.length != 2) {
                throw new DataFlowConfigurationException("Invalid schema " + schema);
            }
            String columnName = parts[0].trim();
            String columnType = parts[1].trim();
            columnNames.add(columnName);
            columnValueTypes.put(columnName, columnType);
        }
    }

    public RecordSchema(List<String> columnNames, Map<String, String> columnValueTypes) {
        this.columnNames = columnNames;
        this.columnValueTypes = columnValueTypes;
    }

    public List<String> getColumnNames() {
        return columnNames;
    }

    public Map<String, String> getColumnValueTypes() {
        return columnValueTypes;
    }

    public String getColumnValueType(String columnName) {
        return columnValueTypes.get(columnName);
    }
}
