/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  StreamValueProviders.java
 */

package dataflow.core.component.stream;

import dataflow.core.engine.DataFlowEngine;

public class StreamValueProviders {

    public static void register(DataFlowEngine engine) {
        engine.registerComponent(MappedStreamValueProviderMetadata.INSTANCE,
                new MappedStreamValueProviderBuilder(engine));
        engine.registerComponent(FilteredStreamValueProviderMetadata.INSTANCE,
                new FilteredStreamValueProviderBuilder(engine));
        engine.registerComponent(StreamListCollectorValueProviderMetadata.INSTANCE,
                new StreamListCollectorValueProviderBuilder(engine));
        engine.registerComponent(StreamForEachMetadata.INSTANCE, new StreamForEachBuilder(engine));
    }
}
