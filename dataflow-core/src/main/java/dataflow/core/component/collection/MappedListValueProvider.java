/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MappedListValueProvider.java
 */

package dataflow.core.component.collection;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;

@DataFlowComponent
public class MappedListValueProvider {

    private final boolean parallel;
    private final String itemValueName;

    @DataFlowConfigurable
    public MappedListValueProvider(
            @DataFlowConfigProperty(required = false, description = "The name of the item value in the mapping DataFlow")
                    String itemValueName,
            @DataFlowConfigProperty(required = false) Boolean parallel) {
        this.itemValueName = itemValueName != null ? itemValueName : "_item";
        this.parallel = parallel != null ? parallel : false;
    }

    @OutputValue
    public List<?> getValue(@InputValue List<?> list,
            @InputValue DataFlowInstance mappingDataFlow) {
        Stream<?> stream = list.stream();
        if (parallel) {
            // TODO(thorntonv): Support parallel execution outside of the common fork-join pool.
            stream = list.stream().parallel();
        }
        final DataFlowExecutionContext executionContext = DataFlowExecutionContext.getCurrentExecutionContext();
        if (executionContext == null) {
            throw new RuntimeException("Execution context is not set");
        }
        return stream.map(item -> executionContext.execute(() -> {
            mappingDataFlow.setValue(itemValueName, item);
            mappingDataFlow.execute();
            return mappingDataFlow.getOutput();
        })).collect(Collectors.toList());
    }
}
