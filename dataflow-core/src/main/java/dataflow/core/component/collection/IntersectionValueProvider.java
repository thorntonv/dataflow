/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  IntersectionValueProvider.java
 */

package dataflow.core.component.collection;

import java.util.Collection;
import java.util.HashSet;

import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.DataFlowConfigurable;

@DataFlowComponent
public class IntersectionValueProvider {

    @DataFlowConfigurable
    public IntersectionValueProvider() {}

    @OutputValue
    public Collection getValue(@InputValue Collection collection1, @InputValue Collection collection2) {
        Collection result = new HashSet(collection1);
        result.retainAll(collection2);
        return result;
    }
}
