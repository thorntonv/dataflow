/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  CollectionValueProviders.java
 */

package dataflow.core.component.collection;

import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.codegen.ListValueProviderCodeGenerator;

public class CollectionValueProviders {

    public static void register(DataFlowEngine engine) {
        engine.registerComponent(MapValueProviderMetadata.INSTANCE, new MapValueProviderBuilder(engine));
        engine.registerComponent(MapEntryValueProviderMetadata.INSTANCE, new MapEntryValueProviderBuilder(engine));
        engine.registerComponent(CollectionSizeValueProviderMetadata.INSTANCE, new CollectionSizeValueProviderBuilder(engine));
        engine.registerComponent(ListValueProviderMetadata.INSTANCE, new ListValueProviderBuilder(engine),
                new ListValueProviderCodeGenerator());
        engine.registerComponent(ListItemValueProviderMetadata.INSTANCE, new ListItemValueProviderBuilder(engine));
        engine.registerComponent(MappedListValueProviderMetadata.INSTANCE, new MappedListValueProviderBuilder(engine));
        engine.registerComponent(ListForEachMetadata.INSTANCE, new ListForEachBuilder(engine));
        engine.registerComponent(
                IntersectionValueProviderMetadata.INSTANCE, new IntersectionValueProviderBuilder(engine));
        engine.registerComponent(UnionValueProviderMetadata.INSTANCE, new UnionValueProviderBuilder(engine));
    }
}
