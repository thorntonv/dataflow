/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  CoreComponents.java
 */

package dataflow.core.component;

import dataflow.core.engine.DataFlowEngine;
import dataflow.core.component.collection.CollectionValueProviders;
import dataflow.core.component.data.DataValueProviders;
import dataflow.core.component.math.MathValueProviders;
import dataflow.core.component.stream.StreamValueProviders;

public class CoreComponents {

    public static void register(DataFlowEngine engine) {
        engine.registerComponent(ConstValueProviderMetadata.INSTANCE, new ConstValueProviderBuilder(engine));
        engine.registerComponent(ExpressionValueProviderMetadata.INSTANCE,
                new ExpressionValueProviderBuilder(engine),
                new ExpressionValueProviderCodeGenerator());
        engine.registerComponent(FallbackValueProviderMetadata.INSTANCE, new FallbackValueProviderBuilder(engine));
        engine.registerComponent(PlaceholderStringValueProviderMetadata.INSTANCE,
                new PlaceholderStringValueProviderBuilder(engine));
        engine.registerComponent(DataFlowInstanceValueProviderMetadata.INSTANCE,
                new DataFlowInstanceValueProviderBuilder(engine));
        CollectionValueProviders.register(engine);
        DataValueProviders.register(engine);
        MathValueProviders.register(engine);
        StreamValueProviders.register(engine);
    }
}
