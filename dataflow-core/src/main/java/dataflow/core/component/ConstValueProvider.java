/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ConstValueProvider.java
 */

package dataflow.core.component;

import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.OutputValue;
import dataflow.core.value.BooleanValue;
import dataflow.core.value.DoubleValue;
import dataflow.core.value.FloatValue;
import dataflow.core.value.IntegerValue;
import dataflow.core.value.LongValue;

@DataFlowComponent(description = "A ValueProvider that provides a given constant value.")
public class ConstValueProvider {

    private Object value;

    @DataFlowConfigurable
    public ConstValueProvider(@DataFlowConfigProperty(
            supportedTypes = {
                    BooleanValue.TYPE,
                    IntegerValue.TYPE,
                    LongValue.TYPE,
                    FloatValue.TYPE,
                    DoubleValue.TYPE,
                    "string",
            },
            description = "The value that will be provided") final Object value) {
        this.value = value;
    }

    @OutputValue(
            supportedTypes = {
                    BooleanValue.TYPE,
                    IntegerValue.TYPE,
                    LongValue.TYPE,
                    FloatValue.TYPE,
                    DoubleValue.TYPE,
                    "string",
            },
            description = "The specified constant value")
    public Object getValue() {
        return value;
    }

}
