/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowInstanceValueProvider.java
 */

package dataflow.core.component;

import java.util.Map;

import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValues;
import dataflow.annotation.OutputValue;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.ThreadLocalDataFlowInstance;

/**
 * A ValueProvider that provides a {@link ThreadLocalDataFlowInstance} for a given DataFlow id.
 */
@DataFlowComponent
public class DataFlowInstanceValueProvider implements AutoCloseable {

    private final ThreadLocalDataFlowInstance dataFlowInstance;
    private boolean providedValuesSet = false;

    @DataFlowConfigurable
    public DataFlowInstanceValueProvider(@DataFlowConfigProperty String dataFlowId) {
        this.dataFlowInstance = new ThreadLocalDataFlowInstance(dataFlowId);
    }

    @OutputValue
    public DataFlowInstance getValue(@InputValues Map<String, Object> values) {
        if (!providedValuesSet) {
            DataFlowInstance parentInstance = DataFlowExecutionContext.getCurrentExecutionContext().getInstance();
            dataFlowInstance.getConfig().getProvidedValueIds().stream()
                    .filter(id -> !id.startsWith("_"))
                    .forEach(id -> dataFlowInstance.setValue(id, parentInstance.getValue(id)));
            providedValuesSet = true;
        }
        if (values != null) {
            values.forEach(dataFlowInstance::setValue);
        }
        return dataFlowInstance;
    }

    @Override
    public void close() throws Exception {
        dataFlowInstance.close();
    }
}
