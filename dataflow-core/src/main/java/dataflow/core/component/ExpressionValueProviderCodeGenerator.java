/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ExpressionValueProviderCodeGenerator.java
 */

package dataflow.core.component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import dataflow.annotation.processor.IndentPrintWriter;
import dataflow.core.engine.PlaceholderString;
import dataflow.core.engine.codegen.CodeGenerationUtil;
import dataflow.core.engine.codegen.DataFlowCodeGenerationContext.ComponentCodeGenerationContext;
import dataflow.core.engine.codegen.DefaultComponentCodeGenerator;

public class ExpressionValueProviderCodeGenerator extends DefaultComponentCodeGenerator {

    public static final String EXPRESSION_PROPERTY_NAME = "expression";

    private static final String[] IMPORTS = new String[]{
            String.format("static %s.abs", Math.class.getName()),
            String.format("static %s.min", Math.class.getName()),
            String.format("static %s.pow", Math.class.getName())
    };

    public ExpressionValueProviderCodeGenerator() {
        super(null);
    }

    @Override
    public Set<String> getImports() {
        Set<String> imports = new HashSet<>(Arrays.asList(IMPORTS));
        imports.addAll(super.getImports());
        return imports;
    }

    @Override
    public void writeExecuteStatements(final IndentPrintWriter out, final ComponentCodeGenerationContext context) {
        Object exprObj = context.getComponentConfig().getProperties().get(EXPRESSION_PROPERTY_NAME);
        String expr = (String) exprObj;
        if(expr == null || expr.isEmpty()) {
            throw new RuntimeException("Expression not defined for " + context.getComponentConfig().getId());
        }
        expr = replacePlaceholders(expr, context);
        out.printf("%s_value = %s;%n", context.getVarNamePrefix(), expr);
    }
}
