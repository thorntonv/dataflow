/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  CodeGenerationUtil.java
 */

package dataflow.core.engine.codegen;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.codegen.DataFlowCodeGenerationContext.ComponentCodeGenerationContext;
import dataflow.core.parser.ComponentConfig;
import dataflow.core.component.ConstValueProviderMetadata;
import dataflow.core.util.StringUtil;
import dataflow.core.value.BooleanValue;
import dataflow.core.value.DoubleValue;
import dataflow.core.value.FloatValue;
import dataflow.core.value.IntegerValue;
import dataflow.core.value.LongValue;

/**
 * Utility methods for code generation.
 */
@SuppressWarnings("WeakerAccess")
public class CodeGenerationUtil {

    public static final Map<String, String> PRIMITIVE_TYPE_CONVERSION_PATTERN_MAP = new HashMap<>();
    public static final Map<String, String> PRIMITIVE_TYPE_MAP = new HashMap<>();

    private static final Set<Class<?>> SIMPLE_CONST_CLASSES = new HashSet<>();
    static {
        PRIMITIVE_TYPE_CONVERSION_PATTERN_MAP.put("boolean", "%s.toBoolean()");
        PRIMITIVE_TYPE_CONVERSION_PATTERN_MAP.put("int", "%s.toInt()");
        PRIMITIVE_TYPE_CONVERSION_PATTERN_MAP.put("long", "%s.toLong()");
        PRIMITIVE_TYPE_CONVERSION_PATTERN_MAP.put("float", "%s.toFloat()");
        PRIMITIVE_TYPE_CONVERSION_PATTERN_MAP.put("double", "%s.toDouble()");

        PRIMITIVE_TYPE_MAP.put("boolean", BooleanValue.class.getName());
        PRIMITIVE_TYPE_MAP.put("int", IntegerValue.class.getName());
        PRIMITIVE_TYPE_MAP.put("long", LongValue.class.getName());
        PRIMITIVE_TYPE_MAP.put("float", FloatValue.class.getName());
        PRIMITIVE_TYPE_MAP.put("double", DoubleValue.class.getName());

        SIMPLE_CONST_CLASSES.addAll(Arrays.asList(int.class, Integer.class, long.class, Long.class,
                float.class, Float.class, double.class, Double.class, String.class));
    }

    public static String getVarName(String id) {
        return "_" + id.replaceAll("-", "_");
    }

    public static String getValueReferenceExpression(ComponentCodeGenerationContext context) {
        return getValueReferenceExpression(context.getComponentConfig(), context);
    }

    public static String getValueReferenceExpression(ComponentConfig config, ComponentCodeGenerationContext context) {
        Object constValue = context.getDataFlowContext().getConstValue(config.getId());
        if(constValue != null) {
            return simpleConstValueToString(constValue);
        }
        return getVarName(config.getId()) + "_value";
    }

    public static String simpleConstValueToString(Object constValue) {
        if (constValue instanceof String) {
            return "\"" + constValue + "\"";
        }
        return constValue.toString();
    }

    public static boolean isConstant(ComponentConfig config) {
        return ConstValueProviderMetadata.TYPE.equals(config.getType());
    }

    public static boolean isProvidedValue(ComponentConfig config) {
        return config.getType() == null;
    }

    public static boolean isSimpleConstant(ComponentConfig config) {
        Object constValue = config.getProperties().get("value");
        return isConstant(config) && SIMPLE_CONST_CLASSES.contains(constValue.getClass());
    }

    /**
     * Returns the code for a statement that sets the value of the given component to the given
     * expression value.
     * @param context the context for the component whose value will be set
     * @param valueExpr the expression to use to set the value
     * @param valueExprType the type of the value expression or null if the type is not known at compile time
     */
    public static String setValueStmt(ComponentCodeGenerationContext context, String valueExpr,
            String valueExprType) {
        String toType = context.getOutputType();
        valueExpr = valueConvertExpr(valueExprType, toType, valueExpr, context.getDataFlowContext());
        if (!context.hasPrimitiveOutputType()) {
            return String.format("%s_value = %s;%n", context.getVarNamePrefix(), valueExpr);
        } else if ("null".equals(valueExpr)) {
            // Clear the value rather than setting the primitive wrapper to null.
            return String.format("%s_value.clear();%n", context.getVarNamePrefix());
        } else {
            return String.format("%s_value.setValue(%s);%n", context.getVarNamePrefix(), valueExpr);
        }
    }

    public static String valueConvertExpr(String fromType, String toType, String expr,
            DataFlowCodeGenerationContext context) {
        if (fromType == null || toType == null) {
            return expr;
        }
        if (fromType.equals(PRIMITIVE_TYPE_MAP.get(toType))) {
            // Avoid engine conversion when converting value classes (eg. IntegerValue, FloatValue, etc) to
            // primitives.
            return String.format(PRIMITIVE_TYPE_CONVERSION_PATTERN_MAP.get(toType), expr);
        }
        Class<?> fromClass = context.getEngine().getValueTypeClass(fromType);
        Class<?> toClass = context.getEngine().getValueTypeClass(toType);
        Class<?>[] fromTypeGenericParams = context.getEngine().getGenericTypeClasses(fromType);
        Class<?>[] targetTypeGenericParams = context.getEngine().getGenericTypeClasses(toType);
        if (!toClass.isAssignableFrom(fromClass) || (targetTypeGenericParams.length > 0 &&
                fromTypeGenericParams.length > 0 && !Arrays.equals(fromTypeGenericParams, targetTypeGenericParams))) {
//            List<String> genericParamClasses = Arrays.stream(targetTypeGenericParams)
//                    .map(typeClass -> typeClass.getName() + ".class").collect(Collectors.toList());
//            String genericParamClassesStr = !genericParamClasses.isEmpty() ?
//                    ", " + StringUtil.join(genericParamClasses, ", ") : "";
            expr = String.format("engine.convert(%s, ValueType.fromString(\"%s\"), ValueType.fromString(\"%s\"))",
                    expr, fromType, toType);
        }
        return expr;
    }

    public static String valueIsSetExpr(ComponentCodeGenerationContext context) {
        return valueIsSetExpr(String.format("%s_value", context.getVarNamePrefix()), context);
    }

    public static String valueIsSetExpr(String valueExpr, ComponentCodeGenerationContext context) {
        if (isConstant(context.getComponentConfig())) {
            return "true";
        }
        return !context.hasPrimitiveOutputType() ?
                String.format("%s != null", valueExpr) :
                String.format("%s != null && %s.isSet()", valueExpr, valueExpr);
    }

    public static String getWaitForCompletableFutureExpression(String completableFutureExpr) {
        return String.format("waitFor(%s)", completableFutureExpr);
    }

    public static List<String> getInProgressAsyncInputIds(ComponentCodeGenerationContext context) {
        return context.getComponentConfig().getDependencies().values().stream()
                .map(ComponentConfig::getId)
                .filter(context.getDataFlowContext().getInProgressAsyncComponentIds()::contains)
                .collect(Collectors.toList());
    }

    /**
     * Returns an expression for a {@link CompletableFuture} that is completed when all of the async inputs of the
     * component are complete.
     */
    public static String getAsyncInputCompletableFutureExpression(
        ComponentCodeGenerationContext context) {
        List<String> futureVarNames = getInProgressAsyncInputIds(context).stream()
                .map(id -> getVarName(id) + "_future")
                .collect(Collectors.toList());
        StringBuilder builder = new StringBuilder();
        builder.append("CompletableFuture.allOf(");
        StringUtil.join(builder, futureVarNames, ", ");
        builder.append(")");
        return builder.toString();
    }

    public static boolean hasCodeGenerator(ComponentConfig config, DataFlowEngine engine) {
        return engine.getComponentCodeGenerator(config.getType()) != null;
    }
}
