/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  StringConverters.java
 */

package dataflow.core.engine.converter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;
import dataflow.annotation.ValueConverter;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.ValueRegistry;
import dataflow.core.engine.ValueType;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.util.ArrayUtil;
import dataflow.core.value.DoubleValue;
import dataflow.core.value.FloatValue;
import dataflow.core.value.IntegerValue;
import dataflow.core.value.LongValue;

@SuppressWarnings("unchecked")
public class StringConverters {

    public static void register(DataFlowEngine engine) {
        engine.registerComponent(String_to_Double_converterMetadata.INSTANCE,
                new String_to_Double_converterBuilder(engine));
        engine.getValueRegistry().register(String_to_Double_converterMetadata.INSTANCE, new String_to_Double_converter());

        engine.registerComponent(String_to_PrimitiveDouble_converterMetadata.INSTANCE,
                new String_to_PrimitiveDouble_converterBuilder(engine));
        engine.getValueRegistry().register(String_to_PrimitiveDouble_converterMetadata.INSTANCE,
                new String_to_Double_converter());

        engine.registerComponent(String_to_DoubleArray_converterMetadata.INSTANCE,
                new String_to_DoubleArray_converterBuilder(engine));
        engine.getValueRegistry().register(String_to_DoubleArray_converterMetadata.INSTANCE,
                new String_to_DoubleArray_converter());

        engine.registerComponent(String_to_DoubleValue_converterMetadata.INSTANCE,
                new String_to_DoubleValue_converterBuilder(engine));
        engine.getValueRegistry().register(String_to_DoubleValue_converterMetadata.INSTANCE,
                new String_to_DoubleValue_converter());

        engine.registerComponent(String_to_Enum_converterMetadata.INSTANCE,
                new String_to_Enum_converterBuilder(engine));
        engine.getValueRegistry().register(String_to_Enum_converterMetadata.INSTANCE, new String_to_Enum_converter());

        engine.registerComponent(String_to_Float_converterMetadata.INSTANCE,
                new String_to_Float_converterBuilder(engine));
        engine.getValueRegistry().register(String_to_Float_converterMetadata.INSTANCE,
                new String_to_Float_converter());

        engine.registerComponent(String_to_PrimitiveFloat_converterMetadata.INSTANCE,
                new String_to_PrimitiveFloat_converterBuilder(engine));
        engine.getValueRegistry().register(String_to_PrimitiveFloat_converterMetadata.INSTANCE,
                new String_to_Float_converter());

        engine.registerComponent(String_to_FloatValue_converterMetadata.INSTANCE,
                new String_to_FloatValue_converterBuilder(engine));
        engine.getValueRegistry().register(String_to_FloatValue_converterMetadata.INSTANCE,
                new String_to_FloatValue_converter());

        engine.registerComponent(String_to_Integer_converterMetadata.INSTANCE,
                new String_to_Integer_converterBuilder(engine));
        engine.getValueRegistry().register(String_to_Integer_converterMetadata.INSTANCE,
                new String_to_Integer_converter());

        engine.registerComponent(String_to_int_converterMetadata.INSTANCE,
                new String_to_int_converterBuilder(engine));
        engine.getValueRegistry().register(String_to_int_converterMetadata.INSTANCE,
                new String_to_Integer_converter());

        engine.registerComponent(String_to_IntegerValue_converterMetadata.INSTANCE,
                new String_to_IntegerValue_converterBuilder(engine));
        engine.getValueRegistry().register(String_to_IntegerValue_converterMetadata.INSTANCE,
                new String_to_IntegerValue_converter());

        engine.registerComponent(String_to_List_converterMetadata.INSTANCE,
                new String_to_List_converterBuilder(engine));
        engine.getValueRegistry().register(String_to_List_converterMetadata.INSTANCE,
                new String_to_List_converter(engine.getValueRegistry()));

        engine.registerComponent(String_to_Long_converterMetadata.INSTANCE,
                new String_to_Long_converterBuilder(engine));
        engine.getValueRegistry().register(String_to_Long_converterMetadata.INSTANCE, new String_to_Long_converter());

        engine.registerComponent(String_to_PrimitiveLong_converterMetadata.INSTANCE,
                new String_to_PrimitiveLong_converterBuilder(engine));
        engine.getValueRegistry().register(String_to_PrimitiveLong_converterMetadata.INSTANCE,
                new String_to_Long_converter());

        engine.registerComponent(String_to_LongValue_converterMetadata.INSTANCE,
                new String_to_LongValue_converterBuilder(engine));
        engine.getValueRegistry().register(String_to_LongValue_converterMetadata.INSTANCE,
                new String_to_LongValue_converter());

        engine.registerComponent(String_to_Date_converterMetadata.INSTANCE,
            new String_to_Date_converterBuilder(engine));
        engine.getValueRegistry().register(String_to_Date_converterMetadata.INSTANCE,
            new String_to_Date_converter());
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class String_to_Double_converter implements ValueConverterFunction<String, Double> {

        @DataFlowConfigurable
        public String_to_Double_converter() {}

        @Override
        @OutputValue
        public Double getValue(@InputValue final String value) {
            return Double.parseDouble(value);
        }
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class String_to_PrimitiveDouble_converter  {

        @DataFlowConfigurable
        public String_to_PrimitiveDouble_converter() {}

        @OutputValue
        public double getValue(@InputValue final String value) { return Double.parseDouble(value); }
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class String_to_DoubleArray_converter implements ValueConverterFunction<String, double[]> {

        @DataFlowConfigurable
        public String_to_DoubleArray_converter() {}

        @Override
        @OutputValue
        public double[] getValue(@InputValue final String value) {
            return ArrayUtil.doubleArrayFromString(value);
        }
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class String_to_DoubleValue_converter implements ValueConverterFunction<String, DoubleValue> {

        @DataFlowConfigurable
        public String_to_DoubleValue_converter() {}

        @Override
        @OutputValue
        public DoubleValue getValue(@InputValue final String value) {
            DoubleValue result = new DoubleValue();
            result.fromString(value);
            return result;
        }
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class String_to_Enum_converter implements ValueConverterFunction<String, Enum> {

        private final Class<Enum> toType;

        @DataFlowConfigurable
        public String_to_Enum_converter(@DataFlowConfigProperty ValueType toType) {
            try {
                this.toType = (Class<Enum>) Class.forName(toType.getType());
            } catch (ClassNotFoundException e) {
                throw new DataFlowConfigurationException("Invalid class " + toType, e);
            }
        }

        String_to_Enum_converter() {
            this.toType = null;
        }

        @OutputValue
        public Enum getValue(@InputValue final String value) {
            return Enum.valueOf(toType, value);
        }

        @Override
        public Enum getValue(final String value, ValueType toValueType) {
            try {
                Class<Enum> toClass = (Class<Enum>) Class.forName(toValueType.getType());
                return Enum.valueOf(toClass, value);
            } catch (ClassNotFoundException e) {
                throw new IllegalArgumentException("Invalid class " + toValueType, e);
            }
        }
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class String_to_Float_converter implements ValueConverterFunction<String, Float> {

        @DataFlowConfigurable
        public String_to_Float_converter() {}

        @Override
        @OutputValue
        public Float getValue(@InputValue final String value) {
            return Float.parseFloat(value);
        }
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class String_to_PrimitiveFloat_converter {

        @DataFlowConfigurable
        public String_to_PrimitiveFloat_converter() {}

        @OutputValue
        public float getValue(@InputValue final String value) { return Float.parseFloat(value); }
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class String_to_FloatValue_converter implements ValueConverterFunction<String, FloatValue> {

        @DataFlowConfigurable
        public String_to_FloatValue_converter() {}

        @Override
        @OutputValue
        public FloatValue getValue(@InputValue final String value) {
            FloatValue result = new FloatValue();
            result.fromString(value);
            return result;
        }
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class String_to_Integer_converter implements ValueConverterFunction<String, Integer> {

        @DataFlowConfigurable
        public String_to_Integer_converter() {}

        @Override
        @OutputValue
        public Integer getValue(@InputValue final String value) {
            return Integer.parseInt(value);
        }
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class String_to_int_converter {

        @DataFlowConfigurable
        public String_to_int_converter() {}

        @OutputValue
        public int getValue(@InputValue final String value) { return Integer.parseInt(value); }
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class String_to_IntegerValue_converter implements ValueConverterFunction<String, IntegerValue> {

        @DataFlowConfigurable
        public String_to_IntegerValue_converter() {}

        @Override
        @OutputValue
        public IntegerValue getValue(@InputValue final String value) {
            IntegerValue result = new IntegerValue();
            result.fromString(value);
            return result;
        }
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class String_to_List_converter implements ValueConverterFunction<String, List> {

        private static final String DEFAULT_DELIMITER = ",";
        private final ValueRegistry valueRegistry;
        private final ValueType toValueType;
        private final String delimiter;

        @DataFlowConfigurable
        public String_to_List_converter(
                @DataFlowConfigProperty(required = false) final ValueType toValueType,
                @DataFlowConfigProperty(required = false) final String delimiter) {
            this.valueRegistry = DataFlowExecutionContext.getCurrentExecutionContext().getEngine().getValueRegistry();
            this.toValueType = toValueType;
            if (toValueType != null && toValueType.getTypeGenericParams().size() > 1) {
                throw new DataFlowConfigurationException("Only a single target generic type param can be specified");
            }
            this.delimiter = delimiter != null ? delimiter : DEFAULT_DELIMITER;
        }

        String_to_List_converter(ValueRegistry valueRegistry) {
            this.valueRegistry = valueRegistry;
            this.toValueType = null;
            this.delimiter = DEFAULT_DELIMITER;
        }

        public String getOutputType() {
            return toValueType.toString();
        }

        @OutputValue
        public List getValue(@InputValue final String value) {
            return getValue(value, toValueType);
        }

        @Override
        public List getValue(final String value, final ValueType toValueType) {
            String[] elements = value.split(delimiter);
            List list = new ArrayList<>(elements.length);
            ValueType listItemType = toValueType.getTypeGenericParams().size() > 0 ?
                    toValueType.getTypeGenericParams().get(0): null;
            for (final String elementStr : elements) {
                String element = elementStr;
                if (element.charAt(0) == '[') {
                    element = element.substring(1);
                }
                if (element.charAt(element.length() - 1) == ']') {
                    element = element.substring(0, element.length() - 1);
                }
                if (listItemType != null) {
                    list.add(valueRegistry.convert(element, ValueRegistry.STRING_VALUE_TYPE, listItemType));
                } else {
                    list.add(element);
                }
            }
            return list;
        }
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class String_to_Long_converter implements ValueConverterFunction<String, Long> {

        @DataFlowConfigurable
        public String_to_Long_converter() {}

        @Override
        @OutputValue
        public Long getValue(@InputValue final String value) {
            return Long.parseLong(value);
        }
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class String_to_PrimitiveLong_converter {

        @DataFlowConfigurable
        public String_to_PrimitiveLong_converter() {}

        @OutputValue
        public long getValue(@InputValue final String value) {
            return Long.parseLong(value);
        }
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class String_to_LongValue_converter implements ValueConverterFunction<String, LongValue> {

        @DataFlowConfigurable
        public String_to_LongValue_converter() {}

        @Override
        @OutputValue
        public LongValue getValue(@InputValue final String value) {
            LongValue result = new LongValue();
            result.fromString(value);
            return result;
        }
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class String_to_Date_converter implements ValueConverterFunction<String, Date> {

        @DataFlowConfigurable
        public String_to_Date_converter() {}

        @Override
        @OutputValue
        public Date getValue(@InputValue final String value) {
            try {
                return Date.from(ZonedDateTime.parse(value).toInstant());
            } catch (DateTimeParseException ex) {}
            try {
                return Date.from(LocalDateTime.parse(value).atZone(ZoneOffset.UTC).toInstant());
            } catch (DateTimeParseException ex) {}
            try {
                return Date.from(LocalDate.parse(value).atStartOfDay(ZoneOffset.UTC).toInstant());
            } catch (DateTimeParseException ex) {}
            throw new RuntimeException("Unable to parse date string " + value);
        }
    }
}
