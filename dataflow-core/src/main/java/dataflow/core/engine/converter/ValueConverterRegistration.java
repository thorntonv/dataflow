/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ValueConverterRegistration.java
 */

package dataflow.core.engine.converter;

import java.util.Objects;

import dataflow.annotation.metadata.DataFlowComponentMetadata;
import dataflow.core.engine.ValueType;

public final class ValueConverterRegistration {

    public static final class ValueConverterRegistrationKey {

        private ValueType fromType;
        private ValueType toType;

        public ValueConverterRegistrationKey(final ValueType fromType, final ValueType toType) {
            this.fromType = fromType;
            this.toType = toType;
        }

        public ValueType getFromType() {
            return fromType;
        }

        public ValueType getToType() {
            return toType;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;
            final ValueConverterRegistrationKey that = (ValueConverterRegistrationKey) o;
            return Objects.equals(fromType, that.fromType) &&
                    Objects.equals(toType, that.toType);
        }

        @Override
        public int hashCode() {
            return Objects.hash(fromType, toType);
        }

        @Override
        public String toString() {
            return "ValueConverterRegistrationKey{" +
                    "fromType=" + fromType +
                    ", toType=" + toType +
                    '}';
        }
    }

    private ValueConverterRegistrationKey key;
    private DataFlowComponentMetadata valueProviderMetadata;
    private ValueConverterFunction<?, ?> converterFunction;

    public ValueConverterRegistration(final ValueConverterRegistrationKey key,
            final DataFlowComponentMetadata valueProviderMetadata, final ValueConverterFunction<?, ?> converterFunction) {
        this.key = key;
        this.valueProviderMetadata = valueProviderMetadata;
        this.converterFunction = converterFunction;
    }

    public ValueType getFromType() {
        return key.getFromType();
    }

    public ValueType getToType() {
        return key.getToType();
    }

    public DataFlowComponentMetadata getValueProviderMetadata() {
        return valueProviderMetadata;
    }

    public ValueConverterFunction getConverterFunction() {
        return converterFunction;
    }
}
