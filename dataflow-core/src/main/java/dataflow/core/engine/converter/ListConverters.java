/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ListConverters.java
 */

package dataflow.core.engine.converter;

import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;
import dataflow.annotation.ValueConverter;
import dataflow.core.engine.DataFlowEngine;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SuppressWarnings("unchecked")
public class ListConverters {

    public static void register(DataFlowEngine engine) {
        engine.registerComponent(List_to_Set_converterMetadata.INSTANCE,
                new List_to_Set_converterBuilder(engine));
        engine.getValueRegistry().register(List_to_Set_converterMetadata.INSTANCE, new List_to_Set_converter());
    }

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static final class List_to_Set_converter implements ValueConverterFunction<List, Set> {

        @DataFlowConfigurable
        public List_to_Set_converter() {}

        @Override
        @OutputValue
        public Set getValue(@InputValue final List list) {
            Set set = new HashSet();
            set.addAll(list);
            return set;
        }
    }
}
