/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ValueConverterFunction.java
 */

package dataflow.core.engine.converter;

import dataflow.core.engine.ValueType;

/**
 * A function from converting objects of one type to another
 */
public interface ValueConverterFunction<FROM_TYPE, TO_TYPE> extends AutoCloseable {

    /**
     * Converts the given value to the target type.
     *
     * @param value       the value to convert
     * @param toValueType the target type
     */
    default TO_TYPE getValue(FROM_TYPE value, ValueType toValueType) {
        // Ignore the generic type params.
        return getValue(value);
    }

    default TO_TYPE getValue(FROM_TYPE value) {
        return getValue(value, null);
    }


    default void close() {}

}

