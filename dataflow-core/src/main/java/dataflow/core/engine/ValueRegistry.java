/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ValueRegistry.java
 */

package dataflow.core.engine;

import dataflow.annotation.metadata.DataFlowComponentMetadata;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dataflow.annotation.metadata.ValueConverterMetadata;
import dataflow.annotation.processor.TypeUtil;
import dataflow.core.engine.converter.ValueConverterFunction;
import dataflow.core.engine.converter.ValueConverterRegistration;
import dataflow.core.value.BooleanValue;
import dataflow.core.value.DoubleValue;
import dataflow.core.value.FloatValue;
import dataflow.core.value.IntegerValue;
import dataflow.core.value.LongValue;

/**
 * Contains information about the set of types that are available for use and provides type conversion functions.
 */
public class ValueRegistry {

    public static final String STRING_TYPE = "string";
    public static final ValueType STRING_VALUE_TYPE = new ValueType(String.class.getName(), Collections.EMPTY_LIST);

    private static final String ARRAY_CLASS_SUFFIX = "[]";

    private final Map<String, Class<?>> valueTypeClassMap = new HashMap<>();
    private final Map<ValueConverterRegistration.ValueConverterRegistrationKey, ValueConverterRegistration> converters = new HashMap<>();

    public ValueRegistry() {
        // Classes that can be referenced using their simple name.
        Class<?>[] simpleNameClasses = new Class<?>[]{
                Object.class, Integer.class, IntegerValue.class, Long.class, LongValue.class, Float.class,
                FloatValue.class, Double.class, DoubleValue.class, Boolean.class, BooleanValue.class, String.class,
                List.class, Stream.class
        };
        Arrays.stream(simpleNameClasses).forEach(simpleNameClass -> {
            valueTypeClassMap.put(simpleNameClass.getSimpleName(), simpleNameClass);
            valueTypeClassMap.put(simpleNameClass.getSimpleName().toLowerCase(), simpleNameClass);
        });
        valueTypeClassMap.put("int", int.class);
        valueTypeClassMap.put("long", long.class);
        valueTypeClassMap.put("float", float.class);
        valueTypeClassMap.put("double",double.class);
        valueTypeClassMap.put("bool", boolean.class);
        valueTypeClassMap.put("boolean", boolean.class);
    }

    public <FROM_TYPE, TO_TYPE> void register(
        DataFlowComponentMetadata converterValueProviderMetadata,
            ValueConverterFunction<FROM_TYPE, TO_TYPE> converterFunction) {
        ValueConverterMetadata valueConverterMetadata = converterValueProviderMetadata.getValueConverterMetadata();
        ValueType fromType = resolveClasses(ValueType.fromString(valueConverterMetadata.getFromType()));
        ValueType toType = resolveClasses(ValueType.fromString(valueConverterMetadata.getToType()));
        ValueConverterRegistration.ValueConverterRegistrationKey key =
                new ValueConverterRegistration.ValueConverterRegistrationKey(fromType, toType);
        converters.put(key, new ValueConverterRegistration(key, converterValueProviderMetadata, converterFunction));
    }

    public boolean canConvert(final String fromType, final String toType) {
        if (fromType.equals(toType)) {
            return true;
        }
        return getConverterRegistration(fromType, toType) != null;
    }

    public <T, R> R convert(final T object, final ValueType fromValueType, final ValueType toValueType) {
        if(resolveClasses(fromValueType).equals(resolveClasses(toValueType))) {
            return (R) object;
        }

        ValueConverterRegistration registration = getConverterRegistration(fromValueType, toValueType);
        if (registration == null) {
            // Fallback using the type of the object.
            ValueType objectValueType = ValueType.of(object);
            if(resolveClasses(objectValueType).equals(resolveClasses(toValueType))) {
                return (R) object;
            }
            registration = getConverterRegistration(objectValueType, toValueType);
        }
        if (registration != null) {
            return (R) registration.getConverterFunction().getValue(object, toValueType);
        }

        Class<?> toValueClass = getValueTypeClass(toValueType.getType());
        if(toValueClass.isAssignableFrom(object.getClass())) {
            // If the object is assignable to the target type then don't convert.
            return (R) object;
        }

        throw new IllegalArgumentException(
                "No converter registered to convert from " + object.getClass().getName() + " to " + toValueType);
    }

    public Class<?> getValueTypeClass(final String valueTypeParam) {
        if("?".equalsIgnoreCase(valueTypeParam)) {
            return Object.class;
        }

        String type = valueTypeParam;
        // Check for an entry that exactly matches the given type.
        Class<?> valueTypeClass = valueTypeClassMap.get(type);
        if (valueTypeClass != null) {
            return valueTypeClass;
        }
        // Remove the array suffix and generic types from the type and look up again.
        boolean isArray = false;
        if (type.endsWith(ARRAY_CLASS_SUFFIX)) {
            type = type.substring(0, type.length() - ARRAY_CLASS_SUFFIX.length());
            isArray = true;
        }
        type = TypeUtil.removeGenericTypes(type);
        valueTypeClass = valueTypeClassMap.get(type);
        if (valueTypeClass != null) {
            return !isArray ? valueTypeClass : Array.newInstance(valueTypeClass, 0).getClass();
        }
        try {
            valueTypeClass = Class.forName(type);
            if (valueTypeClass != null && isArray) {
                valueTypeClass = Array.newInstance(valueTypeClass, 0).getClass();
            }
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("No class for value type " + valueTypeParam + " defined");
        }
        return valueTypeClass;
    }

    public ValueConverterRegistration getConverterRegistration(ValueType fromType, ValueType toType) {
        ValueConverterRegistration.ValueConverterRegistrationKey key =
                new ValueConverterRegistration.ValueConverterRegistrationKey(fromType, toType);
        ValueConverterRegistration registration = converters.get(key);
        if (registration != null) {
            return registration;
        }

        // Resolve type classes.
        fromType = resolveClasses(fromType);
        toType = resolveClasses(toType);

        key = new ValueConverterRegistration.ValueConverterRegistrationKey(fromType, toType);
        registration = converters.get(key);
        if (registration != null) {
            // Cache the converter so that it can be found more quickly in the future.
            converters.put(key, registration);
            return registration;
        }

        // Try to find generic converter that is not specific to the generic type params.
        key = new ValueConverterRegistration.ValueConverterRegistrationKey(
                fromType.getTypeWithoutGenericParams(), toType.getTypeWithoutGenericParams());
        registration = converters.get(key);
        if (registration != null) {
            // Cache the converter so that it can be found more quickly in the future.
            converters.put(key, registration);
            return registration;
        }

        // Try looking for a converter using the fromType parent classes.
        Class<?> fromTypeClass = getValueTypeClass(fromType.getType());
        if (fromTypeClass != Object.class) {
            Set<Class<?>> fromTypeParentClasses = new HashSet<>();
            fromTypeParentClasses.add(fromTypeClass.getSuperclass());
            fromTypeParentClasses.addAll(Arrays.asList(fromTypeClass.getInterfaces()));
            fromTypeParentClasses.remove(null);
            for (Class<?> fromTypeParentClass : fromTypeParentClasses) {
                registration = getConverterRegistration(
                    ValueType.of(fromTypeParentClass), toType);
                if (registration != null) {
                    // Cache the converter so that it can be found more quickly in the future.
                    converters.put(key, registration);
                    return registration;
                }
            }
        }

        // Try looking for a converter using the toType super class.
        Class<?> toTypeClass = getValueTypeClass(toType.getType());
        if (toTypeClass != Object.class) {
            Set<Class<?>> toTypeParentClasses = new HashSet<>();
            toTypeParentClasses.add(toTypeClass.getSuperclass());
            toTypeParentClasses.addAll(Arrays.asList(toTypeClass.getInterfaces()));
            toTypeParentClasses.remove(null);
            for(Class<?> toTypeParentClass : toTypeParentClasses) {
                registration = getConverterRegistration(fromType, ValueType.of(toTypeParentClass));
                if (registration != null) {
                    // Cache the converter so that it can be found more quickly in the future.
                    converters.put(key, registration);
                    return registration;
                }
            }
        }
        return null;
    }

    public DataFlowComponentMetadata getConverterValueProviderMetadata(String fromType, String toType) {
        ValueConverterRegistration registration = getConverterRegistration(fromType, toType);
        return registration != null ? registration.getValueProviderMetadata() : null;
    }
//
//    public boolean isAssignable(ValueType fromType, ValueType toType) throws ClassNotFoundException {
//        fromType = resolveClasses(fromType);
//        toType = resolveClasses(toType);
//        Class<?> fromTypeClass = Class.forName(fromType.getType());
//        Class<?> toTypeClass = Class.forName(toType.getType());
//        if (toTypeClass.isAssignableFrom(fromTypeClass) &&
//                fromType.getTypeGenericParams().size() == toType.getTypeGenericParams().size()) {
//
//        }
//
//        return false;
//    }

    ValueConverterRegistration getConverterRegistration(String fromType, String toType) {
        return getConverterRegistration(ValueType.fromString(fromType), ValueType.fromString(toType));
    }

    ValueType resolveClasses(ValueType valueType) {
        Class<?> typeClass = getValueTypeClass(valueType.getType());
        List<ValueType> genericTypeParams = valueType.getTypeGenericParams().stream()
                .map(this::resolveClasses)
                .collect(Collectors.toList());
        return new ValueType(typeClass.getName(), genericTypeParams);
    }
}
