/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AbstractDataFlowInstance.java
 */

package dataflow.core.engine;

import dataflow.core.parser.ComponentConfig;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.parser.RawConfigurableObjectConfig;
import dataflow.core.retry.Retry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An abstract base class for dynamically generated {@link DataFlowInstance} implementations.
 */
@SuppressWarnings("unchecked")
public abstract class AbstractDataFlowInstance implements DataFlowInstance {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    protected final String instanceId = UUID.randomUUID().toString();
    protected final DataFlowEngine engine;
    protected final DataFlowEventManager eventManager;
    protected final Executor executor;
    protected final DataFlowConfig dataFlowConfig;
    protected long startTimeMillis;
    protected long endTimeMillis;
    protected long maxExecutionTimeMillis;
    protected final List<CompletableFuture<?>> executedFutures = new ArrayList<>();
    protected DataFlowInstance parentInstance;
    private Retry defaultRetryBehavior;

    private String metricNamePrefix;
    private Map<String, String> metricTags;
    private Map<Object, Map<String, String>> componentMetricTags = new IdentityHashMap<>();
    private Map<Object, String> componentIdMap = new IdentityHashMap<>();
    private final AtomicBoolean canceled = new AtomicBoolean(false);
    private final AtomicBoolean closed = new AtomicBoolean(false);

    public AbstractDataFlowInstance(DataFlowConfig dataFlowConfig, final DataFlowEngine engine,
            final Executor executor) {
        this.dataFlowConfig = dataFlowConfig;
        this.engine = engine;
        this.eventManager = engine.getEventManager();
        this.executor = executor;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public long getMaxExecutionTimeMillis() {
        return maxExecutionTimeMillis;
    }

    public void setMaxExecutionTimeMillis(final long maxExecutionTimeMillis) {
        this.maxExecutionTimeMillis = maxExecutionTimeMillis;
    }

    public DataFlowConfig getDataFlowConfig() {
        return dataFlowConfig;
    }

    public DataFlowEngine getEngine() {
        return engine;
    }

    public <T> void forEachComponent(Class<T> componentClass, Consumer<T> function) {
        getComponents().values().stream().filter(v -> componentClass.isAssignableFrom(v.getClass()))
                .map(v -> (T) v)
                .forEach(function);
        updateAvailability();
    }

    public void forEachComponent(BiConsumer<String, Object> function) {
        getComponents().forEach(function);
        updateAvailability();
    }

    public void updateAvailability() {
    }

    @Override
    public void setMetricNamePrefix(String metricNamePrefix) {
        this.metricNamePrefix = metricNamePrefix;
    }

    @Override
    public String getMetricNamePrefix() {
        if (metricNamePrefix != null) {
            return metricNamePrefix;
        }
        return parentInstance != null ? parentInstance.getMetricNamePrefix() : null;
    }

    @Override
    public void setMetricTags(Map<String, String> tags) {
        this.metricTags = tags != null ? new HashMap<>(tags) : new HashMap<>();
    }

    @Override
    public Map<String, String> getMetricTags() {
        Map<String, String> mergedTags = new HashMap<>();
        if (parentInstance != null && parentInstance.getMetricTags() != null) {
            mergedTags.putAll(parentInstance.getMetricTags());
        }
        if (metricTags != null) {
            mergedTags.putAll(metricTags);
        }
        return mergedTags;
    }

    @Override
    public void reset() {
        init();
    }

    public DataFlowConfig getConfig() {
        return dataFlowConfig;
    }

    public Retry getDefaultRetryBehavior() {
        if (defaultRetryBehavior != null) {
            return defaultRetryBehavior;
        }
        return parentInstance != null ? parentInstance.getDefaultRetryBehavior() : null;
    }

    public void setDefaultRetryBehavior(Retry defaultRetryBehavior) {
        this.defaultRetryBehavior = defaultRetryBehavior;
    }

    @Override
    public void cancel() {
        canceled.set(true);
    }

    @Override
    public boolean isCanceled() {
        return canceled.get();
    }

    @Override
    public void close() {
        closed.set(true);
    }

    @Override
    public void finalize() {
        if (!closed.get()) {
            logger.warn("Instance {} for DataFlow {} was not closed",
                getInstanceId(), getDataFlowConfig().getId());
            close();
        }
    }

    protected DataFlowExecutionContext createExecutionContext() {
        DataFlowExecutionContext context = DataFlowExecutionContext
            .createExecutionContext(dataFlowConfig, engine);
        context.setInstance(this);

        DataFlowExecutionContext parentContext = context.getParentContext();
        if (parentContext != null) {
            this.parentInstance = parentContext.getInstance();
        }
        return context;
    }

    public abstract Map<String, Object> getComponents();

    protected abstract void init();

    public synchronized Map<String, String> getMetricTagsForComponent(Object component) {
        Map<String, String> tags = componentMetricTags.get(component);
        if (tags == null) {
            tags = new HashMap<>();
            tags.put("dataFlowId", dataFlowConfig.getId());
            String componentId = getComponentId(component);
            tags.put("componentId", componentId != null ? componentId : "unknown");

            String componentType = null;
            if (componentId != null) {
                ComponentConfig componentConfig = dataFlowConfig.getComponents().get(componentId);
                if (componentConfig != null) {
                    componentType = componentConfig.getType();
                }
            }
            tags.put("componentType", componentType != null ? componentType : "unknown");
            tags.putAll(getMetricTags());
            componentMetricTags.put(component, tags);
        }
        return tags;
    }

    protected synchronized String getComponentId(Object component) {
        String componentId = componentIdMap.get(component);
        if (componentId != null) {
            return componentId;
        }
        for (Map.Entry<String, Object> entry : getComponents().entrySet()) {
            if (entry.getValue() == component) {
                componentIdMap.put(component, entry.getKey());
                return entry.getKey();
            }
        }
        return null;
    }

    protected void cancelExecutedFutures() {
        executedFutures.stream().filter(f -> !f.isDone())
                .forEach(f -> f.cancel(true));
    }

    protected void waitForExecutedFutures() {
        for (CompletableFuture<?> completableFuture : executedFutures) {
            try {
                waitFor(completableFuture);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
            }
        }
    }

    protected <T> T waitFor(CompletableFuture<T> completableFuture)
            throws InterruptedException, ExecutionException, TimeoutException {
        long millisRemaining = maxExecutionTimeMillis - (System.currentTimeMillis() - startTimeMillis);
        if (millisRemaining <= 0) {
            throw new TimeoutException();
        }
        return completableFuture.get(millisRemaining, TimeUnit.MILLISECONDS);
    }

    protected <T> void addComponentTimeout(String componentId, CompletableFuture<T> future) {
        // TODO(thorntonv): Commenting this out to see if it is causing a memory leak.
//        Long componentTimeout = dataFlowConfig.getComponents().get(componentId).getTimeoutMillis();
//        if(componentTimeout == null) {
//            // Default to the remaining execution time if no timeout is specified explicitly for the
//            // component.
//            long millisRemaining = maxExecutionTimeMillis - (System.currentTimeMillis() - startTimeMillis);
//            componentTimeout = millisRemaining;
//        }
//        timeoutExecutor.schedule(() -> {
//            if(!future.isDone()) {
//                logger.warn(componentId + " execution timed out. dataflow=" + getDataFlowConfig().getId());
//                future.completeExceptionally(new TimeoutException());
//            }
//        }, componentTimeout, TimeUnit.MILLISECONDS);
    }

    protected Object buildConstant(Object value) {
        if (value instanceof RawConfigurableObjectConfig) {
            RawConfigurableObjectConfig rawConfig = (RawConfigurableObjectConfig) value;
            return buildObject(rawConfig.getType(), rawConfig, getValues());
        } else {
            return value;
        }
    }

    protected Object buildObject(String type, Map<String, Object> map, Map<String, Object> instanceValues) {
        return engine.buildObject(type, map, instanceValues);
    }

    protected void fireDataFlowExecutionSuccessEvent() {
        eventManager.fireDataFlowExecutionSuccessEvent(dataFlowConfig.getId(), instanceId, startTimeMillis,
                endTimeMillis);
    }

    protected void fireDataFlowExecutionErrorEvent(Throwable error) {
        eventManager.fireDataFlowExecutionErrorEvent(dataFlowConfig.getId(), instanceId, startTimeMillis, endTimeMillis,
                error);
    }

    protected void fireComponentExecutionSuccessEvent(String componentId, long startTimeMillis) {
        eventManager.fireComponentExecutionSuccessEvent(dataFlowConfig.getId(), instanceId, componentId,
                startTimeMillis, System.currentTimeMillis());
    }

    protected void fireComponentExecutionErrorEvent(String componentId, long startTimeMillis, Throwable error) {
        eventManager.fireComponentExecutionErrorEvent(dataFlowConfig.getId(), instanceId, componentId,
                startTimeMillis, endTimeMillis, error);
    }

    protected void fireComponentFallbackEvent(String componentId, String fallbackComponentId) {
        eventManager.fireComponentFallbackEvent(dataFlowConfig.getId(), instanceId, componentId,
                fallbackComponentId);
    }

    protected void fireComponentErrorFallbackEvent(String componentId, String fallbackComponentId,
            Exception error) {
        eventManager.fireComponentErrorFallbackEvent(dataFlowConfig.getId(), instanceId, componentId,
                fallbackComponentId, error);
    }
}
