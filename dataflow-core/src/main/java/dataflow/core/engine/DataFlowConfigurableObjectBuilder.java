/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfigurableObjectBuilder.java
 */

package dataflow.core.engine;

import java.util.List;
import java.util.Map;

/**
 * An interface for builders that build a {@link dataflow.annotation.DataFlowConfigurable} object from configuration
 * properties.
 */
public interface DataFlowConfigurableObjectBuilder {

    /**
     * Build the object
     *
     * @param configProperties the configuration properties
     * @param instanceValues   the values from the DataFlow instance
     * @return the built object
     */
    Object build(Map<String, Object> configProperties, Map<String, Object> instanceValues);


    /**
     * See {@link dataflow.core.engine.DataFlowEngine.DependencyInjector#injectDependencies(Object)}
     */
    default <T> T injectDependencies(T obj, DataFlowEngine engine) {
        return engine.getDependencyInjector().injectDependencies(obj);
    }

    /**
     * See {@link DataFlowEngine#buildObject(String, Map, Map)}
     */
    default Object buildObject(String type, Map<String, Object> propValues, Map<String, Object> instanceValues,
            DataFlowEngine engine) {
        return engine.buildObject(type, propValues, instanceValues);
    }

    /**
     * See {@link DataFlowEngine#buildListObject(List, String, Map)}
     */
    default Object buildListObject(List list, String itemClassName, Map<String, Object> instanceValues, DataFlowEngine engine) {
        return engine.buildListObject(list, itemClassName, instanceValues);
    }
}
