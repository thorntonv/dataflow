/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowEventManager.java
 */

package dataflow.core.engine;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"WeakerAccess", "unused"})
public class DataFlowEventManager {

    private List<DataFlowEventListener> listeners = new ArrayList<>();

    public void addListener(DataFlowEventListener listener) {
        listeners.add(listener);
    }

    public void fireDataFlowLoadEvent(String dataFlowId, long timestamp) {
        listeners.forEach(listener -> listener.onDataFlowLoad(dataFlowId, timestamp));
    }

    public void fireDataFlowUnloadEvent(String dataFlowId, long timestamp) {
        listeners.forEach(listener -> listener.onDataFlowUnload(dataFlowId, timestamp));
    }

    public void fireDataFlowLoadError(String dataFlowId, Throwable error) {
        listeners.forEach(listener -> listener.onDataFlowLoadError(dataFlowId, error));
    }

    public void fireDataFlowChangedEvent(String dataFlowId, long timestamp) {
        listeners.forEach(listener -> listener.onDataFlowChanged(dataFlowId, timestamp));
    }

    public void fireInstanceCreationEvent(String dataFlowId, DataFlowInstance instance, long timestamp) {
        listeners.forEach(listener -> listener.onDataFlowInstanceCreated(dataFlowId, instance, timestamp));
    }

    public void fireDataFlowExecutionSuccessEvent(String dataFlowId, String instanceId, long startTimeMillis,
            long endTimeMillis) {
        listeners.forEach(listener -> listener.onDataFlowExecutionSuccess(
                dataFlowId, instanceId, startTimeMillis, endTimeMillis));
    }

    public void fireDataFlowExecutionErrorEvent(String dataFlowId, String instanceId, long startTimeMillis,
            long endTimeMillis, Throwable error) {
        listeners.forEach(listener -> listener.onDataFlowExecutionError(
                dataFlowId, instanceId, startTimeMillis, endTimeMillis, error));
    }

    public void fireComponentExecutionSuccessEvent(String dataFlowId, String instanceId, String componentId,
            long startTimeMillis, long endTimeMillis) {
        listeners.forEach(listener -> listener.onComponentExecutionSuccess(
                dataFlowId, instanceId, componentId, startTimeMillis, endTimeMillis));
    }

    public void fireComponentExecutionErrorEvent(String dataFlowId, String instanceId, String componentId,
            long startTimeMillis, long endTimeMillis, Throwable error) {
        listeners.forEach(listener -> listener.onComponentExecutionError(
                dataFlowId, instanceId, componentId, startTimeMillis, endTimeMillis, error));
    }

    public void fireComponentFallbackEvent(String dataFlowId, String instanceId, String componentId,
            String fallbackComponentId) {
        listeners.forEach(listener -> listener.onComponentFallback(dataFlowId, instanceId,
                componentId, fallbackComponentId));
    }

    public void fireComponentErrorFallbackEvent(String dataFlowId, String instanceId, String componentId,
            String fallbackComponentId, Throwable error) {
        listeners.forEach(listener -> listener.onComponentErrorFallback(dataFlowId, instanceId,
                componentId, fallbackComponentId, error));
    }
}
