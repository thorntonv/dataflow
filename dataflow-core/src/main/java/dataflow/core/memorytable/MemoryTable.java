/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MemoryTable.java
 */

package dataflow.core.memorytable;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MemoryTable implements Iterable<Map<String, Object>> {

    private static class RowIterator implements Iterator<Map<String, Object>> {

        private Iterator<List<Map<String, Object>>> rowsIt;
        private Iterator<Map<String, Object>> rowIt;

        public RowIterator(final Iterator<List<Map<String, Object>>> rowsIt) {
            this.rowsIt = rowsIt;
            nextRowIt();
        }

        private void nextRowIt() {
            while (rowsIt.hasNext() && (rowIt == null || !rowIt.hasNext())) {
                rowIt = rowsIt.next().iterator();
            }
        }

        @Override
        public boolean hasNext() {
            return rowIt.hasNext();
        }

        @Override
        public Map<String, Object> next() {
            Map<String, Object> next = null;
            if (rowIt.hasNext()) {
                next = rowIt.next();
            } else {
                nextRowIt();
                if (rowIt.hasNext()) {
                    next = rowIt.next();
                }
            }
            nextRowIt();
            return next;
        }
    }

    private Map<MemoryTableKey, List<Map<String, Object>>> data;

    public MemoryTable(Map<MemoryTableKey, List<Map<String, Object>>> data) {
        this.data = data;
    }

    public List<Map<String, Object>> get(MemoryTableKey key) {
        return data.get(key);
    }

    public Map<String, Object> getSingleRow(MemoryTableKey key) {
        List<Map<String, Object>> result = data.get(key);
        return result != null && result.size() > 0 ? result.get(0) : null;
    }

    public Object getSingleColumn(MemoryTableKey key, String columnName) {
        List<Map<String, Object>> result = data.get(key);
        return result != null && result.size() > 0 ? result.get(0).get(columnName) : null;
    }

    public int size() {
        return data.size();
    }

    public Map<MemoryTableKey, List<Map<String, Object>>> getData() {
        return data;
    }

    @Override
    public Iterator<Map<String, Object>> iterator() {
        return new RowIterator(data.values().iterator());
    }
}
