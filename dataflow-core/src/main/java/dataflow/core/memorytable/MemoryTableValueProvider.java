/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MemoryTableValueProvider.java
 */

package dataflow.core.memorytable;

import dataflow.core.parser.RawConfigurableObjectConfig;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.StreamSupport;

import org.apache.commons.csv.CSVFormat;
import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValues;
import dataflow.annotation.OutputValue;
import dataflow.core.datasource.DataSource;
import dataflow.core.datasource.DataSourceManager;
import dataflow.core.datasource.DataSourceUtil;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.PlaceholderString;
import dataflow.core.engine.ValueReference;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.parser.ComponentConfig;
import dataflow.core.component.PlaceholderStringValueProviderMetadata;
import dataflow.core.component.data.RecordSchema;
import dataflow.core.component.data.SourceFormat;

@DataFlowComponent(codeGenerator = MemoryTableValueProviderCodeGenerator.class)
public class MemoryTableValueProvider {

    private static final Number DEFAULT_REFRESH_INTERVAL_SECONDS = 60;

    private static final String REFRESH_INTERVAL_PROP_NAME = "refreshIntervalSeconds";
    private static final String LAZY_LOAD_PROP_NAME = "lazyLoad";

    private final String[] keyColumns;
    private final String outputColumn;
    private final MemoryTableOutputMode outputMode;

    private final MemoryTableKey key;
    private final MemoryTableDescriptor descriptor;
    private final MemoryTableManager memoryTableManager;
    private MemoryTable memoryTable;
    private static Map<String, List<DataSourceManager<MemoryTableDescriptor>.DataSourceSupplierRegistration>> dataFlowIdSupplierRegistrationsMap = new ConcurrentHashMap<>();

    /**
     * Preloads the memory table(s) if not already loaded. This method is called by the DataFlow engine when a
     * DataFlow is first loaded. Multiple tables may be loaded if the source filename contains $() placeholders.
     * Placeholders are replaced with wildcards and any DataSources that match the pattern are loaded. This preloading
     * behavior can be disabled by setting the lazyLoad property to true in the value provider config.
     */
    @SuppressWarnings("unchecked")
    public static void onLoad(ComponentConfig componentConfig, DataFlowConfig dataFlowConfig,
        DataFlowEngine engine) {
        // Check if lazyLoad is set.
        Boolean lazyLoad = (Boolean) componentConfig.getProperties().get(LAZY_LOAD_PROP_NAME);
        if (lazyLoad != null && lazyLoad) {
            // Don't preload the memory tables.
            return;
        }

        MemoryTableValueProviderBuilder builder = new MemoryTableValueProviderBuilder(engine);

        // Get the memory table source config.
        RawConfigurableObjectConfig sourceConfig = (RawConfigurableObjectConfig) componentConfig.getProperties().get("source");
        if (sourceConfig != null && sourceConfig.size() == 1) {
            String sourceType = sourceConfig.getType();

            final DataFlowExecutionContext executionContext = DataFlowExecutionContext.getCurrentExecutionContext();
            final MemoryTableManager memoryTableManager = executionContext.getDependencyInjector().getInstance(MemoryTableManager.class);

            // Get the path configuration.
            Object pathConfig = sourceConfig.get("path");

            Number refreshInterval = (Number) sourceConfig.get(REFRESH_INTERVAL_PROP_NAME);
            if(refreshInterval == null) {
                refreshInterval = DEFAULT_REFRESH_INTERVAL_SECONDS;
            }

            Map<String, Object> values = new HashMap<>();
            Callable<List<MemoryTableDescriptor>> supplier = null;
            if (!(pathConfig instanceof ValueReference)) {
                // The path does not contain a value reference. Just build the value provider and get the memory table
                // descriptor.
                supplier = () -> {
                    DataFlowExecutionContext.createExecutionContext(dataFlowConfig, engine);
                    try {
                        MemoryTableValueProvider valueProvider =
                                builder.build(componentConfig.getProperties(), values);
                        return Collections.singletonList(valueProvider.getMemoryTableDescriptor());
                    } finally {
                        DataFlowExecutionContext.setCurrentExecutionContext(executionContext);
                    }
                };
            } else {
                // The path has a value reference.
                ValueReference valueReference = (ValueReference) pathConfig;
                ComponentConfig placeholderConfig =
                        dataFlowConfig.getComponents().get(valueReference.getComponentId());
                if (placeholderConfig != null &&
                        PlaceholderStringValueProviderMetadata.TYPE.equals(placeholderConfig.getType())) {
                    // The value reference is referring to a placeholder string.
                    String placeholderString = (String) placeholderConfig.getProperties().get("placeholderString");

                    // Build the pattern string by replacing placeholders with the wildcard character.
                    String pattern = placeholderString.replaceAll(PlaceholderString.PLACEHOLDER_PATTERN.pattern(), "*")
                            // Remove dash or underscore before or after the wildcard.
                            .replaceAll("[-_]?\\*[-_]?", "*");

                    // Set the placeholder string value to the root path. This is only done for the purpose of building
                    // an instance of the DataSource.
                    values.put(placeholderConfig.getId(), DataSource.PATH_SEPARATOR);

                    // Build an instance of the DataSource. This instance is only used to find the DataSources that
                    // match the wildcard pattern.
                    DataSource source = (DataSource) executionContext.buildObject(sourceType, sourceConfig, values);

                    supplier = () -> {
                        DataFlowExecutionContext.createExecutionContext(dataFlowConfig, engine);

                        try {
                            List<MemoryTableDescriptor> descriptors = new ArrayList<>();
                            // For each root of the DataSource
                            for(DataSource root : source.getRoots()) {
                                // Get all DataSources that match the placeholder string wildcard pattern.
                                List<DataSource> dataSources = DataSourceUtil.getDataSourcesMatchingGlobPattern(root, pattern);
                                for (DataSource dataSource : dataSources) {
                                    // Set the path placeholder string to the DataSource path.
                                    values.put(placeholderConfig.getId(), dataSource.getPath());
                                    // Build an instance of the value provider to get the memory table descriptor.
                                    MemoryTableValueProvider valueProvider =
                                            builder.build(componentConfig.getProperties(), values);

                                    descriptors.add(valueProvider.getMemoryTableDescriptor());
                                }
                            }
                            return descriptors;
                        } finally {
                            DataFlowExecutionContext.setCurrentExecutionContext(executionContext);
                        }
                    };
                }
            }

            if (supplier != null) {
                dataFlowIdSupplierRegistrationsMap.computeIfAbsent(executionContext.getDataFlowId(), (k) -> new ArrayList<>())
                        .add(memoryTableManager.register(supplier, refreshInterval.longValue()));
            }
        }
    }

    /**
     * Unloads the memory table(s) if there are no other DataFlows referencing them. This method is called by the
     * DataFlow engine when a DataFlow config is unloaded.
     */
    public static void onUnload(ComponentConfig componentConfig, DataFlowConfig dataFlowConfig,
            DataFlowEngine engine) {
        DataFlowExecutionContext executionContext = DataFlowExecutionContext.getCurrentExecutionContext();
        List<DataSourceManager<MemoryTableDescriptor>.DataSourceSupplierRegistration> supplierRegistrations =
                dataFlowIdSupplierRegistrationsMap.get(executionContext.getDataFlowId());
        if (supplierRegistrations != null) {
            MemoryTableManager memoryTableManager = executionContext.getDependencyInjector().getInstance(MemoryTableManager.class);
            supplierRegistrations.forEach(memoryTableManager::unregister);
        }
    }

    @DataFlowConfigurable
    public MemoryTableValueProvider(
            @DataFlowConfigProperty final List<String> keyColumns,
            @DataFlowConfigProperty(required = false) final String outputColumn,
            @DataFlowConfigProperty final String schema,
            @DataFlowConfigProperty(required = false) final MemoryTableOutputMode outputMode,
            @DataFlowConfigProperty(required = false) final SourceFormat sourceFormat,
            @DataFlowConfigProperty final DataSource source,
            @DataFlowConfigProperty(name = LAZY_LOAD_PROP_NAME, required = false) final Boolean lazyLoad,
            @DataFlowConfigProperty(name = REFRESH_INTERVAL_PROP_NAME, required = false) final Long refreshIntervalSeconds,
            @DataFlowConfigProperty(required = false) final List<MemoryTableValidator> validators) throws DataFlowConfigurationException {
        this.keyColumns = keyColumns.toArray(new String[0]);
        this.outputColumn = outputColumn;
        this.outputMode = outputMode != null ? outputMode : MemoryTableOutputMode.MULTI_ROW;

        RecordSchema recordSchema = new RecordSchema(schema);

        CSVFormat csvFormat = (sourceFormat == SourceFormat.TSV) ?
                CSVFormat.DEFAULT.withDelimiter('\t') : CSVFormat.DEFAULT;
        this.descriptor = new MemoryTableDescriptor(recordSchema, keyColumns, source,
                refreshIntervalSeconds != null ? refreshIntervalSeconds : -1, csvFormat);

        this.memoryTableManager = DataFlowExecutionContext.getCurrentExecutionContext()
                .getDependencyInjector().getInstance(MemoryTableManager.class);
        this.memoryTable = memoryTableManager.get(descriptor);

        if(memoryTable == null) {
            if (lazyLoad != null && lazyLoad) {
                // Lazy load the memory table. The table will be unloaded when the DataFlow is unloaded.
                String dataFlowId = DataFlowExecutionContext.getCurrentExecutionContext().getDataFlowId();
                dataFlowIdSupplierRegistrationsMap.computeIfAbsent(dataFlowId, (k) -> new ArrayList<>())
                        .add(memoryTableManager.register(descriptor));
                this.memoryTable = memoryTableManager.get(descriptor);
            }
            if (DataFlowExecutionContext.getCurrentExecutionContext().getInstance() != null && memoryTable == null) {
                // Only throw the exception when the instance is not null because we don't want to fail when running the
                // onLoad method.
                throw new RuntimeException("Memory table not loaded: " + descriptor);
            }
        }
        this.key = new MemoryTableKey(new Object[keyColumns.size()]);
    }

    MemoryTableDescriptor getMemoryTableDescriptor() {
        return descriptor;
    }

    @OutputValue
    public Object getValue(@InputValues Map<String, ?> keyValues) {
        for (int idx = 0; idx < keyColumns.length; idx++) {
            key.getComponents()[idx] = keyValues.get(keyColumns[idx]);
        }
        return getValue();
    }

    public MemoryTableKey getKey() {
        return key;
    }

    public Object getValue() {
        Object result = null;
        switch (outputMode) {
            case SINGLE_ROW:
                result = memoryTable.getSingleRow(key);
                break;
            case SINGLE_COLUMN:
                result = memoryTable.getSingleColumn(key, outputColumn);
                break;
            case MULTI_ROW:
                result = memoryTable.get(key);
                break;
            case ALL_KEYS:
                result = memoryTable.getData().keySet().stream().map(this::keyToMap);
                break;
            case ALL_ROWS:
                result = StreamSupport.stream(memoryTable.spliterator(), false);
                break;
        }
        return result;
    }

    private Map<String, Object> keyToMap(MemoryTableKey key) {
        Map<String, Object> map = new HashMap<>();
        for (int idx = 0; idx < keyColumns.length; idx++) {
            map.put(keyColumns[idx], key.getComponents()[idx]);
        }
        return map;
    }
}
