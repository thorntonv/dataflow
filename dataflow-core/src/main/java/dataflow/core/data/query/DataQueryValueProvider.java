/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataQueryValueProvider.java
 */

package dataflow.core.data.query;

import dataflow.core.data.DataRecords;
import dataflow.core.data.InvalidDataException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public interface DataQueryValueProvider {

    boolean isFilterSupported(DataFilter filter);

    DataRecords query(Map<String, Object> inputValues, List<DataFilter> filters) throws IOException, InvalidDataException;

    Stream<DataRecords> queryForStream(Map<String, Object> paramValues,
        List<DataFilter> filters, int batchSize) throws IOException;

    Optional<Long> getEstimatedResultCount(Map<String, Object> inputValues, List<DataFilter> filters);

    void setOrderByKeyIfNotSpecified(boolean orderByKeyIfNotSpecified);
    void setRequireOrderBy(boolean requireOrderBy);
}
