/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataRecordsSplitList.java
 */

package dataflow.core.data;

import static dataflow.core.data.DataRecordPathUtil.visit;

import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;
import dataflow.core.exception.DataFlowConfigurationException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@DataFlowComponent(description = "Splits a list contained in DataRecords so that a new record is " +
    "created for each item in the list. Columns from the original record and data from the list " +
    "items can be copied to columns in the new record")
public class DataRecordsSplitList {

    private final List<DataRecordPath> listPaths;
    private final Set<String> includeColumns;
    private final Set<String> excludeColumns;
    private final List<String> keyColumns;
    private final Map<String, String> columnNameItemPathMap;

    @DataFlowConfigurable
    public DataRecordsSplitList(
        @DataFlowConfigProperty(required = false, description = "The column that contains the list") String listColumn,
        @DataFlowConfigProperty(required = false, description = "The path of the list within the column. " +
            "If the column itself is a list then this does not need to be set") String listPath,
        @DataFlowConfigProperty(required = false, description = "The paths of the lists if multiple " +
            "lists should be processed") List<DataRecordPath> listPaths,
        @DataFlowConfigProperty(required = false, description = "The names of columns in the " +
            "original record to copy to the new records") Set<String> includeColumns,
        @DataFlowConfigProperty(required = false, description = "The names of columns in the " +
            "original record to exclude. All other columns will be copied to the new records") Set<String> excludeColumns,
        @DataFlowConfigProperty(required = false, description = "A map of column names to paths in " +
            "the list items. The values at the given paths will be copied to the new records in " +
            "columns with the specified names. If this is not not set and the list items are maps " +
            "(or json objects) then the map values will be copied to columns using the map keys as " +
            "the column names") Map<String, String> columnNameItemPathMap,
        @DataFlowConfigProperty(required = false, description = "The key column(s) of the newly " +
            "created records") List<String> keyColumns) {
        this.listPaths = listPaths != null ? new ArrayList<>(listPaths) :
            Collections.singletonList(new DataRecordPath(listColumn, listPath));
        this.includeColumns = includeColumns != null ? includeColumns : Collections.emptySet();
        this.excludeColumns = excludeColumns != null ? excludeColumns : Collections.emptySet();
        this.columnNameItemPathMap = columnNameItemPathMap != null ?
            columnNameItemPathMap : Collections.emptyMap();

        if (this.includeColumns.size() > 0 && this.excludeColumns.size() > 0) {
            throw new DataFlowConfigurationException(
                "Can not specify both include and exclude columns");
        }
        this.keyColumns = keyColumns;
    }

    @OutputValue
    public DataRecords getValue(@InputValue DataRecords records) {
        DataRecords splitRecords = new DataRecords();
        for (DataRecord record : records) {
            Set<String> recordColumnsToCopy = new HashSet<>(includeColumns);
            if (!excludeColumns.isEmpty()) {
                recordColumnsToCopy.addAll(record.keySet());
                recordColumnsToCopy.removeAll(excludeColumns);
            }

            listPaths.forEach(listPath ->
                splitRecords.addAll(splitList(listPath, record, recordColumnsToCopy)));
        }
        splitRecords.setKeyColumnNames(keyColumns != null ?
            keyColumns : records.getKeyColumnNames());
        if (!splitRecords.isEmpty()) {
            // Verify that the key columns are present in the new records.
            Set<String> columnNames = splitRecords.getColumnNames().stream()
                .map(String::toLowerCase)
                .collect(Collectors.toSet());
            Set<String> missingKeyColumns = (splitRecords.getKeyColumnNames() != null ?
                splitRecords.getKeyColumnNames() : Collections.<String>emptySet()).stream()
                .filter(col -> !columnNames.contains(col.toLowerCase()))
                .collect(Collectors.toSet());
            if (!missingKeyColumns.isEmpty()) {
                throw new IllegalArgumentException(
                    "Key columns " + missingKeyColumns + " are missing from the generated records");
            }
        }
        return splitRecords;
    }


    private List<DataRecord> splitList(DataRecordPath listPath, DataRecord record,
            Set<String> recordColumnsToCopy) {
        List<DataRecord> splitRecords = new ArrayList<>();
        List<Object> lists;
        try {
            lists = record.getValuesWithPath(listPath);
        } catch(Exception ex) {
            throw new IllegalArgumentException(
                "Value with path " + listPath + " must be a list");
        }

        for (Object list : lists) {
            if(!(list instanceof Iterable)) {
                throw new IllegalArgumentException(
                    "Value with path " + listPath + " must be a list");
            }
            visit(list, 1, (item, itemPath) -> {
                DataRecord newRecord = new DataRecord();
                recordColumnsToCopy.forEach(col -> newRecord.put(col, record.get(col)));
                if (!columnNameItemPathMap.isEmpty()) {
                    columnNameItemPathMap.forEach((col, path) ->
                        newRecord.put(col, DataRecordPathUtil.getValue(item, path)));
                } else {
                    visit(item, 1, (itemChild, itemChildPath) -> {
                        String colName = itemChildPath;
                        if(colName.isEmpty()) {
                            throw new IllegalArgumentException("Unable to split list. columnNameItemPathMap is not set and the list items are not maps");
                        }
                        newRecord.put(colName, itemChild);
                    });
                }
                splitRecords.add(newRecord);
            });
        }

        return splitRecords;
    }
}
