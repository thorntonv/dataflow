/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataComponents.java
 */

package dataflow.core.data;

import dataflow.core.engine.DataFlowEngine;

public class DataComponents {

    public static void register(DataFlowEngine engine) {
        engine.registerComponent(JoinDataRecordsMetadata.INSTANCE, new JoinDataRecordsBuilder(engine));
        engine.registerComponent(LeftJoinDataRecordsMetadata.INSTANCE, new LeftJoinDataRecordsBuilder(engine));
        engine.registerComponent(DataRecordsSplitListMetadata.INSTANCE, new DataRecordsSplitListBuilder(engine));
        engine.registerComponent(TransformDataRecordsMetadata.INSTANCE, new TransformDataRecordsBuilder(engine),
            new TransformDataRecordsCodeGenerator());
        engine.registerPropertyObjectBuilder(DataRecordPath.class, new DataRecordPathBuilder(engine));
    }
}
