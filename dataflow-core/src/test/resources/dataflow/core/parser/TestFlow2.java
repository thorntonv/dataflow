package dataflow.test;

import dataflow.core.data.*;
import dataflow.core.engine.AbstractDataFlowInstance;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.ValueType;
import dataflow.core.exception.DataFlowExecutionException;
import dataflow.core.memorytable.MemoryTableKey;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.util.MapUtil;
import dataflow.core.value.BooleanValue;
import dataflow.core.value.DoubleValue;
import dataflow.core.value.FloatValue;
import dataflow.core.value.IntegerValue;
import dataflow.core.value.LongValue;
import java.lang.AutoCloseable;
import java.util.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.stream.*;

public class TestFlow2 extends AbstractDataFlowInstance {
    private final Map<String, Runnable> componentInitRunnables = new HashMap<>();
    private List __list0_value = new ArrayList<>(1);
    private boolean __list0_available = true;
    private volatile boolean __list0_finished = false;
    private dataflow.core.memorytable.MemoryTableValueProvider _goc_feature_component;
    private volatile java.lang.Object _goc_feature_value;
    private Object[] _goc_feature_keyComponents;
    private boolean _goc_feature_available = true;
    private volatile boolean _goc_feature_finished = false;
    private dataflow.core.component.FallbackValueProvider _goc_feature_fallback_component;
    private volatile java.lang.Object _goc_feature_fallback_value;
    private boolean _goc_feature_fallback_available = true;
    private volatile boolean _goc_feature_fallback_finished = false;
    private dataflow.core.component.math.BinnedValueProvider _goc_feature_binned_component;
    private volatile dataflow.core.value.IntegerValue _goc_feature_binned_value = new dataflow.core.value.IntegerValue();
    private boolean _goc_feature_binned_available = true;
    private volatile boolean _goc_feature_binned_finished = false;

    public TestFlow2(DataFlowConfig dataFlowConfig, DataFlowEngine engine, Executor executor, Consumer<DataFlowInstance> initFn) {
        super(dataFlowConfig, engine, executor);
        initFn.accept(this);
        init();
    }

    public void init() {
        createExecutionContext();
        try {
            __list0_init();
            _goc_feature_init();
            _goc_feature_fallback_init();
            _goc_feature_binned_init();
            updateAvailability();
        } finally { DataFlowExecutionContext.popExecutionContext(); }
    }

    private void __list0_init() {
    }

    private void _goc_feature_init() {
        this._goc_feature_component = (dataflow.core.memorytable.MemoryTableValueProvider) buildObject("MemoryTableValueProvider", dataFlowConfig.getComponents().get("goc_feature").getProperties(), this.getValues());
        this._goc_feature_keyComponents = _goc_feature_component.getKey().getComponents();
        _goc_feature_keyComponents[0] = "GOC";
        _goc_feature_keyComponents[1] = 1002;
    }

    private void _goc_feature_fallback_init() {
        this._goc_feature_fallback_component = (dataflow.core.component.FallbackValueProvider) buildObject("FallbackValueProvider", dataFlowConfig.getComponents().get("goc_feature_fallback").getProperties(), this.getValues());
    }

    private void _goc_feature_binned_init() {
        this._goc_feature_binned_component = (dataflow.core.component.math.BinnedValueProvider) buildObject("BinnedValueProvider", dataFlowConfig.getComponents().get("goc_feature_binned").getProperties(), this.getValues());
    }

    public Map<String, Object> getComponents() {
        Map<String, Object> components = new LinkedHashMap<>();
        components.put("goc_feature_fallback", _goc_feature_fallback_component);
        components.put("goc_feature_binned", _goc_feature_binned_component);
        return components;
    }

    public void setValue(String id, Object value) {
        switch(id) {
            case "goc_feature":
                _goc_feature_value = value;
                _goc_feature_finished = true;
                _goc_feature_fallback_finished = false;
                _goc_feature_binned_finished = false;
                break;
            case "goc_feature_fallback":
                _goc_feature_fallback_value = value;
                _goc_feature_fallback_finished = true;
                _goc_feature_binned_finished = false;
                break;
            case "goc_feature_binned":
                _goc_feature_binned_value = (dataflow.core.value.IntegerValue)value;
                _goc_feature_binned_finished = true;
                break;
        }
        updateAvailability();
    }

    public Map<String, Object> getValues() {
        Map<String, Object> values = new HashMap<>();
        values.put("_list0", __list0_value);
        values.put("goc_feature", _goc_feature_value);
        values.put("goc_feature_fallback", _goc_feature_fallback_value);
        values.put("goc_feature_binned", _goc_feature_binned_value);
        return values;
    }

    public Object getValue(String id) {
        switch(id) {
            case "_list0":
                return __list0_value;
            case "goc_feature":
                return _goc_feature_value;
            case "goc_feature_fallback":
                return _goc_feature_fallback_value;
            case "goc_feature_binned":
                return _goc_feature_binned_value;
        }
        return null;
    }

    public String getOutputType() {
        return "dataflow.core.value.IntegerValue";
    }

    public String getValueType(String id) {
        switch(id) {
            case "_list0":
                return "java.util.List<java.lang.Object>";
            case "goc_feature":
                return "java.lang.Object";
            case "goc_feature_fallback":
                return "java.lang.Object";
            case "goc_feature_binned":
                return "int";
        }
        return null;
    }

    public void execute() throws DataFlowExecutionException {
        DataFlowExecutionContext executionContext = createExecutionContext();
        this.startTimeMillis = System.currentTimeMillis();
        try {
            componentInitRunnables.values().forEach(runnable -> runnable.run());
            if(!componentInitRunnables.isEmpty()) updateAvailability();
            componentInitRunnables.clear();
            this.executedFutures.clear();
            if(__list0_available && !__list0_finished) {
                __list0_value.clear();
                __list0_value.add(5);
                __list0_finished = true;
            }
            if(_goc_feature_available && !_goc_feature_finished) {
                _goc_feature_value = _goc_feature_component.getValue();
                _goc_feature_finished = true;
            }
            if(_goc_feature_fallback_available && !_goc_feature_fallback_finished) {
                _goc_feature_fallback_value = ((java.lang.Object)_goc_feature_fallback_component.getValue(_goc_feature_value, __list0_value));
                _goc_feature_fallback_finished = true;
            }
            if(_goc_feature_binned_available && !_goc_feature_binned_finished) {
                _goc_feature_binned_value.setValue(((int)_goc_feature_binned_component.getValue(engine.convert(_goc_feature_fallback_value, ValueType.fromString("java.lang.Object"), ValueType.fromString("double")))));
                _goc_feature_binned_finished = true;
            }
            if(!executedFutures.isEmpty()) waitForExecutedFutures();
            this.endTimeMillis = System.currentTimeMillis();
            fireDataFlowExecutionSuccessEvent();
        } catch(Throwable t) {
            this.endTimeMillis = System.currentTimeMillis();
            fireDataFlowExecutionErrorEvent(t);
            cancelExecutedFutures();
            throw new DataFlowExecutionException(t);
        } finally {
            DataFlowExecutionContext.popExecutionContext();
        }
    }

    public dataflow.core.value.IntegerValue getOutput() {
        return _goc_feature_binned_value;
    }

    public void updateAvailability() {
        __list0_available = true;
        _goc_feature_available = true;
        _goc_feature_fallback_available = __list0_available && _goc_feature_available;
        _goc_feature_binned_available = _goc_feature_fallback_available;
    }

    public void close() {
        super.close();
        try {
        } catch(Exception ex) {
            fireDataFlowExecutionErrorEvent(ex);
        }
        try {
            if(_goc_feature_component != null && (_goc_feature_component instanceof AutoCloseable)) ((AutoCloseable) _goc_feature_component).close();
        } catch(Exception ex) {
            fireDataFlowExecutionErrorEvent(ex);
        }
        try {
            if(_goc_feature_fallback_component != null && (_goc_feature_fallback_component instanceof AutoCloseable)) ((AutoCloseable) _goc_feature_fallback_component).close();
        } catch(Exception ex) {
            fireDataFlowExecutionErrorEvent(ex);
        }
        try {
            if(_goc_feature_binned_component != null && (_goc_feature_binned_component instanceof AutoCloseable)) ((AutoCloseable) _goc_feature_binned_component).close();
        } catch(Exception ex) {
            fireDataFlowExecutionErrorEvent(ex);
        }
    }
}
