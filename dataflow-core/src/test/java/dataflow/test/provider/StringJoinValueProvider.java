/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  StringJoinValueProvider.java
 */

package dataflow.test.provider;

import java.util.List;

import com.google.common.base.Joiner;
import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;

@DataFlowComponent
public class StringJoinValueProvider {

    private String separator;

    @DataFlowConfigurable
    public StringJoinValueProvider(@DataFlowConfigProperty(required = false) String separator) {
        this.separator = separator != null ? separator : ",";
    }

    @OutputValue
    public String getValue(@InputValue List<String> values) {
        return Joiner.on(separator).join(values);
    }
}
