/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  RetryTest.java
 */

package dataflow.core.retry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.DataFlowTestExtension;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.parser.DataFlowParser;
import java.util.function.Consumer;
import org.junit.Before;
import org.junit.Test;

public class RetryTest {

    private DataFlowEngine engine;
    private DataFlowParser parser;

    @Before
    public void setUp() {
        this.engine = new DataFlowEngine();
        this.parser = new DataFlowParser(engine);
        DataFlowTestExtension.register(engine);
    }

    @Test
    public void testRetry_retryThenSuccess() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-retryThenSuccess.yaml");
        instance.execute();
        assertEquals("attempt1,attempt2,success", instance.getOutput());
    }

    @Test
    public void testRetry_retryConstValueProvider() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-retryConstValueProvider.yaml");
        instance.execute();
        assertEquals("attempt1,attempt2,success", instance.getOutput());
    }

    @Test
    public void testRetry_retryThenFailure() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-retryThenFailure.yaml");
        try {
            instance.execute();
            fail("expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("java.lang.RuntimeException: simulating a failure", ex.getMessage());
        }
    }

    @Test
    public void testRetry_defaultRetry() throws Exception {
        // Uses the default retry behavior supplied through dependency injection.
        DataFlowInstance instance = registerAndGetInstance("TestFlow-defaultRetry.yaml", (newInstance) ->
            newInstance.setDefaultRetryBehavior(new Retry(io.github.resilience4j.retry.Retry.ofDefaults("default"))));
        instance.execute();
        assertEquals("attempt1,attempt2,success", instance.getOutput());
    }

    private DataFlowInstance registerAndGetInstance(String filename) throws Exception {
        return registerAndGetInstance(filename, instance -> { });
    }

    private DataFlowInstance registerAndGetInstance(String filename, Consumer<DataFlowInstance> initFn) throws Exception {
        DataFlowConfig config = parser.parse(getClass().getResourceAsStream(filename));
        engine.registerDataFlow(config);
        System.out.println(engine.getSource(config.getId()));
        return engine.newDataFlowInstance("flow", initFn);
    }
}