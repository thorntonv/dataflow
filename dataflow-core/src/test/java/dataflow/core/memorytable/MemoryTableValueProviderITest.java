/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MemoryTableValueProviderITest.java
 */

package dataflow.core.memorytable;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.google.common.collect.ImmutableMap;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.DataFlowEventListener;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.DataFlowTestExtension;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.parser.DataFlowParser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class MemoryTableValueProviderITest {

    private DataFlowEngine engine;
    private DataFlowParser parser;
    private MemoryTableManager memoryTableManager;
    private AtomicInteger loadCount;
    private AtomicInteger loadErrorCount;
    private AtomicInteger updateCount;
    private AtomicInteger unloadCount;

    @Before
    public void setUp() {
        this.engine = new DataFlowEngine();
        this.parser = new DataFlowParser(engine);
        DataFlowExecutionContext context = DataFlowExecutionContext.createExecutionContext(null, engine);
        this.memoryTableManager = context.getDependencyInjector().getInstance(MemoryTableManager.class);

        DataFlowTestExtension.register(engine);

        loadCount = new AtomicInteger();
        loadErrorCount = new AtomicInteger();
        updateCount = new AtomicInteger();
        unloadCount = new AtomicInteger();

        engine.getEventManager().addListener(new DataFlowEventListener() {
            @Override
            public void onDataFlowLoad(final String dataFlowId, final long timestamp) {
                loadCount.incrementAndGet();
            }

            @Override
            public void onDataFlowLoadError(final String dataFlowId, final Throwable error) {
                loadErrorCount.incrementAndGet();
            }

            @Override
            public void onDataFlowChanged(final String dataFlowId, final long timestamp) {
                updateCount.incrementAndGet();
            }

            @Override
            public void onDataFlowUnload(final String dataFlowId, final long timestamp) {
                unloadCount.incrementAndGet();
            }
        });

        assertEquals(0, memoryTableManager.getAllocatedMemoryTableDescriptorsCount());
    }

    @After
    public void cleanUp() throws InterruptedException {
        try {
            engine.unregisterDataFlow("flow");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if(memoryTableManager.getAllocatedMemoryTableDescriptorsCount() > 0) {
            assertEquals(0, memoryTableManager.getAllocatedMemoryTableDescriptorsCount());
        }
    }

    @Test
    public void testMissingKeyColumns() {
        try {
            register("TestFlow-missingKeyColumns.yaml");
            assertEquals(0, memoryTableManager.getAllocatedMemoryTableDescriptorsCount());
            getInstance();
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("keyColumns not set for MemoryTableValueProvider provider1", ex.getCause().getMessage());
        }
    }

    @Test
    public void testSingleRowMode() throws Exception {
        register("TestFlow-singleRowMode.yaml");
        try (DataFlowInstance instance = getInstance()) {
            instance.setValue("variant", "a");
            instance.setValue("jobTitleHashCodeValue", 844789886);
            instance.execute();
            Map<String, Object> output = (Map<String, Object>) instance.getOutput();
            assertEquals(.0060725375f, output.get("weight"));
        }
    }

    @Test
    public void testMultipleRowMode() throws Exception {
        register("TestFlow-multipleRowMode.yaml");
        try (DataFlowInstance instance = getInstance()) {
            instance.setValue("valueType", "jobTitleHashCode");
            instance.execute();
            List<Map<String, Object>> output = (List<Map<String, Object>>) instance.getOutput();
            assertEquals(368, output.size());
        }
    }

    @Test
    public void testMultipleRowMode_missingValue() throws Exception {
        register("TestFlow-multipleRowMode.yaml");
        try (DataFlowInstance instance = getInstance()) {
            instance.setValue("valueType", "missing");
            instance.execute();
            assertNull(instance.getOutput());
        }
    }

    @Test
    public void testSingleRowMode_missingValue() throws Exception {
        register("TestFlow-singleRowMode.yaml");
        try (DataFlowInstance instance = getInstance()) {
            instance.setValue("variant", "a");
            instance.setValue("jobTitleHashCodeValue", 1234);
            instance.execute();
            assertNull(instance.getOutput());
        }
    }

    @Test
    public void testSingleColumnMode() throws Exception {
        register("TestFlow-singleColumnMode.yaml");
        try (DataFlowInstance instance = getInstance()) {
            instance.setValue("jobTitleHashCodeValue", 844789886);
            instance.execute();
            assertEquals(.0060725375f, instance.getOutput());
        }
    }

    @Test
    public void testSingleColumnMode_missingValue() throws Exception {
        register("TestFlow-singleColumnMode.yaml");
        try (DataFlowInstance instance = getInstance()) {
            instance.setValue("jobTitleHashCodeValue", 12345);
            instance.execute();
            assertNull(instance.getOutput());
        }
    }

    @Test
    public void testPreload_filenameWithVar() throws Exception {
        register("TestFlow-preloadFilenameWithVar.yaml");
        // dataflow/core/memorytable/ffm.tsv and dataflow/core/memorytable/ffm-a.tsv are preloaded.
        assertEquals(2, memoryTableManager.getAllocatedMemoryTableDescriptorsCount());
    }

    @Test
    public void testPreload_filenameWithoutVar() throws Exception {
        register("TestFlow-preloadFilenameWithoutVar.yaml");
        // dataflow/core/memorytable/ffm.tsv is preloaded
        assertEquals(1, memoryTableManager.getAllocatedMemoryTableDescriptorsCount());
    }

    @Test
    public void testLazyLoadTable_filenameWithoutVar() throws Exception {
        register("TestFlow-lazyLoadFilenameWithoutVar.yaml");
        // There is no variable in the filename so it loaded when the first instance is created.
        assertEquals(1, memoryTableManager.getAllocatedMemoryTableDescriptorsCount());
    }

    @Test
    public void testLazyLoadTable_filenameWithVar() throws Exception {
        assertEquals(0, memoryTableManager.getAllocatedMemoryTableDescriptorsCount());
        register("TestFlow-lazyLoadFilenameWithVar.yaml");
        // There is a variable in the filename so it can't be loaded until the value is set and the instance is executed.
        assertEquals(0, memoryTableManager.getAllocatedMemoryTableDescriptorsCount());
        try (DataFlowInstance instance = getInstance()) {
            instance.setValue("variant", "a");
            instance.setValue("jobTitleHashCodeValue", 12345);
            instance.execute();
            assertEquals(1, memoryTableManager.getAllocatedMemoryTableDescriptorsCount());
            assertNull(instance.getOutput());
        }
    }

    @Test
    public void testAllKeysMode() throws Exception {
        register("TestFlow-allKeys.yaml");
        try (DataFlowInstance instance = getInstance()) {
            instance.execute();
            Set<?> keys = ((Stream<?>) instance.getOutput()).collect(Collectors.toSet());
            assertEquals(500, keys.size());

            assertTrue(keys.contains(ImmutableMap.of("valueType", "jobTitleHashCode", "value", 846627629)));
        }
    }

    @Test
    public void testAllRowsMode() throws Exception {
        register("TestFlow-allRows.yaml");
        try (DataFlowInstance instance = getInstance()) {
            instance.execute();
            Stream<Map<String, Object>> rowsStream = (Stream<Map<String, Object>>) instance.getOutput();
            List<?> rows = rowsStream.collect(Collectors.toList());
            assertEquals(500, rows.size());
            boolean found = true;
            for (Object rowObj : rows) {
                Map<String, Object> row = (Map<String, Object>) rowObj;
                if (row.get("valueType").equals("jobTitleHashCode") && row.get("value").equals(846627629)) {
                    found = true;
                }
            }
            assertTrue(found);
        }
    }

    private void register(String filename) throws Exception {
        DataFlowConfig config = parser.parse(getClass().getResourceAsStream(filename));
        engine.registerDataFlow(config);
        getInstance();
    }

    private DataFlowInstance getInstance() throws Exception {
        return engine.newDataFlowInstance("flow");
    }
}