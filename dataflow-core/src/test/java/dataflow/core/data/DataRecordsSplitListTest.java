/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataRecordsSplitListTest.java
 */

package dataflow.core.data;

import static org.junit.Assert.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;

public class DataRecordsSplitListTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
    }

    @Test
    public void getValue_excludeColumns() throws JsonProcessingException {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", "{\"values\":[{\"v1\":57, \"v2\": 3.14}, {\"v1\":\"102\", \"v2\":1.5}]}");
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test3", "values", null, null, ImmutableSet.of("test3"), null, null);
        DataRecords splitRecords = splitArray.getValue(records);

        assertEquals(2, splitRecords.size());
        assertEquals("{\"columnNames\":[\"test1\",\"test2\",\"v1\",\"v2\"],\"records\":[{\"test1\":\"A\",\"test2\":\"B\",\"v1\":57,\"v2\":3.14},{\"test1\":\"A\",\"test2\":\"B\",\"v1\":\"102\",\"v2\":1.5}]}", mapper.writeValueAsString(splitRecords));
    }

    @Test
    public void getValue_includeColumns() throws JsonProcessingException {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", "{\"values\":[{\"v1\":57, \"v2\": 3.14}, {\"v1\":\"102\", \"v2\":1.5}]}");
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test3", "values", null, ImmutableSet.of("test1"), null, null, null);
        DataRecords splitRecords = splitArray.getValue(records);

        assertEquals(2, splitRecords.size());
        assertEquals("{\"columnNames\":[\"test1\",\"v1\",\"v2\"],\"records\":[{\"test1\":\"A\",\"v1\":57,\"v2\":3.14},{\"test1\":\"A\",\"v1\":\"102\",\"v2\":1.5}]}", mapper.writeValueAsString(splitRecords));
    }

    @Test
    public void getValue_jsonArray() throws JsonProcessingException {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", "[{\"v1\":57, \"v2\": 3.14}, {\"v1\":\"102\", \"v2\":1.5}]");
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test3", null, null, null, ImmutableSet.of("test3"), null, null);
        DataRecords splitRecords = splitArray.getValue(records);

        assertEquals(2, splitRecords.size());
        assertEquals("{\"columnNames\":[\"test1\",\"test2\",\"v1\",\"v2\"],\"records\":[{\"test1\":\"A\",\"test2\":\"B\",\"v1\":57,\"v2\":3.14},{\"test1\":\"A\",\"test2\":\"B\",\"v1\":\"102\",\"v2\":1.5}]}", mapper.writeValueAsString(splitRecords));
    }

    @Test
    public void getValue_missingListPath() {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", "{\"values\":[{\"v1\":57, \"v2\": 3.14}, {\"v1\":\"102\", \"v2\":1.5}]}");
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test3", "values.invalid", null, ImmutableSet.of("test1"), null, null, null);
        DataRecords splitRecords = splitArray.getValue(records);

        assertEquals(0, splitRecords.size());
    }

    @Test
    public void getValue_invalidListPath() {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", "{\"values\":[{\"v1\":{}, \"v2\": 3.14}, {\"v1\":{}, \"v2\":1.5}]}");
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test3", "values.v1", null,  ImmutableSet.of("test1"), null,
            ImmutableMap.<String, String>builder()
                .put("val", ".")
                .build(), null);
        try {
            splitArray.getValue(records);
            fail("expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            assertEquals(
                "Value with path DataRecordPath{columnName='test3', path='values.v1'} must be a list",
                ex.getMessage());
        }
    }

    @Test
    public void getValue_invalidListPath_numeric() {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", "{\"values\":[{\"v1\":100, \"v2\": 3.14}, {\"v1\":101, \"v2\":1.5}]}");
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test3", "values.v1", null, ImmutableSet.of("test1"), null,
            ImmutableMap.<String, String>builder()
                .put("val", ".")
                .build(), null);
        try {
            splitArray.getValue(records);
            fail("expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            assertEquals(
                "Value with path DataRecordPath{columnName='test3', path='values.v1'} must be a list",
                ex.getMessage());
        }
    }

    @Test
    public void getValue_missingListColumnName() {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", "{\"values\":[{\"v1\":57, \"v2\": 3.14}, {\"v1\":\"102\", \"v2\":1.5}]}");
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test4", "values.invalid", null, ImmutableSet.of("test1"), null, null, null);
        DataRecords splitRecords = splitArray.getValue(records);

        assertEquals(0, splitRecords.size());
    }

    @Test
    public void getValue_invalidListColumnName() {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", "{\"values\":[{\"v1\":57, \"v2\": 3.14}, {\"v1\":\"102\", \"v2\":1.5}]}");
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test2", "values.invalid", null, ImmutableSet.of("test1"), null, null, null);

        try {
            splitArray.getValue(records);
            fail("expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            assertEquals("Value with path DataRecordPath{columnName='test2', path='values.invalid'} must be a list", ex.getMessage());
        }
    }

    @Test
    public void getValue_emptyList() {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", "{\"values\":[]}");
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test3", "values.invalid", null, ImmutableSet.of("test1"), null, null, null);
        DataRecords splitRecords = splitArray.getValue(records);

        assertEquals(0, splitRecords.size());
    }

    @Test
    public void getValue_nullList() throws JsonProcessingException {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", "{\"values\":[{\"v1\":57, \"v2\": 3.14}, {\"v1\":\"102\", \"v2\":1.5}]}");
        records.add(record);
        record = new DataRecord();
        record.put("test1", "C");
        record.put("test2", "D");
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test3", "values", null, null, ImmutableSet.of("test3"), null, null);
        DataRecords splitRecords = splitArray.getValue(records);

        assertEquals(2, splitRecords.size());
        assertEquals("{\"columnNames\":[\"test1\",\"test2\",\"v1\",\"v2\"],\"records\":[{\"test1\":\"A\",\"test2\":\"B\",\"v1\":57,\"v2\":3.14},{\"test1\":\"A\",\"test2\":\"B\",\"v1\":\"102\",\"v2\":1.5}]}", mapper.writeValueAsString(splitRecords));
    }

    @Test
    public void getValue_multipleRecords() throws JsonProcessingException {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", "{\"values\":[{\"v1\":57, \"v2\": 3.14}, {\"v1\":\"102\", \"v2\":1.5}]}");
        records.add(record);
        record = new DataRecord();
        record.put("test1", "C");
        record.put("test2", "D");
        record.put("test3", "{\"values\":[{\"v1\":75, \"v2\": 4.13}]}");
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test3", "values", null, null, ImmutableSet.of("test3"), null, null);
        DataRecords splitRecords = splitArray.getValue(records);

        assertEquals(3, splitRecords.size());
        assertEquals("{\"columnNames\":[\"test1\",\"test2\",\"v1\",\"v2\"],\"records\":[{\"test1\":\"A\",\"test2\":\"B\",\"v1\":57,\"v2\":3.14},{\"test1\":\"A\",\"test2\":\"B\",\"v1\":\"102\",\"v2\":1.5},{\"test1\":\"C\",\"test2\":\"D\",\"v1\":75,\"v2\":4.13}]}",
            mapper.writeValueAsString(splitRecords));
    }

    @Test
    public void getValue_columnNameItemPathMap() throws JsonProcessingException {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", "{\"values\":[{\"v1\":57, \"v2\": 3.14}, {\"v1\":\"102\", \"v2\":1.5}]}");
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test3", "values", null, null, ImmutableSet.of("test3"),
            ImmutableMap.<String, String>builder()
                .put("value1", "v1")
                .build(), null);
        DataRecords splitRecords = splitArray.getValue(records);

        assertEquals(2, splitRecords.size());
        assertEquals("{\"columnNames\":[\"test1\",\"test2\",\"value1\"],\"records\":[{\"test1\":\"A\",\"test2\":\"B\",\"value1\":57},{\"test1\":\"A\",\"test2\":\"B\",\"value1\":\"102\"}]}",
            mapper.writeValueAsString(splitRecords));
    }

    @Test
    public void getValue_columnNameItemPathMap_nulls() throws JsonProcessingException {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", "{\"values\":[{\"v1\":57, \"v2\": 3.14}, {\"v2\":1.5}]}");
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test3", "values", null, null, ImmutableSet.of("test3"),
            ImmutableMap.<String, String>builder()
                .put("value1", "v1")
                .build(), null);
        DataRecords splitRecords = splitArray.getValue(records);

        assertEquals(2, splitRecords.size());
        assertEquals("{\"columnNames\":[\"test1\",\"test2\",\"value1\"],\"records\":[{\"test1\":\"A\",\"test2\":\"B\",\"value1\":57},{\"test1\":\"A\",\"test2\":\"B\",\"value1\":null}]}",
            mapper.writeValueAsString(splitRecords));
    }

    @Test
    public void getValue_listColumn() throws JsonProcessingException {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", ImmutableList.of("red", "green", "blue"));
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test3", null, null, ImmutableSet.of("test1"), null,
            ImmutableMap.<String, String>builder()
                .put("color", ".")
                .build(), null);
        DataRecords splitRecords = splitArray.getValue(records);

        assertEquals(3, splitRecords.size());
        assertEquals("{\"columnNames\":[\"color\",\"test1\"],\"records\":[{\"color\":\"red\",\"test1\":\"A\"},{\"color\":\"green\",\"test1\":\"A\"},{\"color\":\"blue\",\"test1\":\"A\"}]}",
            mapper.writeValueAsString(splitRecords));
    }

    @Test
    public void getValue_notListColumn() {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", "C");
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test3", null, null, ImmutableSet.of("test1"), null, null, null);
        try {
            splitArray.getValue(records);
        } catch (IllegalArgumentException ex) {
            assertEquals(
                "Value with path DataRecordPath{columnName='test3', path='null'} must be a list",
                ex.getMessage());
        }
    }

    @Test
    public void getValue_listColumn_listItemsNotMaps() {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", ImmutableList.of("red", "green", "blue"));
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test3", null, null, ImmutableSet.of("test1"), null, null, null);
        try {
            splitArray.getValue(records);
            fail("expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            assertEquals(
                "Unable to split list. columnNameItemPathMap is not set and the list items are not maps",
                ex.getMessage());
        }
    }

    @Test
    public void getValue_nestedLists() throws JsonProcessingException {
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", "{\"values\":[{\"v1\":[\"red\", \"green\"], \"v2\": 3.14}, {\"v1\":[\"blue\", \"purple\"], \"v2\":1.5}]}");
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test3", "values.v1", null, ImmutableSet.of("test1"), null,
            ImmutableMap.<String, String>builder()
            .put("color", ".")
            .build(), null);
        DataRecords splitRecords = splitArray.getValue(records);

        assertEquals(4, splitRecords.size());
        assertEquals("{\"columnNames\":[\"color\",\"test1\"],\"records\":[{\"color\":\"red\",\"test1\":\"A\"},{\"color\":\"green\",\"test1\":\"A\"},{\"color\":\"blue\",\"test1\":\"A\"},{\"color\":\"purple\",\"test1\":\"A\"}]}",
            mapper.writeValueAsString(splitRecords));
    }

    @Test
    public void getValue_keyColumnNotCopied() {
        DataRecords records = new DataRecords();
        records.setKeyColumnNames(ImmutableList.of("test1"));
        DataRecord record = new DataRecord();
        record.put("test1", "A");
        record.put("test2", "B");
        record.put("test3", "{\"values\":[{\"v1\":57, \"v2\": 3.14}, {\"v1\":\"102\", \"v2\":1.5}]}");
        records.add(record);

        DataRecordsSplitList splitArray = new DataRecordsSplitList(
            "test3", "values", null, ImmutableSet.of("test2"), null, null, null);
        try {
            splitArray.getValue(records);
            fail("expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            assertEquals("Key columns [test1] are missing from the generated records", ex.getMessage());
        }
    }
}