/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  JoinDataRecordsUTest.java
 */

package dataflow.core.data;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

public class JoinDataRecordsUTest extends TestCase {

    private JoinDataRecords joinDataRecords;

    @Before
    public void setUp() {
        this.joinDataRecords = new JoinDataRecords();
    }

    public void testGetValue_singleKeyColumn() {
        List<DataRecords> testData = buildTestData();
        DataRecords joined = joinDataRecords.getValue(testData);

        assertEquals(3, joined.size());
        assertEquals(ImmutableList.of("id"), joined.getKeyColumnNames());
        assertEquals(ImmutableList.of("id", "postalCode", "favoriteColor", "age"),
            joined.getColumnNames());

        assertEquals(1, joined.get(0).get("id"));
        assertEquals("green", joined.get(0).get("favoriteColor"));
        assertEquals(31, joined.get(0).get("age"));

        assertEquals(2, joined.get(1).get("id"));
        assertEquals("red", joined.get(1).get("favoriteColor"));
        assertEquals(28, joined.get(1).get("age"));

        assertEquals(3, joined.get(2).get("id"));
        assertEquals("orange", joined.get(2).get("favoriteColor"));
        assertEquals(null, joined.get(2).get("age"));
    }

    public void testGetValue_multipleKeyColumn() {
        List<DataRecords> testData = buildTestData();
        testData.forEach(r -> r.setKeyColumnNames(ImmutableList.of("id", "postalCode")));

        DataRecords joined = joinDataRecords.getValue(testData);

        assertEquals(3, joined.size());
        assertEquals(ImmutableList.of("id", "postalCode"), joined.getKeyColumnNames());
        assertEquals(ImmutableList.of("id", "postalCode", "favoriteColor", "age"),
            joined.getColumnNames());

        assertEquals(1, joined.get(0).get("id"));
        assertEquals("green", joined.get(0).get("favoriteColor"));
        assertEquals(31, joined.get(0).get("age"));

        assertEquals(2, joined.get(1).get("id"));
        assertEquals("red", joined.get(1).get("favoriteColor"));
        assertEquals(28, joined.get(1).get("age"));

        assertEquals(3, joined.get(2).get("id"));
        assertEquals("orange", joined.get(2).get("favoriteColor"));
        assertEquals(null, joined.get(2).get("age"));
    }

//    public void testGetValue_notUniqueKeyColumn() {
//        List<DataRecords> testData = buildTestData();
//        testData.forEach(r -> r.setKeyColumnNames(ImmutableList.of("postalCode")));
//
//        try {
//            joinDataRecords.getValue(testData);
//            fail("expected exception was not thrown");
//        } catch (Exception ex) {
//            // Expected.
//            assertEquals("Non-unique values for column favoriteColor joining on [postalCode] in records: [DataRecord{favoriteColor=green, id=1, postalCode=23228}, DataRecord{favoriteColor=orange, id=3, postalCode=23228}, DataRecord{age=31, id=1, postalCode=23228}]", ex.getMessage());
//        }
//    }

    public void testGetValue_recordsWithMissingJoinColumn() {
        List<DataRecords> testData = buildTestData();
        testData.get(0).setKeyColumnNames(ImmutableList.of("id", "favoriteColor"));
        try {
            joinDataRecords.getValue(testData);
            fail("expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            // Expected.
            assertTrue(ex.getMessage().contains("is missing key columns: [favoriteColor]"));
        }
    }

    @Test
    public void testGetValue_recordWithNullColumnValue() {
        List<DataRecords> testData = buildTestData();
        assertEquals("DataRecords{keyColumnNames=[id], columnNames=[id, postalCode, favoriteColor, age], records=[id:1 {age=31, favoriteColor=green, id=1, postalCode=23228}], [id:2 {age=28, favoriteColor=red, id=2, postalCode=12065}], [id:3 {favoriteColor=orange, id=3, postalCode=23228}]}",
            joinDataRecords.getValue(testData).toString());
        testData.get(0).get(0).put("favoriteColor", null);
    }

    @Test
    public void testGetValue_recordWithNullColumnValue_nonUnique() {
        List<DataRecords> testData = buildTestData();
        testData.get(0).get(0).put("postalCode", null);
        assertEquals("DataRecords{keyColumnNames=[id], columnNames=[id, postalCode, favoriteColor, age], records=[id:1 {age=31, favoriteColor=green, id=1, postalCode=23228}], [id:2 {age=28, favoriteColor=red, id=2, postalCode=12065}], [id:3 {favoriteColor=orange, id=3, postalCode=23228}]}",
            joinDataRecords.getValue(testData).toString());
    }

    private List<DataRecords> buildTestData() {
        List<DataRecords> testData = new ArrayList<>();
        DataRecords records = new DataRecords();
        records.setKeyColumnNames(ImmutableList.of("id"));
        records.setColumnNames(ImmutableList.of("id", "postalCode", "favoriteColor"));
        records.add(new DataRecord(ImmutableMap.<String, Object>builder()
            .put("id", 1)
            .put("postalCode", 23228)
            .put("favoriteColor", "green")
            .build()));
        records.add(new DataRecord(ImmutableMap.<String, Object>builder()
            .put("id", 2)
            .put("postalCode", 12065)
            .put("favoriteColor", "red")
            .build()));
        records.add(new DataRecord(ImmutableMap.<String, Object>builder()
            .put("id", 3)
            .put("postalCode", 23228)
            .put("favoriteColor", "orange")
            .build()));
        testData.add(records);

        records = new DataRecords();
        records.setKeyColumnNames(ImmutableList.of("id"));
        records.setColumnNames(ImmutableList.of("id", "postalCode", "age"));
        records.add(new DataRecord(ImmutableMap.<String, Object>builder()
            .put("id", 1)
            .put("postalCode", 23228)
            .put("age", 31)
            .build()));
        records.add(new DataRecord(ImmutableMap.<String, Object>builder()
            .put("id", 2)
            .put("postalCode", 12065)
            .put("age", 28)
            .build()));
        testData.add(records);
        return testData;
    }
}