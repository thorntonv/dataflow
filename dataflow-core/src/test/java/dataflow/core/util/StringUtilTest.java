/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  StringUtilTest.java
 */

package dataflow.core.util;

import java.util.Collections;

import org.junit.Test;

import com.google.common.collect.ImmutableList;

import static org.junit.Assert.*;

public class StringUtilTest {

    @Test
    public void join() {
        assertEquals("a,b,c", StringUtil.join(ImmutableList.of("a", "b", "c"), ","));
    }

    @Test
    public void join_singleValue() {
        assertEquals("a", StringUtil.join(ImmutableList.of("a"), ","));
    }

    @Test
    public void join_noValues() {
        assertEquals("", StringUtil.join(Collections.emptyList(), ","));
    }
}