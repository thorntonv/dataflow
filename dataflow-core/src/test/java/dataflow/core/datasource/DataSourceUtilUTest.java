/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataSourceUtilUTest.java
 */

package dataflow.core.datasource;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Test;
import com.google.common.collect.ImmutableList;
import dataflow.test.datasource.TestDataSource;

import static org.junit.Assert.*;

/**
 * Unit test for {@link DataSourceUtil}.
 */
public class DataSourceUtilUTest {

    private static final String FILE1_PATH = "/dir1/file-a.txt";
    private static final String FILE2_PATH = "/dir1/file-b.txt";
    private static final String FILE3_PATH = "/dir2/file-y.txt";
    private static final String FILE4_PATH = "/dir2/file-yz.txt";
    private static final String DIR1_PATH = "/dir1/";
    private static final String DIR2_PATH = "/dir2/";

    @Test
    public void testGetClassPathDataSourcesMatchingGlobPattern() throws IOException {
        List<DataSource> matches = DataSourceUtil.getClassPathDataSourcesMatchingGlobPattern(
                "dataflow/core/parser/test-flow-*.yaml");
        assertTrue(matches.size() > 0);
    }

    @Test
    public void testGetChildWithPath() throws IOException {
        DataSource root = buildTestRoot();
        DataSource file1 = DataSourceUtil.getChildWithPath(root, FILE1_PATH).orElse(null);
        assertEquals("file-a.txt", file1.getName());
    }

    @Test
    public void testGetPathElements_rootPath() {
        List<String> pathElements = DataSourceUtil.getPathElements("/");
        assertEquals(ImmutableList.of("/"), pathElements);
    }

    @Test
    public void testGetPathElements_singleDirectory() {
        List<String> pathElements = DataSourceUtil.getPathElements("/dir1/");
        assertEquals(ImmutableList.of("/", "dir1/"), pathElements);
    }

    @Test
    public void testGetPathElements_multipleDirectories() {
        List<String> pathElements = DataSourceUtil.getPathElements("/dir1/dir2/");
        assertEquals(ImmutableList.of("/", "dir1/", "dir2/"), pathElements);
    }

    @Test
    public void testGetPathElements_file() {
        List<String> pathElements = DataSourceUtil.getPathElements("/dir1/dir2/myfile.csv");
        assertEquals(ImmutableList.of("/", "dir1/", "dir2/", "myfile.csv"), pathElements);
    }

    @Test
    public void testNormalizePathSeparator() {
        assertEquals("c:/files/myfile.txt", DataSourceUtil.normalizePathSeparator("c:\\files\\myfile.txt", "\\"));
    }

    @Test
    public void testNormalizePathSeparator_dataSourceSeparator() {
        String path = "/var/test/test1.txt";
        assertEquals(path, DataSourceUtil.normalizePathSeparator(path, "/"));
    }

    @Test
    public void testGetParentPath_root() {
        assertNull(DataSourceUtil.getParentPath("/"));
    }

    @Test
    public void testGetParentPath_topLevelDir() {
        assertEquals("/", DataSourceUtil.getParentPath("/dir1/"));
    }

    @Test
    public void testGetParentPath_topLevelFile() {
        assertEquals("/", DataSourceUtil.getParentPath("/test.txt"));
    }

    @Test
    public void testGetParentPath_nestedDir() {
        assertEquals("/dir1/", DataSourceUtil.getParentPath("/dir1/dir2/"));
    }

    @Test
    public void testGetParentPath_nestedFile() {
        assertEquals("/dir1/", DataSourceUtil.getParentPath("/dir1/test.txt"));
    }

    @Test
    public void testGetDataSourcesMatchingGlobPattern_noMatches() throws IOException {
        TestDataSource root = buildTestRoot();
        List<DataSource> matches = DataSourceUtil.getDataSourcesMatchingGlobPattern(root, "/dir1/dir2/file-1.txt");
        assertEquals(0, matches.size());
    }

    @Test
    public void testGetDataSourcesMatchingGlobPattern_filenameWildcard() throws IOException {
        TestDataSource root = buildTestRoot();
        Map<String, DataSource> pathDataSourceMap = buildPathDataSourceMap(
                DataSourceUtil.getDataSourcesMatchingGlobPattern(root, "/dir1/file-*.txt"));
        assertEquals(2, pathDataSourceMap.size());
        assertTrue(pathDataSourceMap.containsKey(FILE1_PATH));
        assertTrue(pathDataSourceMap.containsKey(FILE2_PATH));
    }

    @Test
    public void testGetDataSourcesMatchingGlobPattern_filenameWildcard_noLeadingSeparator() throws IOException {
        TestDataSource root = buildTestRoot();
        Map<String, DataSource> pathDataSourceMap = buildPathDataSourceMap(
                DataSourceUtil.getDataSourcesMatchingGlobPattern(root, "dir1/file-*.txt"));
        assertEquals(2, pathDataSourceMap.size());
        assertTrue(pathDataSourceMap.containsKey(FILE1_PATH));
        assertTrue(pathDataSourceMap.containsKey(FILE2_PATH));
    }

    @Test
    public void testGetDataSourcesMatchingGlobPattern_dirWildcard() throws IOException {
        TestDataSource root = buildTestRoot();
        Map<String, DataSource> pathDataSourceMap = buildPathDataSourceMap(
                DataSourceUtil.getDataSourcesMatchingGlobPattern(root, "/dir*/file-*.txt"));
        assertEquals(4, pathDataSourceMap.size());
        assertTrue(pathDataSourceMap.containsKey(FILE1_PATH));
        assertTrue(pathDataSourceMap.containsKey(FILE2_PATH));
        assertTrue(pathDataSourceMap.containsKey(FILE3_PATH));
        assertTrue(pathDataSourceMap.containsKey(FILE4_PATH));
    }

    @Test
    public void testGetDataSourcesMatchingGlobPattern_noWildcard_noMatch() throws IOException {
        TestDataSource root = buildTestRoot();
        Map<String, DataSource> pathDataSourceMap = buildPathDataSourceMap(
                DataSourceUtil.getDataSourcesMatchingGlobPattern(root, "/dir1/temp.txt"));
        assertEquals(0, pathDataSourceMap.size());
    }

    @Test
    public void testGetDataSourcesMatchingGlobPattern_noWildcard_match() throws IOException {
        TestDataSource root = buildTestRoot();
        Map<String, DataSource> pathDataSourceMap = buildPathDataSourceMap(
                DataSourceUtil.getDataSourcesMatchingGlobPattern(root, FILE1_PATH));
        assertEquals(1, pathDataSourceMap.size());
        assertTrue(pathDataSourceMap.containsKey(FILE1_PATH));
    }

    @Test
    public void testGetDataSourcesMatchingGlobPattern_singleCharacterWildcard() throws IOException {
        TestDataSource root = buildTestRoot();
        Map<String, DataSource> pathDataSourceMap = buildPathDataSourceMap(
                DataSourceUtil.getDataSourcesMatchingGlobPattern(root, "/dir2/file-??.txt"));
        assertEquals(1, pathDataSourceMap.size());
        assertTrue(pathDataSourceMap.containsKey(FILE4_PATH));
    }

    private Map<String, DataSource> buildPathDataSourceMap(List<DataSource> dataSources) {
        return dataSources.stream().collect(Collectors.toMap(DataSource::getPath, dataSource -> dataSource));
    }

    private TestDataSource buildTestRoot() {
        TestDataSource file1 = new TestDataSource(FILE1_PATH, "");
        TestDataSource file2 = new TestDataSource(FILE2_PATH, "");
        TestDataSource dir1 = new TestDataSource(DIR1_PATH, "")
                .addChild(file1)
                .addChild(file2);
        TestDataSource file3 = new TestDataSource(FILE3_PATH, "");
        TestDataSource file4 = new TestDataSource(FILE4_PATH, "");
        TestDataSource dir2 = new TestDataSource(DIR2_PATH, "")
                .addChild(file3)
                .addChild(file4);
        return new TestDataSource("/", "")
                .addChild(dir1)
                .addChild(dir2);
    }
}