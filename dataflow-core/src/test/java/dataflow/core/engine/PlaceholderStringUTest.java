/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  PlaceholderStringUTest.java
 */

package dataflow.core.engine;

import org.junit.Test;
import com.google.common.collect.ImmutableMap;

import static org.junit.Assert.*;

/**
 * Unit test for {@link PlaceholderString}.
 */
public class PlaceholderStringUTest {

    @Test
    public void testToString_singlePlaceholderAtBeginning() {
        PlaceholderString provider = new PlaceholderString("$(value1)abc123");
        assertEquals("xyzabc123", provider.toString(ImmutableMap.<String, Object>builder()
                .put("value1", "xyz")
                .build()));
    }

    @Test
    public void testToString_singlePlaceholderInMiddle() {
        PlaceholderString provider = new PlaceholderString("abc$(value1)123");
        assertEquals("abcxyz123", provider.toString(ImmutableMap.<String, Object>builder()
                .put("value1", "xyz")
                .build()));
    }

    @Test
    public void testToString_singlePlaceholderAtEnd() {
        PlaceholderString provider = new PlaceholderString("abc123$(value1)");
        assertEquals("abc123xyz", provider.toString(ImmutableMap.<String, Object>builder()
                .put("value1", "xyz")
                .build()));
    }

    @Test
    public void testToString_multiplePlaceholders() {
        PlaceholderString provider = new PlaceholderString("$(value1)abc$(value2)123$(value3)");
        assertEquals("xyzabc!@#123lmno", provider.toString(ImmutableMap.<String, Object>builder()
                .put("value1", "xyz")
                .put("value2", "!@#")
                .put("value3", "lmno")
                .build()));
    }

    @Test
    public void testToString_multiplePlaceholderAtBeginning() {
        PlaceholderString provider = new PlaceholderString("$(value1)$(value2)abc123");
        assertEquals("xyz!@#abc123", provider.toString(ImmutableMap.<String, Object>builder()
                .put("value1", "xyz")
                .put("value2", "!@#")
                .build()));
    }

    @Test
    public void testToString_multiplePlaceholderInMiddle() {
        PlaceholderString provider = new PlaceholderString("abc$(value1)$(value2)123");
        assertEquals("abcxyz!@#123", provider.toString(ImmutableMap.<String, Object>builder()
                .put("value1", "xyz")
                .put("value2", "!@#")
                .build()));
    }

    @Test
    public void testToString_multiplePlaceholderAtEnd() {
        PlaceholderString provider = new PlaceholderString("abc123$(value1)$(value2)");
        assertEquals("abc123xyz!@#", provider.toString(ImmutableMap.<String, Object>builder()
                .put("value1", "xyz")
                .put("value2", "!@#")
                .build()));
    }

    @Test
    public void testToString_multiplePlaceholdersSameKey() {
        PlaceholderString provider = new PlaceholderString("$(value1)abc$(value1)123$(value1)");
        assertEquals("xyzabcxyz123xyz", provider.toString(ImmutableMap.<String, Object>builder()
                .put("value1", "xyz")
                .build()));
    }

    @Test
    public void testToString_missingPlaceholderValue() {
        PlaceholderString provider = new PlaceholderString("$(value1)abc$(value2)123$(value1)");
        try {
            String result = provider.toString(ImmutableMap.<String, Object>builder()
                    .put("value1", "xyz")
                    .build());
            fail("Expected exception was not thrown. Method returned: " + result);
        } catch (IllegalArgumentException ex) {
            // Expected.
        }
    }

}