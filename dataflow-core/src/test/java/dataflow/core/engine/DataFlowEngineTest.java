/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowEngineTest.java
 */

package dataflow.core.engine;

import com.google.common.collect.ImmutableMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Before;
import org.junit.Test;
import com.google.common.collect.ImmutableSet;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.parser.DataFlowParser;
import dataflow.test.provider.TestPreloadingValueProvider;

import static org.junit.Assert.*;

public class DataFlowEngineTest {

    private DataFlowEngine engine;
    private DataFlowParser parser;
    private AtomicInteger loadCount;
    private AtomicInteger loadErrorCount;
    private AtomicInteger updateCount;
    private AtomicInteger unloadCount;

    @Before
    public void setUp() {
        this.engine = new DataFlowEngine();
        this.parser = new DataFlowParser(engine);

        DataFlowTestExtension.register(engine);

        loadCount = new AtomicInteger();
        loadErrorCount = new AtomicInteger();
        updateCount = new AtomicInteger();
        unloadCount = new AtomicInteger();

        engine.getEventManager().addListener(new DataFlowEventListener() {
            @Override
            public void onDataFlowLoad(final String dataFlowId, final long timestamp) {
                loadCount.incrementAndGet();
            }

            @Override
            public void onDataFlowLoadError(final String dataFlowId, final Throwable error) {
                loadErrorCount.incrementAndGet();
            }

            @Override
            public void onDataFlowChanged(final String dataFlowId, final long timestamp) {
                updateCount.incrementAndGet();
            }

            @Override
            public void onDataFlowUnload(final String dataFlowId, final long timestamp) {
                unloadCount.incrementAndGet();
            }
        });

        TestPreloadingValueProvider.preloadedValues.clear();
    }

    @Test
    public void testPreloadDataFlowValueProvider() throws Exception {
        assertTrue(TestPreloadingValueProvider.preloadedValues.isEmpty());
        try(DataFlowInstance instance = registerAndGetInstance("TestFlow-preload.yaml")) {
            instance.execute();
            assertEquals(ImmutableSet.of("A"), instance.getOutput());
        }
        assertFalse(TestPreloadingValueProvider.preloadedValues.isEmpty());
        engine.unregisterDataFlow("flow");
        assertTrue(TestPreloadingValueProvider.preloadedValues.isEmpty());
    }

    @Test
    public void testPreloadDataFlowValueProvider_loadError() throws Exception {
        assertTrue(TestPreloadingValueProvider.preloadedValues.isEmpty());
        try {
            registerAndGetInstance("TestFlow-preloadError.yaml");
        } catch (Exception ex) {
            assertEquals("Simulated load error", ex.getCause().getCause().getMessage());
        }
        assertEquals(0, loadCount.get());
        assertEquals(1, loadErrorCount.get());
        assertTrue(TestPreloadingValueProvider.preloadedValues.isEmpty());
    }

    @Test
    public void testPreloadDataFlowValueProvider_unloadError() throws Exception {
        assertTrue(TestPreloadingValueProvider.preloadedValues.isEmpty());
        try(DataFlowInstance instance = registerAndGetInstance("TestFlow-unloadError.yaml")) {
            instance.execute();
            assertEquals(ImmutableSet.of("A"), instance.getOutput());
        }
        assertFalse(TestPreloadingValueProvider.preloadedValues.isEmpty());
        try {
            engine.unregisterDataFlow("flow");
            fail("Expected exception was not thrown");
        } catch(Exception ex) {
            assertEquals("Failed to unload DataFlow flow", ex.getMessage());
        }
        assertFalse(TestPreloadingValueProvider.preloadedValues.isEmpty());
    }

    @Test
    public void testUpdateDataFlow() throws Exception {
        assertEquals(0, loadCount.get());
        DataFlowInstance instance = registerAndGetInstance("TestFlow-mapEntry.yaml");
        assertEquals(1, loadCount.get());
        instance.setValue("in1", "A");
        instance.setValue("in2", ImmutableMap.of("value", "B"));
        instance.execute();
        assertEquals("B", instance.getOutput());

        // Update with a different flow.
        assertEquals(0, updateCount.get());
        DataFlowConfig config = parser.parse(getClass().getResourceAsStream("TestFlow-joinList.yaml"));
        engine.updateDataFlow(config);
        assertEquals(1, updateCount.get());
        assertEquals(1, unloadCount.get());
        instance = engine.newDataFlowInstance("flow");
        instance.setValue("in1", "X");
        instance.setValue("in3", "Z");
        instance.execute();
        assertEquals("X:Y:Z", instance.getOutput());

        // Unload.
        engine.unregisterDataFlow("flow");
        assertEquals(2, unloadCount.get());

        try {
            engine.newDataFlowInstance("flow");
            fail("Expected exception was not thrown");
        } catch(IllegalArgumentException ex) {
            assertEquals("DataFlow flow is not registered", ex.getMessage());
        }
    }

    @Test
    public void testUnloadDataFlow() throws Exception {
        assertEquals(0, loadCount.get());
        DataFlowInstance instance = registerAndGetInstance("TestFlow-mapEntry.yaml");
        assertEquals(1, loadCount.get());
        instance.setValue("in1", "A");
        instance.setValue("in2", ImmutableMap.of("value", "B"));
        instance.execute();
        assertEquals("B", instance.getOutput());

        assertEquals(0, unloadCount.get());
        engine.unregisterDataFlow("flow");
        assertEquals(1, unloadCount.get());

        try {
            engine.newDataFlowInstance("flow");
            fail("Expected exception was not thrown");
        } catch(IllegalArgumentException ex) {
            assertEquals("DataFlow flow is not registered", ex.getMessage());
        }
    }

    @Test
    public void testUndefinedValueProvider() {
        try {
            registerAndGetInstance("TestFlow-undefinedValueProvider.yaml");
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("Failed to parse DataFlow configuration: could not determine a constructor for the tag !UndefinedValueProvider\n"
                + " in 'reader', line 4, column 7:\n"
                + "        - !UndefinedValueProvider\n"
                + "          ^\n", ex.getMessage());
        }
        assertEquals(0, loadCount.get());
        assertEquals(0, loadErrorCount.get());
        assertEquals(0, unloadCount.get());
    }

    @Test
    public void testUndefinedPropertyObjectType() {
        try {
            registerAndGetInstance("TestFlow-undefinedPropertyObjectType.yaml");
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("Failed to parse DataFlow configuration: could not determine a constructor for the tag !UndefinedTransformer\n"
                + " in 'reader', line 10, column 13:\n"
                + "              - !UndefinedTransformer\n"
                + "                ^\n", ex.getMessage());
        }
        assertEquals(0, loadCount.get());
        assertEquals(0, loadErrorCount.get());
        assertEquals(0, unloadCount.get());
    }

    private DataFlowInstance registerAndGetInstance(String filename) throws Exception {
        DataFlowConfig config = parser.parse(getClass().getResourceAsStream(filename));
        engine.registerDataFlow(config);
        return engine.newDataFlowInstance("flow");
    }
}