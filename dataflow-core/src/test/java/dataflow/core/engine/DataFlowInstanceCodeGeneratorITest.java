/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowInstanceCodeGeneratorITest.java
 */

package dataflow.core.engine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import com.google.common.io.Resources;
import dataflow.core.compiler.CompileException;
import dataflow.core.compiler.JavaCompilerUtil;
import dataflow.core.datasource.DataSourceDataFlowExtension;
import dataflow.core.engine.codegen.DataFlowInstanceCodeGenConfig;
import dataflow.core.engine.codegen.DataFlowInstanceCodeGenerator;
import dataflow.core.exception.DataFlowParseException;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.parser.DataFlowParser;
import java.io.IOException;
import java.nio.charset.Charset;
import org.junit.Before;
import org.junit.Test;

public class DataFlowInstanceCodeGeneratorITest {

    private DataFlowParser parser;
    private DataFlowEngine engine;

    @Before
    public void setUp() {
        engine = new DataFlowEngine();
        parser = new DataFlowParser(engine);
        DataSourceDataFlowExtension.register(engine);
    }

    @Test
    public void testGenerateSource_parser_testFlow2() throws DataFlowParseException, IOException {
        DataFlowConfig config = parser.parse(ClassLoader.getSystemResourceAsStream("dataflow/core/parser/test-flow-2.yaml"));

        DataFlowInstanceCodeGenerator generator = new DataFlowInstanceCodeGenerator(engine);

        DataFlowInstanceCodeGenConfig codeGenConfig = DataFlowInstanceCodeGenConfig.builder()
                .setPackage("dataflow.test")
                .setClassName("TestFlow2")
                .build();
        String source = generator.generateSource(config, codeGenConfig);

        try {
            assertNotNull(JavaCompilerUtil.compile(codeGenConfig.getFullName(), source,
                this.getClass().getClassLoader()));
        } catch (CompileException ex) {
            System.err.println(ex.getDiagnostics().toString());
            System.err.println("\nsource:\n\n" + source);
            fail(ex.getMessage());
        }
        String expected = Resources.toString(ClassLoader.getSystemResource("dataflow/core/parser/TestFlow2.java"), Charset.defaultCharset());
        assertEquals(expected, source);
    }
}
