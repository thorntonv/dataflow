/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MappedStreamValueProviderTest.java
 */

package dataflow.core.component.stream;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.DataFlowTestExtension;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.parser.DataFlowParser;

import static org.junit.Assert.assertEquals;

public class MappedStreamValueProviderTest {

    private DataFlowParser parser;
    private DataFlowEngine engine;

    @Before
    public void setUp() {
        this.engine = new DataFlowEngine();
        DataFlowTestExtension.register(engine);
        this.parser = new DataFlowParser(engine);
    }

    @Test
    public void getValue() throws Exception {
        Stream<String> stream = ImmutableList.of("a", "b", "c").stream();
        Stream<String> mappedStream = (Stream<String>) testFlow("MappedStreamTest.yaml",
                ImmutableMap.<String, Object>builder()
                        .put("stream", stream)
                        .build());
        assertEquals(ImmutableList.of("a_A", "b_A", "c_A"), mappedStream.collect(Collectors.toList()));
    }

    private Object testFlow(String filename, Map<String, Object> input) throws Exception {
        DataFlowConfig config = parser.parse(getClass().getResourceAsStream(filename));
        engine.registerDataFlow(config);
        try (DataFlowInstance instance = engine.newDataFlowInstance(config.getId())) {
            input.forEach(instance::setValue);
            instance.execute();
            return instance.getOutput();
        }
    }
}