/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowComponentMetadataGenerator.java
 */

package dataflow.annotation.processor;

import dataflow.annotation.metadata.DataFlowComponentInputMetadata;
import dataflow.annotation.metadata.DataFlowComponentMetadata;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;

import com.google.auto.service.AutoService;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.metadata.DataFlowConfigurablePropertyMetadata;
import dataflow.annotation.metadata.ValueConverterMetadata;
import dataflow.annotation.metadata.DataFlowComponentOutputMetadata;

import static dataflow.annotation.processor.AnnotationProcessorUtil.buildArrayString;
import static dataflow.annotation.processor.AnnotationProcessorUtil.javaEscape;

@SupportedAnnotationTypes("dataflow.annotation.DataFlowComponent")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@AutoService(Processor.class)
@SuppressWarnings("unused")
public class DataFlowComponentMetadataGenerator extends AbstractProcessor {

    private static final String[] IMPORT_CLASSES = new String[]{
            Map.class.getName(),
            HashMap.class.getName(),
            List.class.getName(),
            ArrayList.class.getName(),
            DataFlowComponentMetadata.class.getName(),
            DataFlowConfigurablePropertyMetadata.class.getName(),
            DataFlowComponentInputMetadata.class.getName(),
            DataFlowComponentOutputMetadata.class.getName(),
            ValueConverterMetadata.class.getName(),
    };

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for (TypeElement annotation : annotations) {
            Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
            for (Element element : annotatedElements) {
                if (!(element instanceof TypeElement)) {
                    printAndThrowError("Unexpected element annotated with " +
                            DataFlowComponent.class.getSimpleName() + " annotation", element);
                }
                if (!element.getModifiers().contains(Modifier.PUBLIC)) {
                    printAndThrowError("Only public classes can be annotated with " +
                            DataFlowComponent.class.getSimpleName() + " annotation", element);
                }
                if (element.getModifiers().contains(Modifier.ABSTRACT)) {
                    printAndThrowError("Classes annotated with " +
                            DataFlowComponent.class.getSimpleName() + " annotation cannot be abstract", element);
                }
                if (element.getEnclosingElement().getKind() == ElementKind.CLASS &&
                        !element.getModifiers().contains(Modifier.STATIC)) {
                    printAndThrowError("Classes annotated with " +
                            DataFlowComponent.class.getSimpleName() + " must be static when nested in another class", element);
                }

                try {
                    writeFile(((TypeElement) element));
                } catch (Exception e) {
                    printAndThrowError("Error generating component metadata: " + e.getMessage(), element);
                }
            }
        }

        return true;
    }

    private void writeFile(TypeElement componentType) throws IOException {
        DataFlowComponentMetadataBuilder metadataBuilder = new DataFlowComponentMetadataBuilder(processingEnv);
        DataFlowComponentMetadata metadata = metadataBuilder.buildMetadata(componentType);

        String metadataSimpleClassName = TypeUtil.getSimpleClassName(metadata.getComponentClassName().replace("$", ".")) + "Metadata";
        String metadataClassName = metadata.getComponentPackageName() + "." + metadataSimpleClassName;
        JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(metadataClassName);
        try (PrintWriter fileWriter = new PrintWriter(builderFile.openWriter())) {
            IndentPrintWriter out = new IndentPrintWriter(fileWriter);

            out.printf("package %s;%n", metadata.getComponentPackageName());
            out.println();

            writeImports(out);

            out.printf("public class %s extends DataFlowComponentMetadata {%n", metadataSimpleClassName);

            out.println();
            out.indent();

            writeFields(metadata, metadataSimpleClassName, out);
            writeConstructor(metadata, metadataSimpleClassName, out);

            out.unindent();
            out.println("}");
        }
    }

    private void writeFields(DataFlowComponentMetadata metadata, String metadataSimpleClassName, IndentPrintWriter out) {
        out.printf("public static final String TYPE = \"%s\";%n", metadata.getTypeName());
        out.println("public static final boolean IS_ASYNC = " + metadata.isAsync() + ";");
        out.println("public static final boolean HAS_ASYNC_GET = " + metadata.hasAsyncGet() + ";");
        out.println("public static final boolean HAS_DYNAMIC_INPUT = " + metadata.hasDynamicInput() + ";");
        out.printf("public static final String DESCRIPTION = \"%s\";%n", javaEscape(metadata.getDescription()));
        out.printf("public static final String COMPONENT_PACKAGE_NAME = \"%s\";%n", metadata.getComponentPackageName());
        out.printf("public static final String COMPONENT_CLASS_NAME = \"%s\";%n", metadata.getComponentClassName());

        out.println("public static final Map<String, DataFlowConfigurablePropertyMetadata> PROPERTY_METADATA_MAP = new HashMap<>();");
        out.println("public static final List<DataFlowComponentInputMetadata> INPUT_METADATA = new ArrayList<>();");
        out.println("public static final DataFlowComponentOutputMetadata OUTPUT_METADATA;");
        out.println("public static final ValueConverterMetadata CONVERTER_METADATA;");

        out.println();
        out.println("static {");
        out.indent();

        for (DataFlowConfigurablePropertyMetadata propMetadata : metadata.getPropertyMetadataMap().values()) {
            out.println("{");
            out.indent();
            writeMetadataVars(propMetadata.getName(), propMetadata.getRawType(),
                    propMetadata.isEnum(),
                    propMetadata.getSupportedTypes(), propMetadata.isRequired(), propMetadata.getDescription(), out);
            out.println("PROPERTY_METADATA_MAP.put(name, new DataFlowConfigurablePropertyMetadata(" +
                    "name, rawType, isEnum, supportedTypes, required, description));");
            out.unindent();
            out.println("}");
        }
        out.println();

        for (DataFlowComponentInputMetadata inputMetadata : metadata.getInputMetadata()) {
            out.println("{");
            out.indent();
            writeMetadataVars(inputMetadata.getName(), inputMetadata.getRawType(), true,
                    inputMetadata.getSupportedTypes(),
                    inputMetadata.isRequired(), inputMetadata.getDescription(), out);
            out.println("INPUT_METADATA.add(new DataFlowComponentInputMetadata(" +
                    "name, rawType, supportedTypes, required, description));");
            out.unindent();
            out.println("}");
        }
        out.println();

        DataFlowComponentOutputMetadata outputMetadata = metadata.getOutputMetadata();
        out.println("{");
        out.indent();
        writeMetadataVars(null, outputMetadata.getRawType(), true,
                outputMetadata.getSupportedTypes(), true, outputMetadata.getDescription(), out);
        out.println("OUTPUT_METADATA = new DataFlowComponentOutputMetadata(rawType, supportedTypes, description);");
        out.unindent();
        out.println("}");

        ValueConverterMetadata converterMetadata = metadata.getValueConverterMetadata();
        if (converterMetadata != null) {
            out.println("{");
            out.indent();
            out.printf("CONVERTER_METADATA = new ValueConverterMetadata(%s, \"%s\", \"%s\");%n",
                    converterMetadata.isLossy(), converterMetadata.getFromType(), converterMetadata.getToType());
            out.unindent();
            out.println("}");
        } else {
            out.println("CONVERTER_METADATA = null;");
        }

        out.unindent();
        out.println("}");

        out.printf("public static final DataFlowComponentMetadata INSTANCE = new %s();", metadataSimpleClassName);
        out.println();
    }

    private void writeConstructor(DataFlowComponentMetadata metadata, String metadataClassName, IndentPrintWriter out) {
        out.printf("public %s() {%n", metadataClassName);
        out.indent();
        out.println("super(TYPE, DESCRIPTION, COMPONENT_PACKAGE_NAME, COMPONENT_CLASS_NAME, " +
                "PROPERTY_METADATA_MAP, INPUT_METADATA, OUTPUT_METADATA, CONVERTER_METADATA, IS_ASYNC, HAS_ASYNC_GET, " +
                " HAS_DYNAMIC_INPUT);");
        out.unindent();
        out.println("}");
        out.println();
    }

    private void writeImports(IndentPrintWriter out) {
        for (String importClass : IMPORT_CLASSES) {
            out.printf("import %s;%n", importClass);
        }
        out.println();
    }

    private void writeMetadataVars(String name, String rawType, boolean isEnum,
            Set<String> supportedTypes, boolean required, String description, IndentPrintWriter out) {
        if (name != null) {
            out.printf("String name = \"%s\";%n", name);
        }
        out.printf("String rawType = \"%s\";%n", rawType);
        out.printf("boolean isEnum = %s;%n", isEnum);
        out.printf("String[] supportedTypes = new String[]{%s};%n",
                buildArrayString(supportedTypes));
        out.printf("boolean required = %s;%n", String.valueOf(required));
        out.printf("String description = \"%s\";%n", javaEscape(description));
    }

    private void printAndThrowError(String msg, Element element) {
        if(element != null) {
            msg = element.toString() + ": " + msg;
        }
        AnnotationProcessorUtil.printAndThrowError(msg, element, processingEnv);
    }
}
