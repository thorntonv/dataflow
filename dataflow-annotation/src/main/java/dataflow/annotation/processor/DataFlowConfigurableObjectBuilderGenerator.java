/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfigurableObjectBuilderGenerator.java
 */

package dataflow.annotation.processor;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;

import com.google.auto.service.AutoService;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.metadata.DataFlowConfigurablePropertyMetadata;

import static dataflow.annotation.processor.AnnotationProcessorUtil.printAndThrowError;
import static dataflow.annotation.processor.DataFlowConfigurablePropertyMetadataBuilder.SIMPLE_PROPERTY_TYPES;
import static dataflow.annotation.processor.TypeUtil.*;

/**
 * Generates the source code for builders of objects that are {@link dataflow.annotation.DataFlowConfigurable}.
 */
@SupportedAnnotationTypes("dataflow.annotation.DataFlowConfigurable")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@AutoService(Processor.class)
@SuppressWarnings("unused")
public class DataFlowConfigurableObjectBuilderGenerator extends AbstractProcessor {

    private static final String[] IMPORT_CLASSES = new String[]{
            Set.class.getName(),
            HashSet.class.getName(),
            Map.class.getName(),
            "java.util.Map.Entry",
            HashMap.class.getName(),
            List.class.getName(),
            BiFunction.class.getName(),
            Function.class.getName(),
            Optional.class.getName(),
            "dataflow.core.engine.DataFlowEngine",
            "dataflow.core.engine.DataFlowExecutionContext",
            "dataflow.core.engine.DataFlowConfigurableObjectBuilder",
            "dataflow.core.engine.ValueReference",
            "dataflow.core.engine.ValueType",
            "dataflow.core.parser.RawConfigurableObjectConfig"
    };

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        DataFlowConfigurablePropertyMetadataBuilder metadataBuilder = new DataFlowConfigurablePropertyMetadataBuilder(processingEnv);

        Map<TypeElement, List<DataFlowConfigurablePropertyMetadata>> classPropertyMetadataMap = new HashMap<>();
        for (TypeElement annotation : annotations) {
            Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
            // For each constructor annotated with DataFlowConfigurable
            for (Element constructorElement : annotatedElements) {
                try {
                    if (constructorElement.getKind() != ElementKind.CONSTRUCTOR) {
                        printAndThrowError(DataFlowConfigurable.class.getSimpleName() +
                                " can only be used on a constructor", constructorElement, processingEnv);
                    }
                    if (!constructorElement.getModifiers().contains(Modifier.PUBLIC)) {
                        printAndThrowError(DataFlowConfigurable.class.getSimpleName() +
                                " annotated constructor should be public", constructorElement, processingEnv);
                    }
                    // Get the class that has this constructor.
                    TypeElement classElement = (TypeElement) constructorElement.getEnclosingElement();
                    String classType = classElement.asType().toString();

                    if (!classElement.getModifiers().contains(Modifier.ABSTRACT) &&
                            classElement.getKind() == ElementKind.CLASS) {
                        if (classElement.getEnclosingElement().getKind() == ElementKind.CLASS &&
                                !classElement.getModifiers().contains(Modifier.STATIC)) {
                            printAndThrowError(DataFlowConfigurable.class.getSimpleName() +
                                    " class should be static when nested in another class", constructorElement, processingEnv);
                        }
                        List<DataFlowConfigurablePropertyMetadata> propertyMetadata =
                                metadataBuilder.buildPropertyMetadata(classElement);
                        classPropertyMetadataMap.put(classElement, propertyMetadata);
                    }
                } catch (Exception e) {
                    printAndThrowError("Error generating builder metadata: " + e.getMessage(), constructorElement, processingEnv);
                }
            }
        }

        for (Map.Entry<TypeElement, List<DataFlowConfigurablePropertyMetadata>> entry : classPropertyMetadataMap.entrySet()) {
            try {
                writePropertyBuilderFile(entry.getKey(), entry.getValue());
            } catch (IOException e) {
                throw new RuntimeException("Error generating builder", e);
            }
        }
        return true;
    }

    private void writePropertyBuilderFile(TypeElement configurableObjectClass,
            List<DataFlowConfigurablePropertyMetadata> propertyMetadata) throws IOException {
        String objectType = configurableObjectClass.asType().toString();
        objectType = removeGenericTypes(objectType);
        String packageName = processingEnv.getElementUtils().getPackageOf(configurableObjectClass).getQualifiedName().toString();
        if(packageName == null || packageName.trim().isEmpty()) {
            throw new RuntimeException("No package name specified for " + objectType);
        }
        String simpleClassName = getSimpleClassName(objectType) + "Builder";
        JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(packageName + "." + simpleClassName);
        try (PrintWriter fileWriter = new PrintWriter(builderFile.openWriter())) {

            IndentPrintWriter out = new IndentPrintWriter(fileWriter);

            out.printf("package %s;%n", packageName);
            out.println();

            writeImports(out);

            out.printf("public class %s implements DataFlowConfigurableObjectBuilder {%n", simpleClassName);
            out.println();
            out.indent();

            writeFields(out);
            writeConstructor(simpleClassName, out);
            writeBuildMethod(objectType, propertyMetadata, out);

            out.unindent();
            out.println("}");
        }
    }

    private void writeFields(IndentPrintWriter out) {
        out.println("private DataFlowEngine engine;");
        out.println();
    }

    private void writeConstructor(String simpleClassName, IndentPrintWriter out) {
        out.printf("public %s(DataFlowEngine engine) {%n", simpleClassName);
        out.indent();
        out.println("this.engine = engine;");
        out.unindent();
        out.println("}");
        out.println();
    }

    private void writeBuildMethod(String objectType,
            List<DataFlowConfigurablePropertyMetadata> propertyMetadataList, IndentPrintWriter out) {
        out.printf("public %s build(Map<String, Object> propertyValues, Map<String, Object> instanceValues) {%n", objectType);
        out.indent();

        StringBuilder params = new StringBuilder();
        for (DataFlowConfigurablePropertyMetadata propertyMetadata : propertyMetadataList) {
            String paramName = propertyMetadata.getName();
            if (params.length() > 0) {
                params.append(", ");
            }
            params.append(paramName);

            String typeWithoutGenerics = removeGenericTypes(propertyMetadata.getRawType());

            String propertyExpression = String.format("propertyValues.get(\"%s\")", paramName);
            String rawPropertyExpression = propertyExpression;

            if (propertyMetadata.isRequired() || isPrimitiveType(propertyMetadata.getRawType())) {
                out.printf("if(%s == null) {%n", propertyExpression);
                out.indent();
                out.printf("throw new IllegalArgumentException(\"%s %s property is not defined\");%n",
                        getSimpleClassName(objectType), paramName);
                out.unindent();
                out.println("}");
                out.println();
            }

            if (propertyMetadata.isEnum()) {
                propertyExpression = String.format("(%s != null) ? %s.valueOf((String)%s) : null",
                        propertyExpression, propertyMetadata.getRawType(), propertyExpression);
            } else if (List.class.getName().equals(typeWithoutGenerics)) {
                String[] genericTypes = getGenericTypes(propertyMetadata.getRawType());
                if (genericTypes.length == 1) {
                    String genericType = genericTypes[0];
                    if (!SIMPLE_PROPERTY_TYPES.contains(genericType)) {
                        propertyExpression = String.format("buildListObject((List) %s, \"%s\", instanceValues, engine)",
                                propertyExpression, genericType);
                    }
                }
            } else if (Map.class.getName().equals(typeWithoutGenerics)) {
                String[] genericTypes = getGenericTypes(propertyMetadata.getRawType());
                if (genericTypes.length == 2) {
                    String keyGenericType = genericTypes[0];
                    String valueGenericType = genericTypes[1];
                    if (!SIMPLE_PROPERTY_TYPES.contains(keyGenericType) ||
                        !SIMPLE_PROPERTY_TYPES.contains(valueGenericType)) {
                        propertyExpression = String.format(
                            "buildMapObject((Map<?,?>) %s, \"%s\", \"%s\", instanceValues, engine)",
                            propertyExpression, keyGenericType, valueGenericType);
                    }
                }
            } else if (!SIMPLE_PROPERTY_TYPES.contains(typeWithoutGenerics)) {
                out.printf("Optional<RawConfigurableObjectConfig> %sObj = (%s != null && %s instanceof RawConfigurableObjectConfig) ? Optional.of((RawConfigurableObjectConfig)%s) : Optional.empty();%n",
                        paramName, propertyExpression, propertyExpression, propertyExpression);
                propertyExpression = String.format(
                        "%sObj.isPresent() ? buildObject(%sObj.get().getType(), %sObj.get(), instanceValues, engine) : %s",
                        paramName, paramName, paramName, propertyExpression);
            }
            propertyExpression = String.format("((%s instanceof ValueReference) ? " +
                    "instanceValues.get(((ValueReference) %s).getComponentId()) : %s)",
                    rawPropertyExpression, rawPropertyExpression, propertyExpression);

            out.printf("%s %s = (%s) engine.convert(%s, ValueType.fromString(\"%s\"));%n",
                propertyMetadata.getRawType(), paramName, propertyMetadata.getRawType(),
                propertyExpression, propertyMetadata.getRawType());

            if (propertyMetadata.isRequired() && !isPrimitiveType(propertyMetadata.getRawType())) {
                out.printf("if (%s == null) {%n", paramName);
                out.indent();
                out.printf("throw new IllegalArgumentException(\"%s %s property is not defined\");%n",
                        getSimpleClassName(objectType), paramName);
                out.unindent();
                out.println("}");
            }
            out.println();
        }
        out.printf("%s obj = new %s(%s);%n", objectType, objectType, params);

        out.println("return injectDependencies(obj, engine);");
        out.unindent();
        out.println("}");
    }

    private void writeImports(IndentPrintWriter out) {
        for (String importClass : IMPORT_CLASSES) {
            out.printf("import %s;%n", importClass);
        }
        out.println();
    }
}
