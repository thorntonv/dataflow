/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowComponentOutputMetadata.java
 */

package dataflow.annotation.metadata;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Holds information about a dataflow component's output.
 */
public class DataFlowComponentOutputMetadata {

    private final String rawType;
    private final Set<String> supportedTypes;
    private final String description;

    public DataFlowComponentOutputMetadata(final String rawType, final String[] supportedTypes,
            final String description) {
        this.rawType = rawType;
        this.supportedTypes = new HashSet<>(Arrays.asList(supportedTypes));
        this.description = description;
    }

    public String getRawType() {
        return rawType;
    }

    public Set<String> getSupportedTypes() {
        return supportedTypes;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "DataFlowComponentOutputMetadata{" +
                "rawType='" + rawType + '\'' +
                ", supportedTypes=" + supportedTypes +
                ", description='" + description + '\'' +
                '}';
    }
}
