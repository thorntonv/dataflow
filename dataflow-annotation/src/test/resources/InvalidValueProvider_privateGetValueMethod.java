package dataflow.test;

import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;

@DataFlowComponent
public class InvalidValueProvider_privateGetValueMethod {

    @DataFlowConfigurable
    public InvalidValueProvider_privateGetValueMethod() {
    }

    @OutputValue
    private String getValue(@InputValue String in1) {
        return null;
    }
}
