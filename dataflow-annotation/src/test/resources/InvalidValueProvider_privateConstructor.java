package dataflow.test;

import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;

@DataFlowComponent
public abstract class InvalidValueProvider_privateConstructor {

    @DataFlowConfigurable
    private InvalidValueProvider_privateConstructor() {
    }

    @OutputValue
    public String getValue(@InputValue String in1) {
        return null;
    }
}
