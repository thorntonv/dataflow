package dataflow.test;

import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;

@DataFlowComponent
public class InvalidValueProvider_missingGetValueMethod {

    @DataFlowConfigurable
    public InvalidValueProvider_missingGetValueMethod() {
    }
}
