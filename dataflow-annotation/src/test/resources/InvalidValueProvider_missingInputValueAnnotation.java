package dataflow.test;

import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.OutputValue;

@DataFlowComponent
public class InvalidValueProvider_missingInputValueAnnotation {

    @DataFlowConfigurable
    public InvalidValueProvider_missingInputValueAnnotation() {
    }

    @OutputValue
    public String getValue(String in1) {
        return null;
    }
}
