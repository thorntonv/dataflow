/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfigurableObjectBuilderGeneratorTest.java
 */

package dataflow.annotation.processor;

import org.junit.Test;
import com.google.testing.compile.Compilation;
import com.google.testing.compile.JavaFileObjects;

import static com.google.testing.compile.CompilationSubject.assertThat;
import static com.google.testing.compile.Compiler.javac;
import static org.junit.Assert.*;

public class DataFlowConfigurableObjectBuilderGeneratorTest {

    @Test
    public void process_validValueProvider() {
        Compilation compilation = javac().withProcessors(new DataFlowConfigurableObjectBuilderGenerator())
                .compile(JavaFileObjects.forResource("ValidValueProvider.java"));
        assertThat(compilation).succeeded();
    }

    @Test
    public void process_privateConstructor() {
        verifyFailedCompilation("InvalidValueProvider_privateConstructor.java",
                "DataFlowConfigurable annotated constructor should be public");
    }

    @Test
    public void process_missingConstructorArgAnnotation() {
        verifyFailedCompilation("InvalidValueProvider_missingConstructorArgAnnotation.java",
                "constructor arguments must be annotated with DataFlowConfigProperty");
    }

    @Test
    public void process_duplicateConstructorArgNames() {
        verifyFailedCompilation("InvalidValueProvider_duplicateConstructorArgNames.java",
                "property names must be unique");
    }

    @Test
    public void process_nested_not_static() {
        verifyFailedCompilation("InvalidValueProvider_nested_not_static.java",
                "class should be static when nested in another class");
    }

    private static void verifyFailedCompilation(String resourceName, String errorMessageSubstring) {
        try {
            Compilation compilation = javac().withProcessors(new DataFlowConfigurableObjectBuilderGenerator())
                    .compile(JavaFileObjects.forResource(resourceName));
            assertThat(compilation).failed();
            fail("Expected exception was not thrown: " + compilation.diagnostics());
        } catch (Exception ex) {
            assertTrue(ex.getMessage(), ex.getMessage().contains(errorMessageSubstring));
        }
    }
}