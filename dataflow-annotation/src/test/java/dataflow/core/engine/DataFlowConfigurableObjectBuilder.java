/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfigurableObjectBuilder.java
 */

package dataflow.core.engine;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import dataflow.test.TestEnum;
import dataflow.test.TestProperty;

/**
 * A placeholder so that generated test classes will compile.
 */
public interface DataFlowConfigurableObjectBuilder {

    default Object buildObject(final String key, final Map<String, Object> value,
            final Map<String, Object> instanceValues, final DataFlowEngine engine) {
        switch (key) {
            case "TestProperty":
                return new TestProperty((int) value.get("first"), (String) value.get("second"));
        }
        return null;
    }

    default List buildListObject(final List inputList, String itemClassName, final Map<String, Object> instanceValues, final DataFlowEngine engine) {
        List result = new ArrayList();
        for(Object item : inputList) {
            if(item instanceof Map) {
                Map.Entry entry = (Map.Entry) ((Map) item).entrySet().stream().findFirst().get();
                result.add(buildObject((String) entry.getKey(), (Map<String, Object>)entry.getValue(), instanceValues, engine));
            } else {
                try {
                    Class itemClass = Class.forName(itemClassName);
                    if (item instanceof String && itemClass.isEnum()) {
                        result.add(Enum.valueOf(itemClass, (String) item));
                    } else {
                        throw new IllegalArgumentException("Can't convert from " + item.getClass().getName() + " to " + itemClassName);
                    }
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return result;
    }

    default <T> T injectDependencies(T obj, final DataFlowEngine engine) {
        return obj;
    }
}
