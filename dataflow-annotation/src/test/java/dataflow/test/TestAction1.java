/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestAction1.java
 */

package dataflow.test;

import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;

public class TestAction1 {

    private Integer prop1;
    private Float prop2;

    static String RESULT;

    @DataFlowConfigurable
    public TestAction1(@DataFlowConfigProperty(description = "first\nprop") Integer prop1,
        @DataFlowConfigProperty(required = false) Float prop2) {
        this.prop1 = prop1;
        this.prop2 = prop2;
    }

    public void execute(@InputValue(description = "first input") String in1) {
        RESULT = prop2 != null ? in1 + prop1 + prop2 : in1 + prop1;
    }
}