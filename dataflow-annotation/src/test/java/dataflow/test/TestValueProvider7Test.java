/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestValueProvider7Test.java
 */

package dataflow.test;

import dataflow.annotation.metadata.DataFlowComponentMetadata;
import org.junit.Before;
import org.junit.Test;
import com.google.common.collect.ImmutableMap;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.ValueReference;

import static dataflow.test.TestEnum.VALUE2;
import static org.junit.Assert.*;

public class TestValueProvider7Test {

    private static final DataFlowComponentMetadata METADATA = TestValueProvider7Metadata.INSTANCE;

    private TestValueProvider7Builder builder;

    @Before
    public void setUp() {
        final DataFlowEngine engine = new DataFlowEngine();
        builder = new TestValueProvider7Builder(engine);
    }

    @Test
    public void testMetadata() {
        assertFalse(METADATA.hasDynamicInput());
        assertFalse(METADATA.isAsync());
        assertFalse(METADATA.hasAsyncGet());

        assertEquals(TestEnum.class.getName(), METADATA.getPropertyMetadataMap().get("testEnum").getRawType());
        assertTrue(TestEnum.class.getName(), METADATA.getPropertyMetadataMap().get("testEnum").isEnum());
        assertTrue(METADATA.getInputMetadata().isEmpty());

        assertEquals(TestEnum.class.getName(), METADATA.getOutputMetadata().getRawType());
    }

    @Test
    public void testBuilder_enum() {
        TestValueProvider7 valueProvider = builder.build(ImmutableMap.<String, Object>builder()
                .put("testEnum", "VALUE2")
                .build(), ImmutableMap.<String, Object>builder()
                .build());
        TestEnum testEnum = valueProvider.getValue();
        assertEquals(VALUE2, testEnum);
    }

    @Test
    public void testBuilder_enum_missing() {
        try {
            builder.build(ImmutableMap.<String, Object>builder()
                    .build(), ImmutableMap.<String, Object>builder()
                    .build());
            fail("Expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            // Expected.
        }
    }

    @Test
    public void testBuilder_enum_valueReference() {
        TestValueProvider7 valueProvider = builder.build(ImmutableMap.<String, Object>builder()
                .put("testEnum", new ValueReference("myVal"))
                .build(), ImmutableMap.<String, Object>builder()
                .put("myVal", VALUE2)
                .build());

        TestEnum testEnum = valueProvider.getValue();
        assertEquals(VALUE2, testEnum);
    }
}
