/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestValueProvider1Test.java
 */

package dataflow.test;

import dataflow.annotation.metadata.DataFlowComponentMetadata;
import org.junit.Before;
import org.junit.Test;
import com.google.common.collect.ImmutableMap;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.ValueReference;

import static org.junit.Assert.*;

public class TestValueProvider1Test {

    private static final DataFlowComponentMetadata METADATA = TestValueProvider1Metadata.INSTANCE;

    private TestValueProvider1Builder builder;

    @Before
    public void setUp() {
        final DataFlowEngine engine = new DataFlowEngine();
        builder = new TestValueProvider1Builder(engine);
    }

    @Test
    public void testMetadata() {
        assertFalse(METADATA.hasDynamicInput());
        assertFalse(METADATA.isAsync());
        assertFalse(METADATA.hasAsyncGet());

        assertEquals("prop1", METADATA.getPropertyMetadataMap().get("prop1").getName());
        assertEquals(Integer.class.getName(), METADATA.getPropertyMetadataMap().get("prop1").getRawType());
        assertEquals("first\nprop", METADATA.getPropertyMetadataMap().get("prop1").getDescription());
        assertTrue(METADATA.getPropertyMetadataMap().get("prop1").isRequired());

        assertEquals("prop2", METADATA.getPropertyMetadataMap().get("prop2").getName());
        assertEquals(Float.class.getName(), METADATA.getPropertyMetadataMap().get("prop2").getRawType());
        assertFalse(METADATA.getPropertyMetadataMap().get("prop2").isRequired());

        assertEquals("in1", METADATA.getInputMetadata().get(0).getName());
        assertEquals(String.class.getName(), METADATA.getInputMetadata().get(0).getRawType());
        assertEquals("first input", METADATA.getInputMetadata().get(0).getDescription());

        assertEquals(String.class.getName(), METADATA.getOutputMetadata().getRawType());
    }

    @Test
    public void testBuilder() {
        TestValueProvider1 valueProvider = builder.build(ImmutableMap.<String, Object>builder()
                .put("prop1", 123)
                .build(), ImmutableMap.<String, Object>builder()
                .build());
        assertEquals("test123", valueProvider.getValue("test"));
    }

    @Test
    public void testBuilder_optionalProperty() {
        TestValueProvider1 valueProvider = builder.build(ImmutableMap.<String, Object>builder()
                .put("prop1", 12)
                .put("prop2", 3.14f)
                .build(), ImmutableMap.<String, Object>builder()
                .build());
        assertEquals("test123.14", valueProvider.getValue("test"));
    }

    @Test
    public void testBuilder_missingRequiredProperty() {
        try {
            builder.build(ImmutableMap.<String, Object>builder()
                    .build(), ImmutableMap.<String, Object>builder()
                    .build());
            fail("Expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            // Expected.
        }
    }

    @Test
    public void testBuilder_valueReference() {
        TestValueProvider1 valueProvider = builder.build(ImmutableMap.<String, Object>builder()
                .put("prop1", new ValueReference("val1"))
                .build(), ImmutableMap.<String, Object>builder()
                .put("val1", 54321)
                .build());
        assertEquals("test_54321", valueProvider.getValue("test_"));
    }
}
