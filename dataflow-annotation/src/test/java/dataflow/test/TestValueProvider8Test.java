/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestValueProvider8Test.java
 */

package dataflow.test;

import dataflow.annotation.metadata.DataFlowComponentMetadata;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.ValueReference;

import static org.junit.Assert.*;

public class TestValueProvider8Test {

    private static final DataFlowComponentMetadata METADATA = TestValueProvider8Metadata.INSTANCE;

    private TestValueProvider8Builder builder;

    @Before
    public void setUp() {
        final DataFlowEngine engine = new DataFlowEngine();
        builder = new TestValueProvider8Builder(engine);
    }

    @Test
    public void testMetadata() {
        assertFalse(METADATA.hasDynamicInput());
        assertFalse(METADATA.isAsync());
        assertFalse(METADATA.hasAsyncGet());

        assertEquals("java.util.List<dataflow.test.TestProperty>", METADATA.getPropertyMetadataMap().get("testList").getRawType());
        assertTrue(METADATA.getInputMetadata().isEmpty());

        assertEquals("java.util.List<dataflow.test.TestProperty>", METADATA.getOutputMetadata().getRawType());
    }

    @Test
    public void testBuilder_customObjectList() {
        TestValueProvider8 valueProvider = builder.build(ImmutableMap.<String, Object>builder()
                .put("testList", ImmutableList.of(
                        ImmutableMap.of("TestProperty", ImmutableMap.of("first", 7, "second", "v1")),
                        ImmutableMap.of("TestProperty", ImmutableMap.of("first", 5, "second", "v2"))
                ))
                .build(), Collections.emptyMap());
        List<TestProperty> testList = valueProvider.getValue();
        assertEquals(2, testList.size());
        assertEquals(7, testList.get(0).getValue1());
        assertEquals("v1", testList.get(0).getValue2());
        assertEquals(5, testList.get(1).getValue1());
        assertEquals("v2", testList.get(1).getValue2());
    }

    @Test
    public void testBuilder_customObjectList_missing() {
        try {
            builder.build(ImmutableMap.<String, Object>builder()
                    .build(), ImmutableMap.<String, Object>builder()
                    .build());
            fail("Expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            // Expected.
        }
    }

    @Test
    public void testBuilder_customObjectList_valueReference() {
        List expectedValue = ImmutableList.of(new TestProperty(1, "one"), new TestProperty(2, "two"));
        TestValueProvider8 valueProvider = builder.build(ImmutableMap.<String, Object>builder()
                .put("testList", new ValueReference("myVal"))
                .build(), ImmutableMap.<String, Object>builder()
                .put("myVal", expectedValue)
                .build());

        assertEquals(expectedValue, valueProvider.getValue());
    }
}
