/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestValueProvider10Test.java
 */

package dataflow.test;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import com.google.common.collect.ImmutableMap;
import dataflow.annotation.metadata.DataFlowComponentMetadata;
import dataflow.core.engine.DataFlowEngine;

import static org.junit.Assert.*;

public class TestValueProvider10Test {

    private static final DataFlowComponentMetadata METADATA = TestValueProvider10_nestedMetadata.INSTANCE;

    private TestValueProvider10_nestedBuilder builder;

    @Before
    public void setUp() {
        final DataFlowEngine engine = new DataFlowEngine();
        builder = new TestValueProvider10_nestedBuilder(engine);
    }

    @Test
    public void testMetadata() {
        assertFalse(METADATA.hasDynamicInput());
        assertFalse(METADATA.isAsync());
        assertFalse(METADATA.hasAsyncGet());

        assertEquals(String.class.getName(), METADATA.getPropertyMetadataMap().get("param").getRawType());
        assertTrue(METADATA.getInputMetadata().isEmpty());

        assertEquals(String.class.getName(), METADATA.getOutputMetadata().getRawType());
    }

    @Test
    public void testBuilder_nestedValueProvider() {
        TestValueProvider10.TestValueProvider10_nested valueProvider = builder.build(
                ImmutableMap.<String, Object>builder()
                        .put("param", "myValue")
                        .build(), Collections.emptyMap());
        assertEquals("myValue", valueProvider.getValue());
    }
}
