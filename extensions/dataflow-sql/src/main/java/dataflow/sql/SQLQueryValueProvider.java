/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SQLQueryValueProvider.java
 */

package dataflow.sql;

import static dataflow.core.retry.Retry.executeWithRetry;
import static dataflow.core.util.MetricsUtil.getMetricName;
import static dataflow.core.util.MetricsUtil.tagsToStringArray;
import static dataflow.sql.SQLDatabaseIterationMode.KEY_BASED;
import static dataflow.sql.SQLDatabaseIterationMode.NUMERIC_KEY_RANGE_BASED;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Iterators;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.InputValues;
import dataflow.annotation.OutputValue;
import dataflow.core.data.DataRecord;
import dataflow.core.data.DataRecords;
import dataflow.core.data.InvalidDataException;
import dataflow.core.data.query.DataFilter;
import dataflow.core.data.query.DataQueryValueProvider;
import dataflow.core.data.query.KeyRangeDataFilter;
import dataflow.core.data.query.LastUpdateTimeFilter;
import dataflow.core.data.query.MaxRowCountDataFilter;
import dataflow.core.data.query.MultiKeyDataFilter;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.retry.Retry;
import dataflow.core.util.StringUtil;
import dataflow.core.value.WrapperUtil;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import reactor.core.publisher.Flux;

@DataFlowComponent(description = "Provides a value from a SQL query")
public class SQLQueryValueProvider implements DataQueryValueProvider {

    private static final Logger logger = LoggerFactory.getLogger(SQLQueryValueProvider.class);

    private static final int DEFAULT_FETCH_SIZE = 100;
    private static final Pattern SELECT_PATTERN = Pattern.compile("(?i)(?s)SELECT (.+?) FROM .*");

    private String baseQuery;
    private String countBaseQuery;
    private final String whereClause;
    private final String orderByClause;
    private final List<String> keyColumns;
    private final List<String> recordsKeyColumns;
    private final String lastUpdateTimeColumn;
    private final SQLDatabaseOutputMode outputMode;
    private final List<SQLQueryDecorator> queryDecorators;
    private final Retry retry;
    private final int fetchSize;
    private final Integer maxRowCount;
    private boolean orderByKeyIfNotSpecified = false;
    private boolean requireOrderBy = false;
    private final SQLDatabaseIterationMode iterationMode;
    private final String maxKeyQuery;
    private final Object fromKey;

    private Counter queryCounter;
    private Timer queryTimer;
    private Counter errorCounter;

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private ExecutorService executor = Executors.newCachedThreadPool();

    @DataFlowConfigurable
    public SQLQueryValueProvider(
        @DataFlowConfigProperty(description =
            "The query to use to get values. Input values will be used to replace named parameters.\n" +
            "Example:\\n" +
            "select age from products where id = :product_id\\n\\n" +
            "In this example the input named product_id will be used to replace :product_id\\n") String baseQuery,
        @DataFlowConfigProperty(required = false, description = "The base query to get the estimated number of result records") String countBaseQuery,
        @DataFlowConfigProperty(required = false, description =
            "The where clause used to filter the results" +
            "Input values will be used to replace named parameters.\\n\\n" +
            "Example: age >= :min_age") String whereClause,
        @DataFlowConfigProperty(required = false, description =
            "The names of the columns that form the key of the table") List<String> keyColumns,
        @DataFlowConfigProperty(required = false, description =
            "The names of the columns that form the key of output records. Will default to keyColumns if not specified") List<String> recordsKeyColumns,
        @DataFlowConfigProperty(required = false, description =
            "Order by clause") String orderByClause,
        @DataFlowConfigProperty(required = false, description =
            "The name of the column that holds the last updated time") String lastUpdateTimeColumn,
        @DataFlowConfigProperty(required = false, description =
            "Decorators that modify the query") List<SQLQueryDecorator> decorators,
        @DataFlowConfigProperty(required = false, description =
            "SINGLE_ROW (default)\\n" +
            "The provider will output a single map<string, object> instead of a list. If the query " +
            "returns multiple rows only the first row is used.\\n\\n" +
            "MULTI_ROW\\n" +
            "The provider will output a list rows where each row is a map<string, object>\\n\\n" +
            "STREAM\\n" +
            "The provider will output a stream of rows where each row is a map<string, object>\\n\\n" +
            "SINGLE_COLUMN\\n" +
            "The provider will output a single value. If the query returns multiple rows only the " +
            "first row is used. Only one column can be specified in the columns list.\\n") SQLDatabaseOutputMode outputMode,
        @DataFlowConfigProperty(required = false, description =
            "A string that identifies the cluster to use" +
            "A cluster with the given id should be registered with the cluster manager") String clusterId,
        @DataFlowConfigProperty(required = false, description = "The starting key") Object fromKey,
        @DataFlowConfigProperty(required = false, description =
            "The number of rows returned in a batch") Integer fetchSize,
        @DataFlowConfigProperty(required = false, description =
            "The maximum number of rows to return") Integer maxRowCount,
        @DataFlowConfigProperty(required = false) DataSource dataSource,
        @DataFlowConfigProperty(required = false) SQLDatabaseIterationMode iterationMode,
        @DataFlowConfigProperty(required = false, description = "A query to get the maximum key if using the KEY_BASED iteration mode") String maxKeyQuery,
        @DataFlowConfigProperty(required = false) Retry retry,
        @DataFlowConfigProperty(required = false) String metricNamePrefix,
        @DataFlowConfigProperty(required = false) Map<String, String> metricTags) {
        DataFlowExecutionContext executionContext = DataFlowExecutionContext.getCurrentExecutionContext();
        DataFlowInstance instance = executionContext != null ? executionContext.getInstance() : null;
        this.keyColumns =
            keyColumns != null ? new ArrayList<>(keyColumns) : Collections.emptyList();
        this.recordsKeyColumns = recordsKeyColumns != null ? recordsKeyColumns : this.keyColumns;
        this.outputMode = outputMode != null ? outputMode : SQLDatabaseOutputMode.MULTI_ROW;
        this.whereClause = whereClause;
        this.orderByClause = orderByClause;
        this.lastUpdateTimeColumn = lastUpdateTimeColumn;
        this.iterationMode = iterationMode;
        if (iterationMode == KEY_BASED && Strings.isNullOrEmpty(orderByClause)) {
            throw new DataFlowConfigurationException(
                "orderByClause is required for " + KEY_BASED + " iteration");
        }
        if (iterationMode == NUMERIC_KEY_RANGE_BASED) {
            if (fromKey == null) {
                throw new DataFlowConfigurationException(
                    "fromKey is required for " + NUMERIC_KEY_RANGE_BASED + " iteration");
            }
            if (fromKey instanceof Integer) {
                fromKey = ((Integer) fromKey).longValue();
            }
            if (!(fromKey instanceof Long)) {
                throw new DataFlowConfigurationException(
                    "fromKey must be an integer or long value for " + NUMERIC_KEY_RANGE_BASED + " iteration");
            }
            if (maxKeyQuery == null) {
                throw new DataFlowConfigurationException(
                    "maxKeyQuery is required for " + NUMERIC_KEY_RANGE_BASED + " iteration");
            }
        }
        this.fromKey = fromKey;
        this.maxKeyQuery = maxKeyQuery;
        this.queryDecorators = decorators != null ?
            new ArrayList<>(decorators) : new ArrayList<>();

        if(retry == null && instance != null) {
            retry = instance.getDefaultRetryBehavior();
        }
        this.retry = Retry.clone(retry);
        if(dataSource == null) {
            SQLDatabaseManager dbManager = executionContext.getDependencyInjector()
                .getInstance(SQLDatabaseManager.class);
            if (dbManager == null) {
                throw new DataFlowConfigurationException("Unable to inject cluster manager. " +
                    "Please verify that the dependency injector is configured properly.");
            }
            executor = dbManager.getExecutor();
            dataSource = (clusterId != null) ?
                dbManager.getDataSource(clusterId) : dbManager.getDefaultDataSource();
        }

        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

        if(fetchSize == null) {
            fetchSize = DEFAULT_FETCH_SIZE;
        }
        this.fetchSize = fetchSize;
        jdbcTemplate.getJdbcTemplate().setFetchSize(fetchSize);

        this.maxRowCount = maxRowCount;
        if (maxRowCount != null) {
            jdbcTemplate.getJdbcTemplate().setMaxRows(maxRowCount);
        }
        this.baseQuery = baseQuery;
        this.countBaseQuery = countBaseQuery;

        MeterRegistry meterRegistry = executionContext != null ?
            executionContext.getDependencyInjector().getInstance(MeterRegistry.class) : null;
        if(meterRegistry != null) {
            Map<String, String> tags = new HashMap<>();
            if (instance != null) {
                tags.putAll(instance.getMetricTagsForComponent(this));
            }
            if (clusterId != null) {
                tags.put("clusterId", clusterId);
            }
            if (metricTags != null) {
                tags.putAll(metricTags);
            }
            metricNamePrefix = metricNamePrefix != null ? metricNamePrefix : getClass().getSimpleName();
            queryCounter = meterRegistry.counter(
                getMetricName(metricNamePrefix, "queryCounter", instance),
                tagsToStringArray(tags));
            queryTimer = meterRegistry.timer(
                getMetricName(metricNamePrefix, "queryTimer", instance),
                tagsToStringArray(tags));
            errorCounter = meterRegistry.counter(
                getMetricName(metricNamePrefix, "errorCounter", instance),
                tagsToStringArray(tags));
            Counter retryCounter = meterRegistry.counter(
                getMetricName(metricNamePrefix, "retryCounter", instance),
                tagsToStringArray(tags));
            if(this.retry != null) {
                this.retry.getEventPublisher().onRetry(event -> retryCounter.increment());
            }
        }
    }

    @OutputValue(description =
        "Outputs the stream, list of rows, single row, or single column value depending on the " +
        "outputMode. See the outputMode property documentation for more details")
    public CompletableFuture<Object> getValue(@InputValues(description =
        "The param values. The name of each param should match a placeholder in the " +
        "baseQuery or whereClause.") Map<String, ?> paramValues) {
        Map<String, ?> unwrappedInputValues = WrapperUtil.unwrapMapValues(paramValues);
        SQLQueryInfo queryInfo = buildQueryInfo(unwrappedInputValues, Collections.emptyList());
        String query = buildQuery(queryInfo);
        MapSqlParameterSource paramSource = buildParamSource(queryInfo.getParamValues());
        CompletableFuture<Object> resultFuture = new CompletableFuture<>();
        long startTime = System.currentTimeMillis();
        executor.submit(() -> executeQuery(query, paramSource, resultFuture));
        return resultFuture.thenApply(o -> {
            if (queryTimer != null) {
                queryTimer.record(System.currentTimeMillis() - startTime, TimeUnit.MILLISECONDS);
            }
            return o;
        });
    }

    @Override
    public DataRecords query(Map<String, Object> paramValues, List<DataFilter> filters)
        throws InvalidDataException, IOException {
        long startTime = System.currentTimeMillis();
        List<SQLQueryDecorator> filterDecorators = getQueryDecoratorsForFilters(filters);
        Map<String, ?> unwrappedInputValues = WrapperUtil.unwrapMapValues(paramValues);
        SQLQueryInfo queryInfo = buildQueryInfo(unwrappedInputValues, filterDecorators);
        String query = buildQuery(queryInfo);
        MapSqlParameterSource paramSource = buildParamSource(queryInfo.getParamValues());

        if (queryCounter != null) {
            queryCounter.increment();
        }

        List<Map<String, Object>> results;
        try {
            results = executeWithRetry(retry, () ->
                jdbcTemplate.queryForList(query, paramSource));

            if (queryTimer != null) {
                queryTimer.record(System.currentTimeMillis() - startTime, TimeUnit.MILLISECONDS);
            }
        } catch (Exception e) {
            if (errorCounter != null) {
                errorCounter.increment();
            }
            throw new IOException("Error executing query: " + query, e);
        }
        DataRecords records = createDataRecords(results);
        if(!records.validateKeyColumns()) {
            if (errorCounter != null) {
                errorCounter.increment();
            }
            throw new InvalidDataException(String.format(
                "Fetched records are missing key column(s) %s: %s", keyColumns, records));
        }
        return records;
    }

    @Override
    public Stream<DataRecords> queryForStream(Map<String, Object> paramValues,
            List<DataFilter> filters, int batchSize) throws IOException {
        if(iterationMode == KEY_BASED) {
            return StreamSupport.stream(Spliterators.spliteratorUnknownSize(new KeyBasedResultIterator(
                paramValues, filters, fromKey, batchSize, this), Spliterator.ORDERED), false);
        } else if(iterationMode == NUMERIC_KEY_RANGE_BASED) {
            return StreamSupport.stream(Spliterators.spliteratorUnknownSize(new NumericKeyRangeBasedResultIterator(
                paramValues, filters, (Long) fromKey, fetchSize, maxKeyQuery, jdbcTemplate, this), Spliterator.ORDERED), false);
        }
        List<SQLQueryDecorator> filterDecorators = getQueryDecoratorsForFilters(filters);
        Map<String, ?> unwrappedInputValues = WrapperUtil.unwrapMapValues(paramValues);
        SQLQueryInfo queryInfo = buildQueryInfo(unwrappedInputValues, filterDecorators);
        String query = buildQuery(queryInfo);
        MapSqlParameterSource paramSource = buildParamSource(queryInfo.getParamValues());

        try {
            Stream<Map<String, Object>> resultStream = jdbcTemplate.queryForStream(
                query, paramSource, new ColumnMapRowMapper());
            return StreamSupport.stream(Spliterators.spliteratorUnknownSize(Iterators.partition(
                resultStream.iterator(), batchSize), Spliterator.ORDERED), false)
                .map(this::createDataRecords);
        } catch (Exception e) {
            if (errorCounter != null) {
                errorCounter.increment();
            }
            throw new IOException("Error executing query: " + query, e);
        }
    }

    @Override
    public Optional<Long> getEstimatedResultCount(Map<String, Object> paramValues,
        List<DataFilter> filters) {
        if (countBaseQuery != null && countBaseQuery.isEmpty()) {
            // Count support has been explicitly disabled.
            return Optional.empty();
        }
        try {
            List<SQLQueryDecorator> filterDecorators = getQueryDecoratorsForFilters(filters);
            Map<String, ?> unwrappedInputValues = WrapperUtil.unwrapMapValues(paramValues);
            SQLQueryInfo queryInfo = buildQueryInfo(unwrappedInputValues, filterDecorators);
            if (countBaseQuery != null) {
                queryInfo.setBaseQuery(countBaseQuery);
            } else {
                // Try to build a query to select count from the base query. This may not work in all
                // cases.
                Matcher matcher = SELECT_PATTERN.matcher(baseQuery);
                if (matcher.matches()) {
                    queryInfo.setBaseQuery(String.format("SELECT COUNT(*) %s",
                        baseQuery.substring(matcher.end(1))));
                } else {
                    return Optional.empty();
                }
            }
            // Ordering is not needed for a count query.
            queryInfo.setOrderByClause(null);
            boolean ignoreRequireOverride = true;
            String query = buildQuery(queryInfo, ignoreRequireOverride);
            MapSqlParameterSource paramSource = buildParamSource(queryInfo.getParamValues());
            Long count = jdbcTemplate.queryForObject(query, paramSource, Long.class);
            if (count != null && count >= 0) {
                if (countBaseQuery == null) {
                    // We were able successfully get a count we can use this baseQuery in the future.
                    countBaseQuery = queryInfo.getBaseQuery();
                }
                return Optional.of(count);
            }
        } catch (Exception ex) {
            logger.warn("Unable to get estimated result count", ex);
        }
        return Optional.empty();
    }

    @Override
    public void setOrderByKeyIfNotSpecified(boolean orderByKeyIfNotSpecified) {
        this.orderByKeyIfNotSpecified = orderByKeyIfNotSpecified;
    }

    @Override
    public void setRequireOrderBy(boolean requireOrderBy) {
        this.requireOrderBy = requireOrderBy;
    }

    public void setBaseQuery(String baseQuery) {
        this.baseQuery = baseQuery;
    }

    @Override
    public boolean isFilterSupported(DataFilter filter) {
        if (filter.getClass() == MultiKeyDataFilter.class ||
            filter.getClass() == KeyRangeDataFilter.class ||
            filter.getClass() == MaxRowCountDataFilter.class) {
            return true;
        } else if (filter.getClass() == LastUpdateTimeFilter.class &&
            lastUpdateTimeColumn != null) {
            return true;
        }
        return false;
    }

    private List<SQLQueryDecorator> getQueryDecoratorsForFilters(List<DataFilter> filters) {
        jdbcTemplate.getJdbcTemplate().setMaxRows(maxRowCount != null ? maxRowCount : -1);
        jdbcTemplate.getJdbcTemplate().setFetchSize(fetchSize);
        List<SQLQueryDecorator> filterDecorators = new ArrayList<>();
        for (DataFilter filter : filters) {
            if (filter.getClass() == MultiKeyDataFilter.class) {
                filterDecorators.add(new KeysFilterQueryDecorator(
                    ((MultiKeyDataFilter) filter).getKeys()));
            } else if (filter.getClass() == KeyRangeDataFilter.class) {
                KeyRangeDataFilter keyRangeFilter = (KeyRangeDataFilter) filter;
                filterDecorators.add(new KeyRangeFilterQueryDecorator(
                    keyRangeFilter.getFromKey(), keyRangeFilter.isFromKeyInclusive(),
                    keyRangeFilter.getToKey(), keyRangeFilter.isToKeyInclusive()));
            } else if (filter.getClass() == LastUpdateTimeFilter.class) {
                if (!isFilterSupported(filter)) {
                    throw new DataFlowConfigurationException(
                        "lastUpdateTimeColumn must be set in order to use a LastUpdateTimeFilter");
                }
                filterDecorators.add(new LastUpdateTimeFilterQueryDecorator(
                    ((LastUpdateTimeFilter) filter).getLastUpdateTime()));
            } else if (filter.getClass() == MaxRowCountDataFilter.class) {
                int rowCount = ((MaxRowCountDataFilter) filter).getMaxRowCount();
                jdbcTemplate.getJdbcTemplate().setMaxRows(rowCount);
                jdbcTemplate.getJdbcTemplate().setFetchSize(rowCount);
            } else {
                throw new IllegalArgumentException(
                    "Unsupported filter type " + filter.getClass().getName());
            }
        }
        return filterDecorators;
    }

    private void executeQuery(String query, MapSqlParameterSource paramSource,
        CompletableFuture<Object> resultFuture) {
        if (queryCounter != null) {
            queryCounter.increment();
        }

        try {
            if(outputMode == SQLDatabaseOutputMode.STREAM) {
                final ColumnMapRowMapper rowMapper = new ColumnMapRowMapper();
                ResultSetExtractor<Object> resultSetExtractor = rs -> {
                    Flux<Map<String, Object>> flux = Flux.generate(sink -> {
                        try {
                            if(rs.next()) {
                                DataRecord record = new DataRecord();
                                record.putAll(rowMapper.mapRow(rs, 0));
                                sink.next(record);
                            } else {
                                sink.complete();
                            }
                        } catch (SQLException ex) {
                            sink.error(ex);
                        }
                    });
                    resultFuture.complete(flux);

                    // Wait for the flux to terminate.
                    Semaphore semaphore = new Semaphore(1);
                    semaphore.acquireUninterruptibly();
                    flux.doFinally(signalType -> semaphore.release());
                    semaphore.acquireUninterruptibly();
                    return null;
                };

                executeWithRetry(retry, () -> jdbcTemplate.query(query, resultSetExtractor));
                return;
            }
            DataRecords result = createDataRecords(executeWithRetry(retry, () ->
                jdbcTemplate.queryForList(query, paramSource)));
            switch (outputMode) {
                case SINGLE_COLUMN:
                    if (result.size() > 0 && !result.get(0).isEmpty()) {
                        resultFuture.complete(result.get(0).values().iterator().next());
                    } else {
                        resultFuture.complete(null);
                    }
                    break;
                case SINGLE_ROW:
                    resultFuture.complete(result.size() > 0 ? result.get(0) : null);
                    break;
                case MULTI_ROW:
                    if(!result.validateKeyColumns()) {
                        throw new InvalidDataException(String.format(
                            "Fetched records are missing key column(s) %s: %s", keyColumns, result));
                    }
                    resultFuture.complete(result);
                    break;
            }
        } catch (Exception ex) {
            if (errorCounter != null) {
                errorCounter.increment();
            }
            resultFuture.completeExceptionally(ex);
        }
    }

    private DataRecords createDataRecords(List<Map<String, Object>> results) {
        DataRecords records = new DataRecords();
        records.setKeyColumnNames(recordsKeyColumns);
        records.addAll(results.stream().map(DataRecord::new).collect(Collectors.toList()));
        return records;
    }

    private MapSqlParameterSource buildParamSource(Map<String, ?> inputParams) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        for (Map.Entry<String, ?> entry : inputParams.entrySet()) {
            Object value = entry.getValue();
            paramSource.addValue(entry.getKey(), value);
        }
        return paramSource;
    }

    private String buildQuery(SQLQueryInfo queryInfo) {
        return buildQuery(queryInfo, false);
    }

    private String buildQuery(SQLQueryInfo queryInfo, boolean ignoreRequireOrderBy) {
        StringBuilder result = new StringBuilder();
        result.append(queryInfo.getBaseQuery());
        if (!queryInfo.getWhereClauses().isEmpty()) {
            result.append(" WHERE ");
            result.append(Joiner.on(" AND ").join(queryInfo.getWhereClauses()));
        }
        if (queryInfo.getOrderByClause() != null && !queryInfo.getOrderByClause().isEmpty()) {
            result.append(" ORDER BY ").append(queryInfo.getOrderByClause());
        } else if (orderByKeyIfNotSpecified) {
            // Order by key by default.
            result.append(" ORDER BY ").append(StringUtil.join(keyColumns, ", "));
        } else if(requireOrderBy && !ignoreRequireOrderBy) {
            throw new DataFlowConfigurationException("An order by clause is required");
        }
        return result.toString();
    }

    private SQLQueryInfo buildQueryInfo(Map<String, ?> paramValues, List<SQLQueryDecorator> additionalDecorators) {
        List<String> whereClauses = new ArrayList<>();
        if (!Strings.isNullOrEmpty(whereClause)) {
            whereClauses.add(whereClause);
        }

        SQLQueryInfo queryInfo = new SQLQueryInfo();
        queryInfo.setBaseQuery(baseQuery);
        queryInfo.setKeyColumns(keyColumns);
        queryInfo.setLastUpdateTimeColumn(lastUpdateTimeColumn);
        queryInfo.setWhereClauses(whereClauses);
        queryInfo.setOrderByClause(orderByClause);
        queryInfo.setParamValues(paramValues != null ?
            new HashMap<>(paramValues) : new HashMap<>());

        if (fromKey != null) {
            queryInfo.getParamValues().put("_fromKey", fromKey);
        }
        queryInfo.getParamValues().put("_batchSize", fetchSize);
        for (SQLQueryDecorator decorator : queryDecorators) {
            decorator.apply(queryInfo);
        }
        for (SQLQueryDecorator decorator : additionalDecorators) {
            decorator.apply(queryInfo);
        }
        return queryInfo;
    }

    static Object getKeyColumnValue(Object key, int columnIdx) {
        if (key instanceof List) {
            List keyColumnValues = (List) key;
            if (keyColumnValues.size() <= columnIdx) {
                throw new IllegalArgumentException(String.format(
                    "No value for column index %s in key %s", columnIdx, key.toString()));
            }
            return keyColumnValues.get(columnIdx);
        } else if (columnIdx == 0) {
            return key;
        } else {
            throw new IllegalArgumentException(
                String.format("No value for column index %d in key %s", columnIdx, key.toString()));
        }
    }
}
