/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  IndexManager.java
 */

package dataflow.elasticsearch;

import static java.lang.Thread.sleep;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequest;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequest.AliasActions;
import org.elasticsearch.action.admin.indices.alias.get.GetAliasesRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.forcemerge.ForceMergeRequest;
import org.elasticsearch.action.admin.indices.forcemerge.ForceMergeResponse;
import org.elasticsearch.action.admin.indices.settings.put.UpdateSettingsRequest;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.GetAliasesResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.cluster.health.ClusterHealthStatus;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IndexManager implements AutoCloseable {

    static final String DOC_TYPE = "_doc";
    private static final Logger logger = LoggerFactory.getLogger(IndexManager.class);

    private final String clusterId;
    private final ElasticsearchClient elasticsearchClient;
    private final String metadataIndexName = "indexmetadata";
    private final ObjectMapper jsonMapper = new ObjectMapper();
    private final Map<String, String> typeActiveAliasNameMap;
    private final ObjectMapper objectMapper;
    private final int defaultNumPriorSchemaVersionsToKeep;
    private final int defaultNumActiveBackupsForCurrentVersion;
    private final int defaultMinIndexAgeForCleanupHours;
    private final ExecutorService executor = Executors.newCachedThreadPool();

    public IndexManager(String clusterId, Map<String, String> typeActiveAliasNameMap,
        ObjectMapper objectMapper, ElasticsearchClient elasticsearchClient,
        int defaultNumPriorSchemaVersionsToKeep, int defaultNumActiveBackupsForCurrentVersion,
        int defaultMinIndexAgeForCleanupHours) {
        this.clusterId = clusterId;
        this.typeActiveAliasNameMap = typeActiveAliasNameMap != null ? typeActiveAliasNameMap : new HashMap<>();
        this.objectMapper = objectMapper;
        this.elasticsearchClient = elasticsearchClient;
        this.defaultNumPriorSchemaVersionsToKeep = defaultNumPriorSchemaVersionsToKeep;
        this.defaultNumActiveBackupsForCurrentVersion = defaultNumActiveBackupsForCurrentVersion;
        this.defaultMinIndexAgeForCleanupHours = defaultMinIndexAgeForCleanupHours;

        jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public String getClusterId() {
        return clusterId;
    }

    public IndexMetadata createIndex(String type, String name, String version,
        String schemaDefinitionJson, int schemaVersion) throws IOException, IndexerException {
        // Elasticsearch requires lowercase index names.
        name = name.toLowerCase();
        IndexMetadata metadata = new IndexMetadata();
        metadata.setIndexCreateDateTime(new Date());
        metadata.setType(type);
        metadata.setName(name);
        metadata.setVersion(version);
        metadata.setSchemaVersion(schemaVersion);
        metadata.setIndexingStatus(IndexingStatusEnum.IN_PROGRESS);
        metadata.setSwitchPercent(0);

        // does index exist?
        GetIndexRequest existsReq = new GetIndexRequest(name);

        if (!elasticsearchClient.indices_exists(existsReq)) {
            logger.info("{} index {}:{} does not exist, will create it with appropriate mappings",
                type, name, version);

            removeIndexMetadata(metadata);

            CreateIndexRequest createRequest = new CreateIndexRequest(name);
            createRequest.source(schemaDefinitionJson, XContentType.JSON);
            CreateIndexResponse response = elasticsearchClient.indices_create(createRequest);

            if (response == null || !response.isAcknowledged() || !response.isShardsAcknowledged()) {
                throw new IndexerException(String.format("%s index %s:%s create index failed, " +
                    "acknowledged: %b, shardsAcknowledged: %b", type, name, version,
                    response != null && response.isAcknowledged(),
                    response != null && response.isShardsAcknowledged()));
            }

            // create a header status doc
            updateMetadataAsync(metadata);

            logger.info("{} index {}:{} successfully created", type, name, version);
        } else {
            throw new IndexerException(String.format("%s index %s:%s exists, cannot continue, " +
                "this should not happen, each run should have its own unique name",
                type, name, version));
        }
        return metadata;
    }

    /*
     * Returns a list of metadata for all indexes.
     */
    public List<IndexMetadata> getIndexMetadata() throws IOException {
        SearchSourceBuilder getPhysicalIndexesQuery = new SearchSourceBuilder()
            .size(10000)
            .fetchSource(new String[]{"*"}, null)
            .query(QueryBuilders.matchAllQuery());

        SearchRequest elasticRequest = new SearchRequest();
        elasticRequest.indices(metadataIndexName);
        elasticRequest.source(getPhysicalIndexesQuery);

        SearchResponse searchResponse = elasticsearchClient.search(elasticRequest);

        List<IndexMetadata> metadataList = new ArrayList<>();
        if (searchResponse == null || searchResponse.getHits() == null) {
            return metadataList;
        }
        for (SearchHit hit : searchResponse.getHits().getHits()) {
            try {
                metadataList.add(jsonMapper.readValue(hit.getSourceAsString(), IndexMetadata.class));
            } catch(Throwable t) {
                logger.warn("Error deserializing index metadata: " + hit.getSourceAsString());
            }
        }

        metadataList.sort(Comparator.comparing(IndexMetadata::getIndexCreateDateTime).reversed());
        return metadataList;
    }

    public Optional<IndexMetadata> getIndexMetadata(String type, String name, String version)
        throws IOException {
        return getIndexMetadata().stream()
            .filter(metadata -> metadata.getType().equals(type))
            // Elasticsearch requires lowercase index names.
            .filter(metadata -> metadata.getName().equals(name.toLowerCase()))
            .filter(metadata -> metadata.getVersion().equals(version))
            .findAny();
    }

    /**
     * Returns the metadata for the active index of the given type.
     */
    public Optional<IndexMetadata> getActiveIndexMetadata(String type)
            throws IndexerException, IOException {
        List<Integer> schemaVersions = getIndexMetadata().stream()
            .filter(metadata -> metadata.getType().equals(type))
            .map(IndexMetadata::getSchemaVersion)
            .distinct()
            .sorted(Comparator.reverseOrder())
            .collect(Collectors.toList());

        for (int schemaVersion : schemaVersions) {
            Optional<IndexMetadata> metadata = getIndexMetadataForAlias(
                getAliasName(type, schemaVersion));
            if (metadata.isPresent()) {
                return metadata;
            }
        }
        return Optional.empty();
    }

    /*
     * If metadata for the index exists, delete it from the index manager index.
     */
    private void removeIndexMetadata(IndexMetadata metadata) throws IndexerException {
        DeleteRequest req = new DeleteRequest(metadataIndexName, DOC_TYPE, metadata.getName());
        try {
            elasticsearchClient.delete(req);
        } catch (ElasticsearchStatusException es) {
            if (es.status() != RestStatus.NOT_FOUND) {
                throw new IndexerException(String.format("Error removing metadata for %s index %s:%s",
                    metadata.getType(), metadata.getName(), metadata.getVersion()), es);
            }
        } catch (Throwable t) {
            throw new IndexerException(String.format("Error removing metadata for %s index %s:%s",
                metadata.getType(), metadata.getName(), metadata.getVersion()), t);
        }
    }

    /*
     * Asynchronously & synchronous - writes this status block to the index manager index.
     * It streams it to json immediately, so it captures the state of the metadata
     * at the time of this call, even if it commits later.
     *
     * It's acknowledged that because this is async, index manager metadata can arrive
     * out of order. That's ok, the final call setting the index to FAILED or COMPLETE will
     * set the final counts, etc.
     */
    public void updateMetadataAsync(IndexMetadata metadata) throws IOException {
        internalUpdateMetadata(metadata, true);
    }

    public void updateMetadataSync(IndexMetadata metadata) throws IOException {
        internalUpdateMetadata(metadata, false);
    }

    protected void internalUpdateMetadata(IndexMetadata metadata, boolean async) throws IOException {
        String json = jsonMapper.writeValueAsString(metadata);
        IndexRequest req = new IndexRequest(metadataIndexName, DOC_TYPE, metadata.getName() /* <-- the id of the doc is the name of the index */)
            .source(json, XContentType.JSON)
            .timeout("5m"); // wait a long time since this is async

        ActionListener<IndexResponse> listener = new ActionListener<IndexResponse>() {
            @Override
            public void onResponse(IndexResponse indexResponse) {
            }

            @Override
            public void onFailure(Exception e) {
                logger.warn("Error asynchronously updating metadata for {} index {}:{}",
                    metadata.getType(), metadata.getName(), metadata.getVersion(), e);
            }
        };

        if (async) {
            elasticsearchClient.indexAsync(req, listener);
        } else {
            IndexResponse resp = elasticsearchClient.indexSync(req);
            if (resp.getResult() != DocWriteResponse.Result.CREATED &&
                resp.getResult() != DocWriteResponse.Result.UPDATED) {
                logger.warn("Error updating metadata for {} index {}:{}",
                    metadata.getType(), metadata.getName(), metadata.getVersion());
            }
        }
    }

    public Optional<IndexMetadata> getIndexMetadataForAlias(String alias) throws IndexerException {
        GetAliasesRequest req = new GetAliasesRequest(alias);

        try {
            GetAliasesResponse response = elasticsearchClient.indices_getAlias(req);

            // alias doesn't exist at all yet
            if (response.status() == RestStatus.NOT_FOUND) {
                return Optional.empty();
            }

            for (String indexName : response.getAliases().keySet()) {
                if (response.getAliases().get(indexName).stream()
                    .anyMatch(aliasMetadata -> aliasMetadata.getAlias().equals(alias))) {
                    Optional<IndexMetadata> metadata = getIndexMetadata().stream()
                        .filter(md -> md.getName().equals(indexName)).findAny();
                    if (metadata.isPresent()) {
                        return metadata;
                    }
                }
            }
            return Optional.empty();
        } catch (Throwable t) {
            throw new IndexerException("Error trying to retrieve index aliases", t);
        }
    }

    /*
     * If there is an index with the given alias then removes the alias from that index.
     */
    public boolean removeAlias(String alias) throws IndexerException, IOException {
        Optional<IndexMetadata> metadata = getIndexMetadataForAlias(alias);
        IndicesAliasesRequest request = new IndicesAliasesRequest();

        if (metadata.isPresent()) {
            request.addAliasAction(new IndicesAliasesRequest.AliasActions(AliasActions.Type.REMOVE)
                .index(metadata.get().getName())
                .alias(alias));
            AcknowledgedResponse indicesAliasesResponse =
                elasticsearchClient.indices_updateAliases(request);
            return indicesAliasesResponse.isAcknowledged();
        }
        return true;
    }

    /*
     * Makes the physical index passed the active index, atomically, for the logical index type
     */
    public boolean makeActiveIndex(IndexMetadata metadata) throws IOException, IndexerException {
        IndicesAliasesRequest request = new IndicesAliasesRequest();

        String aliasName = getAliasName(metadata.getType(), metadata.getSchemaVersion());

        Optional<IndexMetadata> existingActiveIndexMetadata = getActiveIndexMetadata(metadata.getType());

        if (existingActiveIndexMetadata.isPresent()) {
            request.addAliasAction(new IndicesAliasesRequest.AliasActions(AliasActions.Type.REMOVE)
                .index(existingActiveIndexMetadata.get().getName())
                .alias(aliasName));
        }
        request.addAliasAction(new IndicesAliasesRequest.AliasActions(AliasActions.Type.ADD)
            .index(metadata.getName())
            .alias(aliasName));

        AcknowledgedResponse indicesAliasesResponse =
            elasticsearchClient.indices_updateAliases(request);

        return indicesAliasesResponse.isAcknowledged();
    }

    /*
     * Force drops an index and removes it from the index manager list.
     */
    public void removeIndex(IndexMetadata metadata) throws IOException, IndexerException {
        Optional<IndexMetadata> activeIndexMetadata = getActiveIndexMetadata(metadata.getType());
        if (activeIndexMetadata.isPresent() &&
                metadata.getName().equals(activeIndexMetadata.get().getName()) &&
                metadata.getVersion().equals(activeIndexMetadata.get().getVersion())) {
            String msg = String.format("Attempted to remove the active %s index", metadata.getType());
            logger.warn(msg);
            throw new IllegalArgumentException(msg);
        }

        // it's ok to drop
        try {
            deleteIndexManagerDocument(metadata.getName());
        } catch(Throwable t) {
            // swallow
        }

        dropIndex(metadata);
    }

    /**
     * Sets the number of replicas for the specified index.
     */
    public void setNumberOfReplicas(IndexMetadata metadata, int count) throws IOException {
        UpdateSettingsRequest request = new UpdateSettingsRequest(metadata.getName())
            .settings(Settings.builder()
                .put("index.number_of_replicas", count)
                .build());
        elasticsearchClient.indices_updateSettings(request, RequestOptions.DEFAULT);
    }

    public void setTransLogAsync(IndexMetadata metadata, boolean async) throws IOException {
        UpdateSettingsRequest request = new UpdateSettingsRequest(metadata.getName())
            .settings(Settings.builder()
                .put("index.translog.durability", async ? "async" : "request")
                .build());
        elasticsearchClient.indices_updateSettings(request, RequestOptions.DEFAULT);
    }

    public void switchIndex(IndexMetadata metadata, int switchDuration, TimeUnit switchDurationTimeUnit,
        Supplier<Boolean> canceledFn) throws IOException, IndexerException {
        long switchStartTime = System.currentTimeMillis();
        if (switchDuration> 0) {
            logger.info("Starting gradual switch to {} index {}:{} over {} minutes",
                metadata.getType(), metadata.getName(), metadata.getVersion(),
                switchDurationTimeUnit.toMinutes(switchDuration));
            metadata.setIndexingStatus(IndexingStatusEnum.SWITCH_IN_PROGRESS);
            long switchDurationMillis = switchDurationTimeUnit.toMillis(switchDuration);
            long elapsed = 0;
            while (elapsed < switchDurationMillis) {
                elapsed = System.currentTimeMillis() - switchStartTime;
                if(canceledFn.get()) {
                    metadata.setSwitchPercent(0);
                    updateMetadataSync(metadata);
                    throw new IndexerException("Indexing was canceled");
                }
                metadata.setSwitchPercent(Math.max(1, (int) (100 * (float)elapsed / switchDurationMillis)));
                updateMetadataSync(metadata);
                logger.info("{} index {} switch at {} percent", metadata.getType(),
                    metadata.getName(), metadata.getSwitchPercent());
                try {
                    sleep(Math.min(switchDurationMillis / 10, 60 * 1000));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            metadata.setSwitchPercent(100);
            updateMetadataSync(metadata);
        }
        if (canceledFn.get()) {
            metadata.setSwitchPercent(0);
            updateMetadataSync(metadata);
            throw new IndexerException("Indexing was canceled");
        }
    }

    public void forceMergeSegments(IndexMetadata metadata, int maxNumSegments) {
        // force merge segments for performance
        if (metadata.getIndexingStatus() == IndexingStatusEnum.COMPLETE) {
            logger.info("Forcing merge on segments on {} index {}:{}, " +
                "please be patient, will take several minutes",
                metadata.getType(), metadata.getName(), metadata.getVersion());
            ForceMergeRequest fmr = new ForceMergeRequest(metadata.getName());
            fmr.maxNumSegments(maxNumSegments);

            Object forceMergeWaiter = new Object();

            ActionListener<ForceMergeResponse> fmListener = new ActionListener<ForceMergeResponse>() {
                @Override
                public void onResponse(ForceMergeResponse forceMergeResponse) {
                    logger.info("Force Merge successful on {} index {}:{}",
                        metadata.getType(), metadata.getName(), metadata.getVersion());
                    synchronized(forceMergeWaiter) {
                        forceMergeWaiter.notifyAll();
                    }
                }

                @Override
                public void onFailure(Exception e) {
                    if (e instanceof org.apache.http.ConnectionClosedException) {
                        logger.warn("Connection was closed while merging {} index {}:{}, " +
                            "merge will likely be successful and finish up in background",
                            metadata.getType(), metadata.getName(), metadata.getVersion(), e);
                    } else {
                        logger.warn("Merge of {} index {}:{} failed with exception",
                            metadata.getType(), metadata.getName(), metadata.getVersion());
                    }
                    synchronized(forceMergeWaiter) {
                        forceMergeWaiter.notifyAll();
                    }
                }
            };

            elasticsearchClient.indices_forcemergeAsync(fmr, fmListener);

            logger.info("Waiting on force merge, please wait");
            synchronized(forceMergeWaiter) {
                try {
                    forceMergeWaiter.wait();
                } catch (InterruptedException e) {
                }
            }
            logger.info("Force merge finished");
        }
    }

    public CompletableFuture<Map<String, Object>> writeDocuments(Map<String, Object> docIdDocMap,
        IndexMetadata metadata, boolean waitForIndexingToFinish, int timeOutMillis, int retryCount,
        int retryIntervalMillis, Consumer<Integer> retryCountConsumer) {
        CompletableFuture<Map<String, Object>> futureResult = new CompletableFuture<>();
        executor.submit(() -> {
            Map<String, String> docIdJsonMap = docIdDocMap.entrySet().stream().collect(
                Collectors.toMap(Entry::getKey, entry -> {
                    try {
                        return objectMapper.writeValueAsString(entry.getValue());
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException("Unable to serialize document", e);
                    }
                }));
            List<DocWriteRequest> elasticRequests = new ArrayList<>();

            docIdJsonMap.forEach((docId, json) -> elasticRequests.add(new IndexRequest(
                metadata.getName(), DOC_TYPE, docId).source(json, XContentType.JSON)));

            BulkRequest bulkRequest = new BulkRequest();
            if (waitForIndexingToFinish) {
                bulkRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.WAIT_UNTIL);
            }

            elasticRequests.forEach(bulkRequest::add);
            bulkRequest.timeout(TimeValue.timeValueMillis(timeOutMillis));

            executeAsyncBulkRequest(bulkRequest, futureResult, docIdDocMap, "write", retryCount,
                retryIntervalMillis, retryCountConsumer);
        });
        return futureResult;
    }

    public CompletableFuture<Set<String>> deleteDocuments(Set<String> docIds, IndexMetadata metadata,
        boolean waitForIndexingToFinish, int timeOutMillis, int retryCount, int retryIntervalMillis,
        Consumer<Integer> retryCountConsumer) {
        CompletableFuture<Set<String>> futureResult = new CompletableFuture<>();
        executor.submit(() -> {
            List<DocWriteRequest> elasticRequests = new ArrayList<>();

            docIds.forEach(docId -> elasticRequests.add(new DeleteRequest(
                metadata.getName(), DOC_TYPE, docId)));

            BulkRequest bulkRequest = new BulkRequest();
            if (waitForIndexingToFinish) {
                bulkRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.WAIT_UNTIL);
            }

            elasticRequests.forEach(bulkRequest::add);
            bulkRequest.timeout(TimeValue.timeValueMillis(timeOutMillis));

            executeAsyncBulkRequest(bulkRequest, futureResult, docIds, "delete",
                retryCount, retryIntervalMillis, retryCountConsumer);
        });
        return futureResult;
    }

    public boolean checkClusterHealth() {
        try {
            if (elasticsearchClient.cluster_health().getStatus() != ClusterHealthStatus.GREEN) {
                logger.warn("Cluster " + elasticsearchClient.getEndpoint() + " is not healthy");
                return false;
            }
        } catch (IOException e) {
            throw new RuntimeException("Error performing health check of " +
                elasticsearchClient.getEndpoint(), e);
        }
        return true;
    }

    private <T> void executeAsyncBulkRequest(BulkRequest bulkRequest,
        CompletableFuture<T> futureResult, T returnValueOnComplete, String operation,
        int retryCount, int retryIntervalMillis,
        Consumer<Integer> retryCountConsumer) {

        ActionListener<BulkResponse> listener = new ActionListener<BulkResponse>() {
            int timeOuts = 0;

            @Override
            public void onResponse(BulkResponse bulkResponse) {
                if (timeOuts > 0) {
                    retryCountConsumer.accept(timeOuts);
                }
                if (!bulkResponse.hasFailures()) {
                    futureResult.complete(returnValueOnComplete);
                } else {
                    StringBuilder docErrors = new StringBuilder();
                    Arrays.stream(bulkResponse.getItems())
                        .filter(BulkItemResponse::isFailed)
                        .forEach(resp ->
                            docErrors.append("Document failure: ").append(resp.getFailureMessage())
                                .append("\n")
                        );
                    futureResult.completeExceptionally(new IndexerException(
                        "Error performing " + operation + ": " + docErrors.toString()));
                }
            }

            @Override
            public void onFailure(Exception e) {
                if (e instanceof java.net.ConnectException || e instanceof java.net.SocketTimeoutException) {
                    timeOuts++;
                    if (timeOuts <= retryCount) {
                        try {
                            logger.warn("Document " + operation + " timed out. will retry");
                            sleep(retryIntervalMillis);
                            elasticsearchClient.bulkAsync(bulkRequest, this);
                            return;
                        } catch (InterruptedException ex) {
                            throw new RuntimeException(ex);
                        }
                    }
                }
                if (timeOuts > 0) {
                    retryCountConsumer.accept(timeOuts);
                }
                futureResult.completeExceptionally(e);
            }
        };
        elasticsearchClient.bulkAsync(bulkRequest, listener);
    }

    public List<IndexMetadata> cleanupOldIndexes(String indexType, int numPriorSchemaVersionsToKeep,
            int numActiveBackupsForCurrentVersion, int minIndexAgeForCleanupHours)
                throws IndexerException, IOException {
        return cleanupOldIndexes(indexType, numPriorSchemaVersionsToKeep,
            numActiveBackupsForCurrentVersion, minIndexAgeForCleanupHours, false);
    }

    /**
     * Cleans up old indexes and returns a list containing the metadata of any indexes that were
     * dropped.
     */
    public List<IndexMetadata> cleanupOldIndexes(String indexType, int numPriorSchemaVersionsToKeep,
            int numActiveBackupsForCurrentVersion, int minIndexAgeForCleanupHours,
        boolean cleanupFailedIndexes)
            throws IndexerException, IOException {
        logger.warn("Beginning cleanup for {} indexes", indexType);

        List<IndexMetadata> indexesRemoved = new ArrayList<>();
        IndexMetadata activeIndexMetadata = getActiveIndexMetadata(indexType).orElse(null);
        if(activeIndexMetadata == null) {
            logger.warn("{} index does not have an active version. skipping cleanup", indexType);
            return indexesRemoved;
        }
        for(int v = activeIndexMetadata.getSchemaVersion(); v > 0; v--) {
            indexesRemoved.addAll(cleanupOneIndexVersion(indexType, v,
                activeIndexMetadata, numPriorSchemaVersionsToKeep,
                numActiveBackupsForCurrentVersion, minIndexAgeForCleanupHours, cleanupFailedIndexes));
        }

        logger.warn("{} indexes cleanup finished, {} indexes dropped in cleanup",
            indexType, indexesRemoved.size());

        indexesRemoved.forEach(removed -> logger.warn("{} index {}:{} dropped",
            removed.getType(), removed.getName(), removed.getVersion())
        );

        return indexesRemoved;
    }

    public boolean physicalIndexExists(IndexMetadata metadata) throws IOException {
        GetIndexRequest request = new GetIndexRequest(metadata.getName());
        return elasticsearchClient.indices_exists(request);
    }

    public String getAliasName(String type, int schemaVersion) {
        return typeActiveAliasNameMap.getOrDefault(type, type) + "_v" + schemaVersion;
    }

    private List<IndexMetadata> cleanupOneIndexVersion(String indexType, int schemaVersion,
            IndexMetadata activeIndexMetadata,
            int numPriorSchemaVersionsToKeep, int numActiveBackupsForCurrentVersion,
            int minIndexAgeForCleanupHours, boolean cleanupFailedIndexes)
            throws IOException, IndexerException {
        logger.warn("Index Manager Cleanup: Starting {} index cleanup for schema version {}",
            indexType, schemaVersion);
        int currentSchemaVersion = activeIndexMetadata.getSchemaVersion();
        String alias = getAliasName(indexType, schemaVersion);
        Optional<IndexMetadata> aliasMetadata = getIndexMetadataForAlias(alias);

        if (currentSchemaVersion - schemaVersion > numPriorSchemaVersionsToKeep) {
            logger.warn("Index Manager Cleanup: {} is more than {} versions old, removing alias {}",
                schemaVersion, numPriorSchemaVersionsToKeep, alias);
            removeAlias(alias);
        } else {
            if (!aliasMetadata.isPresent() && schemaVersion == currentSchemaVersion) {
                logger.warn("Index Manager Cleanup: there is no alias {}!  Refusing to clean indexes for schema version {}, manually fix or reindex",
                    alias, schemaVersion);
                return Collections.emptyList();
            }

            if (aliasMetadata.isPresent() && !physicalIndexExists(aliasMetadata.get()) &&
                    schemaVersion == currentSchemaVersion) {
                logger.warn("Index Manager Cleanup: existing alias for current version points to an index which does not physically exist!  Refusing to clean indexes, manually fix or reindex");
                return Collections.emptyList();
            }
            logger.warn("Index Manager Cleanup: existing alias {} points to {}",
                alias, aliasMetadata.isPresent() ? aliasMetadata.get().getName() : "NOTHING");
        }

        List<IndexMetadata> indices = getIndexMetadata().stream()
            .filter(metadata -> metadata.getType().equals(indexType))
            .filter(metadata -> metadata.getSchemaVersion() == schemaVersion)
            .sorted(Comparator.comparing(IndexMetadata::getIndexCreateDateTime).reversed())
            .collect(Collectors.toList());

        List<IndexMetadata> indexesToRemove = new ArrayList<>();

        boolean foundExistingPhysical = false;
        List<IndexMetadata> backups = new ArrayList<>();

        logger.warn("Index Manager Cleanup: Indexes to be inspected, in order of freshest to least fresh:");
        indices.forEach(metadata -> logger.info("  {}:{} {}",
            metadata.getName(), metadata.getVersion(),
            metadata.equals(activeIndexMetadata) ? "** ACTIVE" : ""));

        for(IndexMetadata metadata : indices) {
            logger.warn("Index Manager Cleanup: inspecting {}:{}",
                metadata.getName(), metadata.getVersion());

            if (currentSchemaVersion - schemaVersion > numPriorSchemaVersionsToKeep) {
                logger.warn("Index Manager Cleanup: index is more than {} schema versions ago, removing",
                    numPriorSchemaVersionsToKeep);
                indexesToRemove.add(metadata);
            } else if (aliasMetadata.isPresent() && metadata.equals(aliasMetadata.get())) {
                logger.warn("Index Manager Cleanup: found entry for current active index {}, looking for backups",
                    metadata.getName());
                foundExistingPhysical = true;
            } else if (!foundExistingPhysical && metadata.getIndexingStatus() == IndexingStatusEnum.COMPLETE) {
                logger.warn("Index Manager Cleanup: {} is complete and newer than the active index",
                    metadata.getName());
            } else if (metadata.getIndexingStatus() != IndexingStatusEnum.COMPLETE) {
                logger.warn("Index Manager Cleanup: {} cannot be backup because it is not complete", metadata.getName());
                if(cleanupFailedIndexes) {
                    logger.warn("Cleaning up failed index {}", metadata.getName());
                    indexesToRemove.add(metadata);
                }
            } else if (!physicalIndexExists(metadata)) {
                logger.warn("Index Manager Cleanup: {} cannot be backup because the physical index does not actually exist", metadata.getName());
                if(cleanupFailedIndexes) {
                    logger.warn("Cleaning up failed index {}", metadata.getName());
                    indexesToRemove.add(metadata);
                }
            } else if (backups.size() < numActiveBackupsForCurrentVersion) {
                backups.add(metadata);
                logger.warn("Index Manager Cleanup: found good backup for current active index - backup {} is {}",
                    backups.size(), metadata.getName());
            } else {
                // this index is neither the main, nor the backup
                logger.warn("Index Manager Cleanup: {} is neither the current active or a valid backup, it will be removed",
                    metadata.getName());
                indexesToRemove.add(metadata);
            }
        }

        List<IndexMetadata> removedIndexes = new ArrayList<>();
        for (IndexMetadata indexToRemove : indexesToRemove) {
            LocalDateTime indexCreateDateTime = indexToRemove.getIndexCreateDateTime() != null ?
                indexToRemove.getIndexCreateDateTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime() : null;
            if ((indexCreateDateTime == null || indexCreateDateTime.isAfter(
                LocalDateTime.now().minusHours(minIndexAgeForCleanupHours)))) {
                logger.warn("Index Manager Cleanup: Skipping cleanup of {} because it is less than {} hours old",
                    indexToRemove.getName(), minIndexAgeForCleanupHours);
                continue;
            }
            // remove it from index manager, and remove the physical index too
            if (physicalIndexExists(indexToRemove) && !dropIndex(indexToRemove)) {
                logger.warn("Index Manager Cleanup: could not drop {} even though it exists!!  Will try again later...",
                    indexToRemove.getName());
            } else if (!deleteIndexManagerDocument(indexToRemove.getName())) {
                logger.warn("Index Manager Cleanup: could not delete the index manager document for {}.... will try again later...",
                    indexToRemove.getName());
            } else {
                removedIndexes.add(indexToRemove);
            }
        }
        return removedIndexes;
    }

    private boolean dropIndex(IndexMetadata metadata) throws IOException {
        DeleteIndexRequest request = new DeleteIndexRequest(metadata.getName());
        AcknowledgedResponse deleteIndexResponse = elasticsearchClient.indices_delete(request);
        return deleteIndexResponse != null && deleteIndexResponse.isAcknowledged();
    }

    private boolean deleteIndexManagerDocument(String name) throws IOException {
        DeleteRequest request = new DeleteRequest(metadataIndexName, name);
        DeleteResponse deleteResponse = elasticsearchClient.delete(request);
        return deleteResponse != null
            && deleteResponse.getResult() == DocWriteResponse.Result.DELETED;
    }

    public int getDefaultNumPriorSchemaVersionsToKeep() {
        return defaultNumPriorSchemaVersionsToKeep;
    }

    public int getDefaultNumActiveBackupsForCurrentVersion() {
        return defaultNumActiveBackupsForCurrentVersion;
    }

    public int getDefaultMinIndexAgeForCleanupHours() {
        return defaultMinIndexAgeForCleanupHours;
    }

    @Override
    public void close() throws Exception {
        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.MINUTES);
    }
}
