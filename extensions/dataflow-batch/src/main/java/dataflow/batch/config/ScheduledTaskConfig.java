/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ScheduledTaskConfig.java
 */

package dataflow.batch.config;

import java.time.Duration;
import java.util.Objects;

public class ScheduledTaskConfig extends TaskConfig {

    private String scheduleCron;
    private Duration frequency;

    public String getScheduleCron() {
        return scheduleCron;
    }

    public void setScheduleCron(String scheduleCron) {
        this.scheduleCron = scheduleCron;
    }

    public Duration getFrequency() {
        return frequency;
    }

    public void setFrequency(Duration frequency) {
        this.frequency = frequency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        ScheduledTaskConfig that = (ScheduledTaskConfig) o;
        return Objects.equals(getScheduleCron(), that.getScheduleCron()) && Objects
            .equals(getFrequency(), that.getFrequency());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getScheduleCron(), getFrequency());
    }

    @Override
    public String toString() {
        return "ScheduledTaskConfig{" +
            "scheduleCron='" + scheduleCron + '\'' +
            ", frequency=" + frequency +
            "} " + super.toString();
    }
}
