/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  CassandraClusterManager.java
 */

package dataflow.cassandra;

import java.util.HashMap;
import java.util.Map;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.CodecRegistry;

/**
 * A manager for Cassandra cluster objects. Other classes can get a registered cluster given its id.
 */
public class CassandraClusterManager {

    private String defaultClusterId;
    private Map<String, Cluster> idClusterMap = new HashMap<>();

    /**
     * A convenience method to register codecs for the DataFlow primitive wrapers eg.
     * {@link dataflow.core.value.IntegerValue}, {@link dataflow.core.value.DoubleValue}, etc.
     * with the Cassandra driver {@link CodecRegistry}.
     */
    public static void registerDataFlowPrimitiveValueCodecs(CodecRegistry registry) {
        registry.register(BooleanValueCodec.INSTANCE);
        registry.register(IntegerValueCodec.INSTANCE);
        registry.register(LongValueCodec.INSTANCE);
        registry.register(FloatValueCodec.INSTANCE);
        registry.register(DoubleValueCodec.INSTANCE);
    }

    /**
     * Sets the id of the cluster that will be returned by {@link #getDefaultCluster()}
     */
    public void setDefaultClusterId(String defaultClusterId) {
        this.defaultClusterId = defaultClusterId;
    }

    /**
     * Registers a cassandra cluster with the given id.
     */
    public void registerCluster(String id, Cluster cluster) {
        idClusterMap.put(id, cluster);
    }

    /**
     * Returns the cluster with the given id.
     */
    public Cluster getCluster(String id) {
        Cluster cluster = idClusterMap.get(id);
        if (cluster == null) {
            throw new IllegalArgumentException("Cluster " + id + " is not registered");
        }
        return cluster;
    }

    /**
     * Returns the default cluster or null if no default cluster is specified.
     */
    public Cluster getDefaultCluster() {
        if (idClusterMap.size() == 1) {
            return idClusterMap.values().iterator().next();
        }
        Cluster cluster = idClusterMap.get(defaultClusterId);
        if (cluster == null) {
            throw new IllegalStateException("No default cassandra cluster specified");
        }
        return cluster;
    }
}
