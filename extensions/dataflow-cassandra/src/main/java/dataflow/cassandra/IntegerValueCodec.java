/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  IntegerValueCodec.java
 */

package dataflow.cassandra;

import java.nio.ByteBuffer;

import com.datastax.driver.core.DataType;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.core.exceptions.InvalidTypeException;
import dataflow.core.value.IntegerValue;

/**
 * A {@link TypeCodec} for DataFlow {@link IntegerValue}.
 */
public class IntegerValueCodec extends TypeCodec<IntegerValue> {

    public static final IntegerValueCodec INSTANCE = new IntegerValueCodec();

    private IntegerValueCodec() {
        super(DataType.cint(), IntegerValue.class);
    }

    @Override
    public ByteBuffer serialize(final IntegerValue value,
            final ProtocolVersion protocolVersion) throws InvalidTypeException {
        ByteBuffer bb = ByteBuffer.allocate(4);
        bb.putInt(0, value.toInt());
        return bb;
    }

    @Override
    public IntegerValue deserialize(final ByteBuffer bytes, final ProtocolVersion protocolVersion)
            throws InvalidTypeException {
        if (bytes == null || bytes.remaining() == 0)
            return new IntegerValue();
        if (bytes.remaining() != 4)
            throw new InvalidTypeException(
                    "Invalid 32-bits integer value, expecting 4 bytes but got " + bytes.remaining());
        return new IntegerValue(bytes.getInt(bytes.position()));
    }

    @Override
    public IntegerValue parse(String value) {
        if (value == null || value.isEmpty() || value.equalsIgnoreCase("NULL")) {
            return null;
        }
        try {
            IntegerValue retVal = new IntegerValue();
            retVal.fromString(value);
            return retVal;
        } catch (NumberFormatException e) {
            throw new InvalidTypeException(String.format("Cannot parse 32-bits int value from \"%s\"", value));
        }
    }

    @Override
    public String format(IntegerValue value) {
        if (value == null) return "NULL";
        return value.toString();
    }
}
