/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  CassandraTableValueProvider.java
 */

package dataflow.cassandra;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;

import org.checkerframework.checker.nullness.qual.Nullable;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValues;
import dataflow.annotation.OutputValue;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.exception.DataFlowConfigurationException;

import static dataflow.core.util.StringUtil.join;

@DataFlowComponent(description = "Provides a value from a cassandra table")
public class CassandraTableValueProvider {

    private final List<String> keyColumns;
    private final Set<String> outputColumns;
    private final PreparedStatement queryPreparedStatement;
    private final CassandraTableOutputMode outputMode;

    private Session session;

    @DataFlowConfigurable
    public CassandraTableValueProvider(
            @DataFlowConfigProperty(description = "The name of the keyspace of the table") String keyspace,
            @DataFlowConfigProperty(description = "The name of the table") String tableName,
            @DataFlowConfigProperty(description = "The names of the columns that form the key of the table") List<String> keyColumns,
            @DataFlowConfigProperty(required = false, description =
                    "The names of the columns that are output in the provided value. " +
                            "All columns will be included if this is not set") List<String> outputColumns,
            @DataFlowConfigProperty(required = false, description =
                    "The query to use to get values (not needed if key and columns are specified). " +
                            "Input values will be used to replace named parameters. See the Prepared statements section of the " +
                            "Cassandra documentation for more information.\\n\\n" +
                            "Example:\\n" +
                            "select age from products where id = :product_id\\n\\n" +
                            "In this example the input named product_id will be used to replace :product_id\\n") String query,
            @DataFlowConfigProperty(required = false, description =
                    "The read consistency level. See the Cassandra driver documentation for more details. One of the following values:\\n\\n" +
                            "ANY, ONE, TWO, THREE, QUORUM, ALL, LOCAL_QUORUM. EACH_QUORUM, SERIAL, LOCAL_SERIAL, LOCAL_ONE\\n") ConsistencyLevel consistencyLevel,
            @DataFlowConfigProperty(required = false, description =
                    "SINGLE_ROW (default)\\n" +
                            "The provider will output a single map<string, object> instead of a list. If the cassandra query " +
                            "returns multiple rows only the first row is used.\\n\\n" +
                            "MULTI_ROW\\n" +
                            "The provider will output a list rows where each row is a map<string, object>\\n\\n" +
                            "SINGLE_COLUMN\\n" +
                            "The provider will output a single value. If the cassandra query returns multiple rows only the " +
                            "first row is used. Only one column can be specified in the columns list.\\n") CassandraTableOutputMode outputMode,
            @DataFlowConfigProperty(required = false, description =
                    "A string that identifies the cluster to use if more than one cassandra cluster is defined. " +
                            "A cluster with the given id should be registered with the CassandraClusterManager") String clusterId) {
        if (keyspace == null || keyspace.isEmpty()) {
            throw new DataFlowConfigurationException("Keyspace was not specified");
        }
        if (tableName == null || tableName.isEmpty()) {
            throw new DataFlowConfigurationException("Table name was not specified");
        }
        this.keyColumns = keyColumns != null ? new ArrayList<>(keyColumns) : Collections.emptyList();
        this.outputColumns = outputColumns != null ? new LinkedHashSet<>(outputColumns) : Collections.emptySet();
        this.outputMode = outputMode;

        if (outputMode == CassandraTableOutputMode.SINGLE_COLUMN && this.outputColumns.size() != 1) {
            throw new DataFlowConfigurationException(
                    "Only a single output column should be specified for single column output mode");
        }

        if ((query == null || query.isEmpty()) && (keyColumns == null || keyColumns.isEmpty())) {
            throw new DataFlowConfigurationException("Either key columns or a query must be specified");
        }

        CassandraClusterManager clusterManager = DataFlowExecutionContext.getCurrentExecutionContext()
                .getDependencyInjector().getInstance(CassandraClusterManager.class);

        if (clusterManager == null) {
            throw new DataFlowConfigurationException("Unable to inject CassandraClusterManager. " +
                    "Please verify that the dependency injector is configured properly.");
        }
        query = (query != null && !query.isEmpty()) ? query : buildQuery(keyColumns, outputColumns, keyspace,
                tableName);
        this.session = (clusterId != null) ?
                clusterManager.getCluster(clusterId).connect(keyspace) :
                clusterManager.getDefaultCluster().connect(keyspace);
        this.queryPreparedStatement = session.prepare(query);
        this.queryPreparedStatement.setConsistencyLevel(consistencyLevel);
    }

    @OutputValue(description =
            "Outputs the list of rows, single row, or single column value depending on the outputMode. " +
                    "See the outputMode property documentation for more details")
    public CompletableFuture<Object> getValue(@InputValues(description =
            "The key values. The name of each input should match the name of the key") Map<String, ?> keyValues) {
        CompletableFuture<Object> resultFuture = new CompletableFuture<>();
        BoundStatement stmt = queryPreparedStatement.bind();

        if (keyColumns != null) {
            keyColumns.forEach(keyColumn -> {
                if (!keyValues.containsKey(keyColumn))
                    throw new IllegalArgumentException("Key value " + keyColumn + " was not specified");
            });
        }
        for (Map.Entry<String, ?> entry : keyValues.entrySet()) {
            Object value = entry.getValue();
            if (value != null) {
                stmt.set(entry.getKey(), value, (Class<Object>) value.getClass());
            } else {
                stmt.setToNull(entry.getKey());
            }
        }
        ListenableFuture<ResultSet> resultSetFuture = session.executeAsync(stmt);
        Futures.addCallback(resultSetFuture, new FutureCallback<ResultSet>() {
            @Override
            public void onSuccess(@Nullable final ResultSet resultSet) {
                if (resultSet == null) {
                    resultFuture.complete(null);
                    return;
                }
                switch (outputMode) {
                    case SINGLE_COLUMN:
                        resultFuture.complete(buildSingleColumnResult(resultSet));
                        break;
                    case SINGLE_ROW:
                        resultFuture.complete(buildSingleRowResult(resultSet));
                        break;
                    case MULTI_ROW:
                        resultFuture.complete(buildMultiRowResult(resultSet));
                        break;
                }
            }

            @Override
            public void onFailure(@Nonnull final Throwable throwable) {
                resultFuture.completeExceptionally(throwable);
            }
        }, MoreExecutors.directExecutor());
        return resultFuture;
    }

    private List<Map<String, Object>> buildMultiRowResult(ResultSet resultSet) {
        List<Map<String, Object>> resultsList = new ArrayList<>();
        Set<String> columnNames = !outputColumns.isEmpty() ? outputColumns : getAllColumnNames(resultSet);
        resultSet.forEach(row -> resultsList.add(buildRowMap(row, columnNames)));
        return resultsList;
    }

    private Map<String, Object> buildSingleRowResult(ResultSet resultSet) {
        Row row = resultSet.one();
        Set<String> columnNames = !outputColumns.isEmpty() ? outputColumns : getAllColumnNames(resultSet);
        return row != null ? buildRowMap(row, columnNames) : null;
    }

    private Object buildSingleColumnResult(ResultSet resultSet) {
        Row row = resultSet.one();
        Set<String> columnNames = !outputColumns.isEmpty() ? outputColumns : getAllColumnNames(resultSet);
        return row != null ? buildRowMap(row, columnNames).values().iterator().next() : null;
    }

    private static Map<String, Object> buildRowMap(Row row, Set<String> outputColumns) {
        Map<String, Object> retVal = new HashMap<>();
        for (String columnName : outputColumns) {
            Object value = row.getObject(columnName);
            if (value != null) {
                retVal.put(columnName, value);
            }
        }
        return retVal;
    }

    private static Set<String> getAllColumnNames(ResultSet resultSet) {
        int columnCount = resultSet.getColumnDefinitions().size();
        Set<String> columnNames = new HashSet<>(columnCount);
        for(int idx = 0; idx < columnCount; idx++) {
            columnNames.add(resultSet.getColumnDefinitions().getName(idx));
        }
        return columnNames;
    }

    private static String buildQuery(List<String> keyColumns, List<String> outputColumns, String keyspace,
            String tableName) {
        StringBuilder result = new StringBuilder();
        result.append("SELECT ");
        if (outputColumns != null && !outputColumns.isEmpty()) {
            join(result, outputColumns, ", ");
        } else {
            result.append("*");
        }
        result.append(" FROM ").append(keyspace).append(".").append(tableName);
        result.append(" WHERE ");
        join(result, keyColumns.stream().map(k -> k + " = :" + k).collect(Collectors.toList()), " AND ");
        return result.toString();
    }
}
