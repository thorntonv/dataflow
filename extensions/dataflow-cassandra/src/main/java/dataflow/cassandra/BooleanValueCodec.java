/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  BooleanValueCodec.java
 */

package dataflow.cassandra;

import java.nio.ByteBuffer;

import com.datastax.driver.core.DataType;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.core.exceptions.InvalidTypeException;
import dataflow.core.value.BooleanValue;

/**
 * A {@link TypeCodec} for DataFlow {@link BooleanValue}.
 */
public class BooleanValueCodec extends TypeCodec<BooleanValue> {

    public static final BooleanValueCodec INSTANCE = new BooleanValueCodec();

    private static final ByteBuffer TRUE = ByteBuffer.wrap(new byte[] {1});
    private static final ByteBuffer FALSE = ByteBuffer.wrap(new byte[] {0});

    private BooleanValueCodec() {
        super(DataType.cboolean(), BooleanValue.class);
    }

    @Override
    public ByteBuffer serialize(final BooleanValue value,
            final ProtocolVersion protocolVersion) throws InvalidTypeException {
        return value.toBoolean() ? TRUE.duplicate() : FALSE.duplicate();
    }

    @Override
    public BooleanValue deserialize(final ByteBuffer bytes, final ProtocolVersion protocolVersion)
            throws InvalidTypeException {
        if (bytes == null || bytes.remaining() == 0)
            return new BooleanValue();
        if (bytes.remaining() != 1)
            throw new InvalidTypeException(
                    "Invalid 1-byte boolean value, but got " + bytes.remaining());
        return new BooleanValue(bytes.get() != 0);
    }

    @Override
    public BooleanValue parse(String value) {
        if (value == null || value.isEmpty() || value.equalsIgnoreCase("NULL")) {
            return null;
        }
        try {
            BooleanValue retVal = new BooleanValue();
            retVal.fromString(value);
            return retVal;
        } catch (NumberFormatException e) {
            throw new InvalidTypeException(String.format("Cannot parse boolean value from \"%s\"", value));
        }
    }

    @Override
    public String format(BooleanValue value) {
        if (value == null) return "NULL";
        return value.toString();
    }
}
