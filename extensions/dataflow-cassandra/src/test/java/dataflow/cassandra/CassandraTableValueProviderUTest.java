/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  CassandraTableValueProviderUTest.java
 */

package dataflow.cassandra;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Session;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.SimpleDependencyInjector;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CassandraTableValueProviderUTest {

    static final String TEST_KEYSPACE = "test";
    static final String TEST_TABLE_NAME = "users";
    static final String USER_ID_COLUMN = "user_id";
    static final String NAME_COLUMN = "name";
    static final String POSTAL_CODE_COLUMN = "postalcode";

    static final List<String> DEFAULT_KEY_COLUMN_NAMES = ImmutableList.of(USER_ID_COLUMN);
    static final List<String> DEFAULT_OUTPUT_COLUMN_NAMES = ImmutableList.of(NAME_COLUMN, POSTAL_CODE_COLUMN);
    static final Map<String, Object> TEST_INPUT1 = ImmutableMap.<String, Object>builder()
            .put(USER_ID_COLUMN, 1)
            .build();
    static final Map<String, Object> TEST_INPUT2 = ImmutableMap.<String, Object>builder()
            .put(USER_ID_COLUMN, 2)
            .build();
    static final Map<String, Object> TEST_INPUT_NON_EXISTENT_USER = ImmutableMap.<String, Object>builder()
            .put(USER_ID_COLUMN, 1001)
            .build();
    private static final String TEST_QUERY1 = "SELECT name, postalcode FROM test.users WHERE user_id = :user_id";

    static final String TEST_NAME = "john";
    static final Integer TEST_POSTAL_CODE = 94941;
    static final String TEST_NAME2 = "jim";
    static final Integer TEST_POSTAL_CODE2 = 94111;

    @Mock
    private Cluster mockCluster;
    @Mock
    private Session mockSession;
    @Mock
    private PreparedStatement mockPreparedStatement;
    @Mock
    private BoundStatement mockBoundStatement;
    @Captor
    private ArgumentCaptor<String> queryCaptor;
    @Mock
    private ResultSetFuture mockResultSetFuture;
    @Mock
    private ResultSet mockResultSet;
    @Captor
    private ArgumentCaptor<Runnable> callbackCaptor;

    @Before
    public void setUp() throws ExecutionException, InterruptedException {
        final CassandraClusterManager clusterManager = new CassandraClusterManager();
        clusterManager.registerCluster("test", mockCluster);
        final DataFlowEngine engine = new DataFlowEngine(new SimpleDependencyInjector(clusterManager));
        DataFlowExecutionContext.createExecutionContext(null, engine);

        when(mockCluster.connect(TEST_KEYSPACE)).thenReturn(mockSession);
        when(mockSession.prepare(queryCaptor.capture())).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.bind()).thenReturn(mockBoundStatement);
        when(mockSession.executeAsync(mockBoundStatement)).thenReturn(mockResultSetFuture);
        when(mockResultSetFuture.isDone()).thenReturn(true);
        doNothing().when(mockResultSetFuture).addListener(callbackCaptor.capture(), any());
        when(mockResultSetFuture.get()).thenReturn(mockResultSet);
    }

    @Test
    public void getValue_noResult() throws ExecutionException, InterruptedException {
        when(mockResultSetFuture.get()).thenReturn(null);
        CassandraTableValueProvider valueProvider = buildValueProvider(
                DEFAULT_KEY_COLUMN_NAMES, ImmutableList.of(POSTAL_CODE_COLUMN), CassandraTableOutputMode.SINGLE_COLUMN);
        CompletableFuture<Object> futureResult = valueProvider.getValue(TEST_INPUT1);

        callbackCaptor.getValue().run();
        assertNull(futureResult.get());
    }

    @Test
    public void getValue_inputMissingKeyValue() throws ExecutionException, InterruptedException {
        when(mockResultSetFuture.get()).thenReturn(null);
        CassandraTableValueProvider valueProvider = buildValueProvider(
                DEFAULT_KEY_COLUMN_NAMES, ImmutableList.of(POSTAL_CODE_COLUMN), CassandraTableOutputMode.SINGLE_COLUMN);

        try {
            valueProvider.getValue(new HashMap<>());
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            // Expected.
        }
    }

    @Test
    public void getValue_error() throws ExecutionException, InterruptedException {
        when(mockResultSetFuture.get()).thenThrow(new RuntimeException());

        CassandraTableValueProvider valueProvider = buildValueProvider(
                DEFAULT_KEY_COLUMN_NAMES, DEFAULT_OUTPUT_COLUMN_NAMES, CassandraTableOutputMode.SINGLE_ROW);
        CompletableFuture<Object> futureResult = valueProvider.getValue(TEST_INPUT1);
        assertEquals(TEST_QUERY1, queryCaptor.getValue());

        callbackCaptor.getValue().run();
        try {
            futureResult.get();
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            // Expected.
        }
    }

    static CassandraTableValueProvider buildValueProvider(String query, List<String> outputColumnNames,
            CassandraTableOutputMode outputMode) {
        return new CassandraTableValueProvider(TEST_KEYSPACE, TEST_TABLE_NAME, null, outputColumnNames,
                query, ConsistencyLevel.ONE, outputMode, null);
    }

    static CassandraTableValueProvider buildValueProvider(List<String> keyColumnNames,
            List<String> outputColumnNames,
            CassandraTableOutputMode outputMode) {
        return new CassandraTableValueProvider(TEST_KEYSPACE, TEST_TABLE_NAME, keyColumnNames, outputColumnNames,
                null, ConsistencyLevel.ONE, outputMode, null);
    }
}