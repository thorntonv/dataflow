/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AbstractTypeCodecTest.java
 */

package dataflow.cassandra;

import java.nio.ByteBuffer;

import org.junit.Test;
import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.core.exceptions.InvalidTypeException;

import static org.junit.Assert.*;

public abstract class AbstractTypeCodecTest<T> {

    abstract TypeCodec<T> getTypeCodec();

    abstract T getTestValue();

    abstract String getTestValueAsString();

    abstract T getEmptyValue();

    @Test
    public void testSerializeDeserialize() {
        ByteBuffer buffer = getTypeCodec().serialize(getTestValue(), null);
        T deserialized = getTypeCodec().deserialize(buffer, null);
        assertEquals(getTestValue(), deserialized);
    }

    @Test
    public void testDeserialize_invalidBuffer() {
        ByteBuffer buffer = ByteBuffer.allocate(100);
        buffer.put("invalid".getBytes());
        try {
            getTypeCodec().deserialize(buffer, null);
            fail("Expected exception was not thrown");
        } catch(InvalidTypeException ex) {
            // Expected.
        }
    }

    @Test
    public void testDeserialize_emptyBuffer() {
        ByteBuffer buffer = ByteBuffer.allocate(0);
        assertEquals(getEmptyValue(), getTypeCodec().deserialize(buffer, null));
    }

    @Test
    public void testParse() {
        assertEquals(getTestValue(), getTypeCodec().parse(getTestValueAsString()));
    }

    @Test
    public void testParse_invalidString() {
        try {
            getTypeCodec().parse("test");
            fail("Expected exception was not thrown");
        } catch (InvalidTypeException ex) {
            // Expected.
        }
    }

    @Test
    public void testParse_emptyString() {
        assertNull(getTypeCodec().parse(""));
    }

    @Test
    public void testFormat() {
        assertEquals(getTestValueAsString(), getTypeCodec().format(getTestValue()));
    }
}