/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  CassandraTableValueProviderITest.java
 */

package dataflow.cassandra;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.cassandraunit.CQLDataLoader;
import org.cassandraunit.dataset.CQLDataSet;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.CodecRegistry;
import com.datastax.driver.core.QueryOptions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.value.IntegerValue;

import static dataflow.cassandra.CassandraTableValueProviderUTest.*;
import static org.cassandraunit.utils.EmbeddedCassandraServerHelper.getHost;
import static org.cassandraunit.utils.EmbeddedCassandraServerHelper.getNativeTransportPort;
import static org.junit.Assert.*;

/**
 * Integration tests for {@link CassandraTableValueProvider} that use an embedded Cassandra instance.
 */
public class CassandraTableValueProviderITest {

    @BeforeClass
    public static void beforeClass() throws Exception {
        EmbeddedCassandraServerHelper.startEmbeddedCassandra(
                EmbeddedCassandraServerHelper.CASSANDRA_RNDPORT_YML_FILE);

        EmbeddedCassandraServerHelper.cleanEmbeddedCassandra();
        CQLDataLoader dataLoader = new CQLDataLoader(EmbeddedCassandraServerHelper.getSession());
        CQLDataSet dataSet = new ClassPathCQLDataSet("test.cql", CassandraTableValueProviderUTest.TEST_KEYSPACE);
        dataLoader.load(dataSet);
    }

    @Before
    public void setUp() {
        CassandraClusterManager clusterManager = new CassandraClusterManager();

        QueryOptions queryOptions = new QueryOptions();
        queryOptions.setRefreshSchemaIntervalMillis(0);
        queryOptions.setRefreshNodeIntervalMillis(0);
        queryOptions.setRefreshNodeListIntervalMillis(0);
        CodecRegistry codecRegistry = new CodecRegistry();
        CassandraClusterManager.registerDataFlowPrimitiveValueCodecs(codecRegistry);
        Cluster cluster = Cluster.builder()
                .addContactPoints(getHost())
                .withCodecRegistry(codecRegistry)
                .withPort(getNativeTransportPort())
                .withoutJMXReporting()
                .withQueryOptions(queryOptions)
                .build();

        clusterManager.registerCluster("test", cluster);
        DataFlowEngine engine = new DataFlowEngine(new SimpleDependencyInjector(clusterManager));
        CassandraDataFlowExtension.register(engine);
        DataFlowExecutionContext.createExecutionContext(null, engine);
    }

    @Test
    public void getValue_multiRow() throws ExecutionException, InterruptedException {
        CassandraTableValueProvider valueProvider = buildValueProvider(
                "SELECT * from test.users", DEFAULT_OUTPUT_COLUMN_NAMES, CassandraTableOutputMode.MULTI_ROW);
        CompletableFuture<Object> futureResult = valueProvider.getValue(Collections.emptyMap());

        List<Map<String, Object>> result = (List<Map<String, Object>>) futureResult.get();
        assertEquals(2, result.size());
        assertEquals(TEST_NAME, result.get(0).get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE, result.get(0).get(POSTAL_CODE_COLUMN));
        assertEquals(TEST_NAME2, result.get(1).get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE2, result.get(1).get(POSTAL_CODE_COLUMN));
    }

    @Test
    public void getValue_multiRow_noResult() throws ExecutionException, InterruptedException {
        CassandraTableValueProvider valueProvider = buildValueProvider(
                "SELECT * from test.users WHERE user_id = 1001", DEFAULT_OUTPUT_COLUMN_NAMES, CassandraTableOutputMode.MULTI_ROW);
        CompletableFuture<Object> futureResult = valueProvider.getValue(Collections.emptyMap());
        List<Map<String, Object>> result = (List<Map<String, Object>>) futureResult.get();
        assertEquals(0, result.size());
    }

    @Test
    public void getValue_singleRow() throws ExecutionException, InterruptedException {
        CassandraTableValueProvider valueProvider = buildValueProvider(
                DEFAULT_KEY_COLUMN_NAMES, DEFAULT_OUTPUT_COLUMN_NAMES, CassandraTableOutputMode.SINGLE_ROW);
        CompletableFuture<Object> futureResult = valueProvider.getValue(TEST_INPUT1);

        Map<String, Object> result = (Map<String, Object>) futureResult.get();
        assertEquals(TEST_NAME, result.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE, result.get(POSTAL_CODE_COLUMN));
    }

    @Test
    public void getValue_singleRow_query() throws ExecutionException, InterruptedException {
        CassandraTableValueProvider valueProvider = buildValueProvider(
                "SELECT * FROM test.users WHERE postalcode = :postalcode ALLOW FILTERING", DEFAULT_OUTPUT_COLUMN_NAMES, CassandraTableOutputMode.SINGLE_ROW);
        CompletableFuture<Object> futureResult = valueProvider.getValue(ImmutableMap.<String, Object>builder()
                .put("postalcode", 94111)
                .build());

        Map<String, Object> result = (Map<String, Object>) futureResult.get();
        assertEquals(TEST_NAME2, result.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE2, result.get(POSTAL_CODE_COLUMN));
    }

    @Test
    public void getValue_singleRow_concurrentRequests() throws ExecutionException, InterruptedException {
        CassandraTableValueProvider valueProvider = buildValueProvider(
                DEFAULT_KEY_COLUMN_NAMES, DEFAULT_OUTPUT_COLUMN_NAMES, CassandraTableOutputMode.SINGLE_ROW);
        CompletableFuture<Object> futureResult1 = valueProvider.getValue(TEST_INPUT1);
        CompletableFuture<Object> futureResult2 = valueProvider.getValue(TEST_INPUT2);

        Map<String, Object> result1 = (Map<String, Object>) futureResult1.get();
        assertEquals(TEST_NAME, result1.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE, result1.get(POSTAL_CODE_COLUMN));
        Map<String, Object> result2 = (Map<String, Object>) futureResult2.get();
        assertEquals(TEST_NAME2, result2.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE2, result2.get(POSTAL_CODE_COLUMN));
    }

    @Test
    public void getValue_singleRow_noResult() throws ExecutionException, InterruptedException {
        CassandraTableValueProvider valueProvider = buildValueProvider(
                DEFAULT_KEY_COLUMN_NAMES, DEFAULT_OUTPUT_COLUMN_NAMES, CassandraTableOutputMode.SINGLE_ROW);
        CompletableFuture<Object> futureResult = valueProvider.getValue(TEST_INPUT_NON_EXISTENT_USER);
        assertNull(futureResult.get());
    }

    @Test
    public void getValue_singleRow_allColumns() throws ExecutionException, InterruptedException {
        CassandraTableValueProvider valueProvider = buildValueProvider(
                DEFAULT_KEY_COLUMN_NAMES, null, CassandraTableOutputMode.SINGLE_ROW);
        CompletableFuture<Object> futureResult = valueProvider.getValue(TEST_INPUT1);
        Map<String, Object> result = (Map<String, Object>) futureResult.get();
        assertEquals(1, result.get(USER_ID_COLUMN));
        assertEquals(TEST_NAME, result.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE, result.get(POSTAL_CODE_COLUMN));
    }

    @Test
    public void getValue_singleColumn() throws ExecutionException, InterruptedException {
        CassandraTableValueProvider valueProvider = buildValueProvider(
                DEFAULT_KEY_COLUMN_NAMES, ImmutableList.of(POSTAL_CODE_COLUMN), CassandraTableOutputMode.SINGLE_COLUMN);
        CompletableFuture<Object> futureResult = valueProvider.getValue(TEST_INPUT1);
        assertEquals(TEST_POSTAL_CODE, futureResult.get());
    }

    @Test
    public void getValue_singleColumn_noResult() throws ExecutionException, InterruptedException {
        CassandraTableValueProvider valueProvider = buildValueProvider(
                DEFAULT_KEY_COLUMN_NAMES, ImmutableList.of(POSTAL_CODE_COLUMN), CassandraTableOutputMode.SINGLE_COLUMN);
        CompletableFuture<Object> futureResult = valueProvider.getValue(TEST_INPUT_NON_EXISTENT_USER);
        assertNull(futureResult.get());
    }

    /**
     * Tests that {@link IntegerValue} is converted to integer using the {@link IntegerValueCodec}.
     */
    @Test
    public void getValue_integerValueKeyValue() throws ExecutionException, InterruptedException {
        CassandraTableValueProvider valueProvider = buildValueProvider(
                DEFAULT_KEY_COLUMN_NAMES, DEFAULT_OUTPUT_COLUMN_NAMES, CassandraTableOutputMode.SINGLE_ROW);
        CompletableFuture<Object> futureResult = valueProvider.getValue(ImmutableMap.<String, Object>builder()
                .put(USER_ID_COLUMN, new IntegerValue(1))
                .build());

        Map<String, Object> result = (Map<String, Object>) futureResult.get();
        assertEquals(TEST_NAME, result.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE, result.get(POSTAL_CODE_COLUMN));
    }
}
