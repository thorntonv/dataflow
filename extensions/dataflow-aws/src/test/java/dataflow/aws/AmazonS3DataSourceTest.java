/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AmazonS3DataSourceTest.java
 */

package dataflow.aws;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.util.IOUtils;
import dataflow.core.datasource.DataSource;
import dataflow.core.datasource.DataSourceUtil;
import io.findify.s3mock.S3Mock;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AmazonS3DataSourceTest {

    private static final String TEST_BUCKET_NAME = "testbucket";

    private static final String TEST_DIR1_PATH = "/dir1/";
    private static final String TEST_DIR2_PATH = "/dir2/";

    private static final String TEST_FILE1_PATH = "/dir1/file-1.txt";
    private static final String TEST_FILE1_KEY = TEST_FILE1_PATH.substring(1);
    private static final String TEST_FILE1_DATA = "data1";
    private static final String TEST_FILE2_PATH = "/dir1/file-2.txt";
    private static final String TEST_FILE2_KEY = TEST_FILE2_PATH.substring(1);
    private static final String TEST_FILE2_DATA = "data2";
    private static final String TEST_FILE3_PATH = "/dir2/file-3.txt";
    private static final String TEST_FILE3_KEY = TEST_FILE3_PATH.substring(1);
    private static final String TEST_FILE3_DATA = "data3";

    private static final String TEST_REGION = "us-east-1";

    private S3Mock api;
    private AmazonS3 client;

    @Before
    public void setUp() {
        this.api = new S3Mock.Builder().withPort(8123).withInMemoryBackend().build();
        api.start();
        AwsClientBuilder.EndpointConfiguration endpoint = new AwsClientBuilder.EndpointConfiguration(
                "http://localhost:8123", TEST_REGION);
        this.client = AmazonS3ClientBuilder
                .standard()
                .withPathStyleAccessEnabled(true)
                .withEndpointConfiguration(endpoint)
                .withCredentials(new AWSStaticCredentialsProvider(new AnonymousAWSCredentials()))
                .build();
        client.createBucket(TEST_BUCKET_NAME);
        client.putObject(TEST_BUCKET_NAME, TEST_FILE1_KEY, TEST_FILE1_DATA);
        client.putObject(TEST_BUCKET_NAME, TEST_FILE2_KEY, TEST_FILE2_DATA);
        client.putObject(TEST_BUCKET_NAME, TEST_FILE3_KEY, TEST_FILE3_DATA);
    }

    @Test
    public void testGetInputStream() throws IOException {
        AmazonS3DataSource dataSource = createDataSource(TEST_FILE1_PATH);
        try (InputStream in = dataSource.getInputStream()) {
            assertEquals(TEST_FILE1_DATA, IOUtils.toString(in));
        }
    }

    @Test
    public void testGetOutputStream() throws IOException {
        AmazonS3DataSource dataSource = createDataSource("new-file.txt");
        try(OutputStream out = dataSource.getOutputStream()) {
            out.write("This is a test".getBytes());
        }
        assertEquals("This is a test", IOUtils.toString(dataSource.getInputStream()));
    }

    @Test
    public void testHasChanged() throws InterruptedException {
        AmazonS3DataSource dataSource = createDataSource(TEST_FILE1_PATH);
        assertTrue(dataSource.hasChanged());
        assertFalse(dataSource.hasChanged());
        Thread.sleep(2000);
        client.putObject(TEST_BUCKET_NAME, TEST_FILE1_KEY, "changed");
        assertTrue(dataSource.hasChanged());
        assertFalse(dataSource.hasChanged());
    }

    @Test
    public void testGetChildren() {
        AmazonS3DataSource dataSource = createDataSource(TEST_DIR1_PATH);
        Set<String> childPaths = dataSource.getChildren().stream()
                .map(DataSource::getPath)
                .collect(Collectors.toSet());
        assertEquals(2, childPaths.size());
        assertTrue(childPaths.contains(TEST_FILE1_PATH));
        assertTrue(childPaths.contains(TEST_FILE2_PATH));
    }

    @Test
    public void testGetChild() {
        AmazonS3DataSource dataSource = createDataSource(TEST_DIR1_PATH);
        Optional<DataSource> child = dataSource.getChild("file-1.txt");
        assertTrue(child.isPresent());
        assertEquals(TEST_FILE1_PATH, child.get().getPath());
    }

    @Test
    public void testGetChild_nonExistent() {
        AmazonS3DataSource dataSource = createDataSource(TEST_DIR1_PATH);
        Optional<DataSource> child = dataSource.getChild("file-a.txt");
        assertTrue(child.isPresent());
    }

    @Test
    public void testEqualsAndHashCode() {
        AmazonS3DataSource dataSource1 = createDataSource(TEST_FILE1_PATH);
        AmazonS3DataSource dataSource2 = createDataSource(TEST_FILE1_PATH);
        AmazonS3DataSource dataSource3 = createDataSource(TEST_FILE2_PATH);
        assertEquals(dataSource1, dataSource2);
        assertEquals(dataSource1.hashCode(), dataSource2.hashCode());
        assertNotEquals(dataSource1, dataSource3);
    }

    @Test
    public void testGetParent() throws IOException {
        DataSource dataSource = createDataSource(TEST_FILE1_PATH);
        DataSource parent = dataSource.getParent();
        assertEquals(TEST_DIR1_PATH, parent.getPath());
        parent = parent.getParent();
        assertEquals(DataSource.PATH_SEPARATOR, parent.getPath());
        parent = parent.getParent();
        assertNull(parent.getPath());
    }

    @Test
    public void testRootGetChildren() throws IOException {
        DataSource dataSource = createDataSource(DataSource.PATH_SEPARATOR);
        Set<String> childPaths = dataSource.getChildren().stream().map(DataSource::getPath).collect(Collectors.toSet());
        assertEquals(2, childPaths.size());
        assertTrue(childPaths.contains(TEST_DIR1_PATH));
        assertTrue(childPaths.contains(TEST_DIR2_PATH));
    }

    @Test
    public void testFindDataSourcesMatchingPattern() throws IOException {
        List<DataSource> results = new ArrayList<>();
        for (DataSource root : createDataSource(TEST_FILE1_PATH).getRoots()) {
            results.addAll(DataSourceUtil.getDataSourcesMatchingGlobPattern(root, "/dir1/file*"));
        }
        Set<String> foundPaths = results.stream().map(DataSource::getPath).collect(Collectors.toSet());
        assertEquals(2, foundPaths.size());
        assertTrue(foundPaths.contains(TEST_FILE1_PATH));
        assertTrue(foundPaths.contains(TEST_FILE2_PATH));
    }

    @After
    public void cleanUp() {
        api.stop();
    }

    private AmazonS3DataSource createDataSource(String path) {
        return new AmazonS3DataSource(TEST_BUCKET_NAME, path, DataSource.PATH_SEPARATOR, null, client);
    }
}