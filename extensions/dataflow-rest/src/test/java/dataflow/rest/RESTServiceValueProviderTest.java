/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  RESTServiceValueProviderTest.java
 */

package dataflow.rest;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.header;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.queryParam;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.parser.DataFlowParser;
import java.net.URI;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;


public class RESTServiceValueProviderTest {

    private DataFlowEngine engine;
    private DataFlowParser parser;
    private MockRestServiceServer mockServer;

    @Before
    public void setUp() {
        RestTemplate restTemplate = new RestTemplate();
        RESTServiceManager serviceManager = new RESTServiceManager();
        serviceManager.register("test", "http://localhost:8080", restTemplate);

        SimpleDependencyInjector dependencyInjector = new SimpleDependencyInjector();
        dependencyInjector.register(serviceManager);
        this.engine = new DataFlowEngine(dependencyInjector);
        RESTDataFlowExtension.register(engine);
        this.parser = new DataFlowParser(engine);
        this.mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void test1_simplePost() throws Exception {
        mockServer.expect(ExpectedCount.once(),
            requestTo(new URI("http://localhost:8080/users")))
            .andExpect(method(HttpMethod.POST))
            .andExpect(content().json("{\"name\":\"Tom\", \"location\":{\"postalCode\":60120}}"))
            .andRespond(withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body("{\"result\": \"success\"}")
            );
        DataFlowInstance instance = registerAndGetInstance("Test1-simplePost.yaml");
        instance.execute();
        mockServer.verify();
        Map<String, Object> result = (Map<String, Object>) instance.getOutput();
        assertEquals(1, result.size());
        assertEquals("success", result.get("result"));
    }

    @Test
    public void test2_simpleGet() throws Exception {
        mockServer.expect(ExpectedCount.once(),
            requestTo(new URI("http://localhost:8080/users/count")))
            .andExpect(method(HttpMethod.GET))
            .andRespond(withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body("{\"result\": \"success\"}")
            );
        DataFlowInstance instance = registerAndGetInstance("Test2-simpleGet.yaml");
        instance.execute();
        mockServer.verify();
        Map<String, Object> result = (Map<String, Object>) instance.getOutput();
        assertEquals(1, result.size());
        assertEquals("success", result.get("result"));
    }

    @Test
    public void test3_getWithRequestParams() throws Exception {
        mockServer.expect(ExpectedCount.once(),
            requestTo(new URI("http://localhost:8080/users?userId=123")))
            .andExpect(method(HttpMethod.GET))
            .andExpect(queryParam("userId", "123"))
            .andRespond(withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body("{\"result\": \"success\"}")
            );
        DataFlowInstance instance = registerAndGetInstance("Test3-getWithQueryParams.yaml");
        instance.execute();
        mockServer.verify();
        Map<String, Object> result = (Map<String, Object>) instance.getOutput();
        assertEquals(1, result.size());
        assertEquals("success", result.get("result"));
    }

    @Test
    public void test4_getWithUrlParams() throws Exception {
        mockServer.expect(ExpectedCount.once(),
            requestTo(new URI("http://localhost:8080/users/123/details")))
            .andExpect(method(HttpMethod.GET))
            .andRespond(withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body("{\"result\": \"success\"}")
            );
        DataFlowInstance instance = registerAndGetInstance("Test4-getWithUrlParams.yaml");
        instance.setValue("userId", 123);
        instance.execute();
        mockServer.verify();
        Map<String, Object> result = (Map<String, Object>) instance.getOutput();
        assertEquals(1, result.size());
        assertEquals("success", result.get("result"));
    }

    @Test
    public void test5_getWithHeaders() throws Exception {
        mockServer.expect(ExpectedCount.once(),
            requestTo(new URI("http://localhost:8080/users")))
            .andExpect(method(HttpMethod.GET))
            .andExpect(header("cacheEnabled", "false"))
            .andRespond(withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body("{\"result\": \"success\"}")
            );
        DataFlowInstance instance = registerAndGetInstance("Test5-getWithHeaders.yaml");
        instance.execute();
        mockServer.verify();
        Map<String, Object> result = (Map<String, Object>) instance.getOutput();
        assertEquals(1, result.size());
        assertEquals("success", result.get("result"));
    }

    @Test
    public void test6_postReturnsCustomType() throws Exception {
        mockServer.expect(ExpectedCount.once(),
            requestTo(new URI("http://localhost:8080/users")))
            .andExpect(method(HttpMethod.POST))
            .andExpect(content().json("{\"name\":\"Tom\", \"location\":{\"postalCode\":60120}}"))
            .andRespond(withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body("{\"id\": \"5\",\"name\":\"Tom\",\"postalCode\":60120}")
            );
        DataFlowInstance instance = registerAndGetInstance("Test6-postReturnsCustomType.yaml");
        instance.execute();
        mockServer.verify();
        User result = (User) instance.getOutput();
        assertEquals((Integer) 5, result.getId());
        assertEquals("Tom", result.getName());
        assertEquals("60120", result.getPostalCode());
    }

    private DataFlowInstance registerAndGetInstance(String filename) throws Exception {
        DataFlowConfig config = parser.parse(getClass().getResourceAsStream(filename));
        engine.registerDataFlow(config);
        return engine.newDataFlowInstance("flow");
    }
}