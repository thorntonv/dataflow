/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  RESTServiceValueProvider.java
 */

package dataflow.rest;

import static dataflow.core.retry.Retry.executeWithRetry;
import static dataflow.core.util.MetricsUtil.getMetricName;
import static dataflow.core.util.MetricsUtil.tagsToStringArray;

import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.retry.Retry;
import dataflow.rest.RESTServiceManager.RESTServiceRegistration;
import dataflow.rest.client.ApiClient;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

@DataFlowComponent
public class RESTServiceValueProvider {

    private final ApiClient apiClient;
    private final String path;
    private final HttpMethod method;
    private final String[] contentTypes;
    private final String[] authNames;
    private final String[] accepts;
    private final Class<?> returnType;
    private final Retry retry;
    private Counter requestCounter;
    private Timer requestTimer;
    private Counter errorCounter;

    @DataFlowConfigurable
    public RESTServiceValueProvider(
        @DataFlowConfigProperty String serviceId,
        @DataFlowConfigProperty String path,
        @DataFlowConfigProperty(required = false) HttpMethod method,
        @DataFlowConfigProperty(required = false) String contentType,
        @DataFlowConfigProperty(required = false) String authNames,
        @DataFlowConfigProperty(required = false) String accepts,
        @DataFlowConfigProperty(required = false) String returnType,
        @DataFlowConfigProperty(required = false) Retry retry,
        @DataFlowConfigProperty(required = false) String metricNamePrefix,
        @DataFlowConfigProperty(required = false) Map<String, String> metricTags) {
        DataFlowExecutionContext executionContext = DataFlowExecutionContext.getCurrentExecutionContext();
        RESTServiceManager serviceManager = executionContext.getDependencyInjector()
            .getInstance(RESTServiceManager.class);

        if(serviceManager == null) {
            throw new DataFlowConfigurationException("A " + RESTServiceManager.class.getSimpleName() +
                " instance must be registered");
        }
        RESTServiceRegistration registration = serviceManager.get(serviceId);
        if(registration == null) {
            throw new DataFlowConfigurationException("No REST service with id " + serviceId +
                " is registered");
        }

        apiClient = new ApiClient(registration.getRestTemplate());
        apiClient.setBasePath(registration.getBasePath());

        this.path = UriComponentsBuilder.fromPath(path).build().toUriString();
        this.method = method != null ? method : HttpMethod.POST;
        this.contentTypes = contentType != null ? contentType.split(",") :
            new String[]{"application/json"};
        this.authNames = authNames != null ? authNames.split(",") : new String[0];
        this.accepts = accepts != null ? accepts.split(",") :
            new String[]{"application/json;charset&#x3D;UTF-8"};
        try {
            this.returnType = returnType != null ? Class.forName(returnType) : LinkedHashMap.class;
        } catch (ClassNotFoundException e) {
            throw new DataFlowConfigurationException("Invalid return type " + returnType, e);
        }
        DataFlowInstance instance = executionContext.getInstance();
        this.retry = Retry.clone(retry != null ? retry : instance.getDefaultRetryBehavior());

        MeterRegistry meterRegistry = executionContext.getDependencyInjector()
            .getInstance(MeterRegistry.class);
        if (meterRegistry != null) {
            Map<String, String> tags = new HashMap<>(executionContext.getInstance()
                .getMetricTagsForComponent(this));
            tags.put("serviceId", serviceId);
            tags.put("path", path);
            if (method != null) {
                tags.put("method", method.name());
            }
            if (metricTags != null) {
                tags.putAll(metricTags);
            }
            metricNamePrefix = metricNamePrefix != null ? metricNamePrefix : getClass().getSimpleName();
            requestCounter = meterRegistry.counter(
                getMetricName(metricNamePrefix, "requestCounter", instance),
                tagsToStringArray(tags));
            requestTimer = meterRegistry.timer(
                getMetricName(metricNamePrefix, "requestTimer", instance),
                tagsToStringArray(tags));
            errorCounter = meterRegistry.counter(
                getMetricName(metricNamePrefix, "requestErrorCounter", instance),
                tagsToStringArray(tags));
            Counter retryCounter = meterRegistry.counter(
                getMetricName(metricNamePrefix, "retryCounter", instance),
                tagsToStringArray(tags));
            if(this.retry != null) {
                this.retry.getEventPublisher().onRetry(event -> retryCounter.increment());
            }
        }
    }

    @OutputValue
    public Object getValue(@InputValue(required = false) Map<String, Object> headers,
        @InputValue(required = false) Object body,
        @InputValue(required = false) Map<String, Object> queryParams,
        @InputValue(required = false) Map<String, Object> formParams) throws Exception {

        final HttpHeaders headerParams = new HttpHeaders();
        addAll(headers, headerParams, Object::toString);

        MultiValueMap<String, String> queryParamsMultiValueMap = new LinkedMultiValueMap<>();
        addAll(queryParams, queryParamsMultiValueMap, Object::toString);

        MultiValueMap<String, Object> formParamsMultiValueMap = new LinkedMultiValueMap<>();
        addAll(formParams, formParamsMultiValueMap, v -> v);

        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        long startTime = System.currentTimeMillis();
        try {
            if (requestCounter != null) {
                requestCounter.increment();
            }
            Object result = executeWithRetry(retry, () -> apiClient.invokeAPI(path, method, queryParamsMultiValueMap, body,
                headerParams, formParamsMultiValueMap, accept, contentType, authNames, returnType));
            if (requestTimer != null) {
                requestTimer.record(System.currentTimeMillis() - startTime, TimeUnit.MILLISECONDS);
            }
            return result;
        } catch(Exception ex) {
            if (errorCounter != null) {
                errorCounter.increment();
            }
            throw ex;
        }
    }

    private <T> void addAll(Map<String, Object> map,
        MultiValueMap<String, T> multiValueMap,
        Function<Object, T> converterFn) {
        if (map != null && !map.isEmpty()) {
            map.forEach((paramName, paramValue) -> {
                if (paramValue instanceof List) {
                    List paramValues = (List) paramValue;
                    paramValues.forEach(value -> multiValueMap.add(paramName, converterFn.apply(value)));
                } else {
                    multiValueMap.add(paramName, converterFn.apply(paramValue));
                }
            });
        }
    }
}
