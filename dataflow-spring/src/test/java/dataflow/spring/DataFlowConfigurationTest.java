/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfigurationTest.java
 */

package dataflow.spring;

import static org.junit.Assert.*;

import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.parser.DataFlowParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DataFlowTestConfiguration.class)
public class DataFlowConfigurationTest {

    @Autowired
    private DataFlowEngine engine;

    @Autowired
    private DataFlowParser parser;

    @Autowired
    private TestImpl testImpl;

    @Before
    public void setUp() {
        engine.registerComponent(DependencyInjectionTestComponentMetadata.INSTANCE,
            new DependencyInjectionTestComponentBuilder(engine));
    }

    @Test
    public void testWiring() {
        assertNotNull(engine);
        assertNotNull(parser);
    }

    @Test
    public void testDependencyInjection() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("dependencyInjectionTest.yaml");
        instance.execute();
        TestImpl injectedImpl = (TestImpl) instance.getOutput();
        assertSame(injectedImpl, testImpl);
    }

    private DataFlowInstance registerAndGetInstance(String filename) throws Exception {
        DataFlowConfig config = parser.parse(getClass().getResourceAsStream(filename));
        engine.registerDataFlow(config);
        return engine.newDataFlowInstance("flow");
    }

}