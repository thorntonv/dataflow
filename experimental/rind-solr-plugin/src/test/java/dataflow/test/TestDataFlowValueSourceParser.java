/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestDataFlowValueSourceParser.java
 */

package dataflow.test;

import dataflow.core.DataFlowManager;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.memorytable.MemoryTableManager;
import dataflow.core.parser.DataFlowParser;
import rind.solr.plugin.DataFlowValueSourceParser;
import rind.solr.plugin.SolrRindDataFlowExtension;

import static dataflow.core.datasource.DataSourceUtil.getClassPathDataSourcesMatchingGlobPattern;

/**
 * A {@link DataFlowValueSourceParser} implementation used for testing.
 */
public class TestDataFlowValueSourceParser extends DataFlowValueSourceParser {

    private static DataFlowManager DATAFLOW_MANAGER;

    static {
        SimpleDependencyInjector dependencyInjector = new SimpleDependencyInjector();
        DataFlowEngine engine = new DataFlowEngine(dependencyInjector);
        dependencyInjector.register(new MemoryTableManager(engine));

        DATAFLOW_MANAGER = new DataFlowManager(new DataFlowParser(engine), engine);
        SolrRindDataFlowExtension.register(engine);

        DATAFLOW_MANAGER.registerConfigProvider(() ->
                getClassPathDataSourcesMatchingGlobPattern("/dataflow-config/*.yaml"));
    }

    @Override
    protected DataFlowEngine getDataFlowEngine() {
        return DATAFLOW_MANAGER.getEngine();
    }
}
