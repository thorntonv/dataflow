/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SolrRindDataFlowExtension.java
 */

package rind.solr.plugin;

import dataflow.core.engine.DataFlowEngine;

public class SolrRindDataFlowExtension {

    public static void register(DataFlowEngine engine) {
        engine.registerComponent(IntDocValueProviderMetadata.INSTANCE, new IntDocValueProviderBuilder(engine));
        engine.registerComponent(LongDocValueProviderMetadata.INSTANCE, new LongDocValueProviderBuilder(engine));
        engine.registerComponent(FloatDocValueProviderMetadata.INSTANCE, new FloatDocValueProviderBuilder(engine));
        engine.registerComponent(DoubleDocValueProviderMetadata.INSTANCE, new DoubleDocValueProviderBuilder(engine));
        engine.registerComponent(BooleanDocValueProviderMetadata.INSTANCE, new BooleanDocValueProviderBuilder(engine));

        engine.registerComponent(ObjectDocValueProviderMetadata.INSTANCE, new ObjectDocValueProviderBuilder(engine));
        engine.registerComponent(FunctionQueryValueProviderMetadata.INSTANCE,
                new FunctionQueryValueProviderBuilder(engine));
    }
}
